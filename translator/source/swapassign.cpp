#include "../modules/strings.h"
#include "../include/variable.h"
#include "../include/elements/swapassign.h"
#include "../../compiler/include/defstrings.h"
#include <algorithm>

using std::sort;

namespace LEAT{

  ELEMENT_SWAP_ASSIGN::ELEMENT_SWAP_ASSIGN(XMLElement* elem) :
    Instruction(elem->IntAttribute(ATTR_ORDER))
  {
    XMLElement *variable = elem->FirstChildElement(XMLTAG_VARIABLE);
    while (variable){
      variables.push_back(new Variable(variable));
      variable = variable->NextSiblingElement(XMLTAG_VARIABLE);
    }
    sort(variables.begin(),variables.end(),Variable::Comparator());
  }

  ELEMENT_SWAP_ASSIGN::~ELEMENT_SWAP_ASSIGN(){
    for (auto it = variables.begin(); it != variables.end(); ++it){
      if (*it)
	delete (*it);
      (*it) = nullptr;
    }
  }

} // namespace LEAT
