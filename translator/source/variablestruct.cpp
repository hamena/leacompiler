#include "../include/variable.h"
#include "../include/expressionshandler.h"
#include "../modules/util/safeuse.h"
#include "../../compiler/include/defstrings.h"
#include <algorithm>

using std::sort;

namespace LEAT{

  Variable::Variable(XMLElement* elem) : type(nullptr){
    id = SafeUse::get_id(elem->Attribute(ATTR_ID),"var");
    construct(elem);
  }

  Variable::Variable(const Variable& arg) :
    order(arg.order),
    id(arg.id)
  {
    type = new ELEMENT_TYPE(*arg.type);
  }

  Variable::Variable(Variable&& arg) :
    order(arg.order),
    id(arg.id),
    type(arg.type)
  {
    arg.type = nullptr;
  }

  Variable::Variable(XMLElement *elem, const string& structName) : type(nullptr){
    id = SafeUse::save_arg(elem->Attribute(ATTR_ID), structName);
    construct(elem);
  }

  void Variable::construct(XMLElement *elem){
    order = elem->IntAttribute(ATTR_ORDER);
    XMLElement *a = elem->FirstChildElement(XMLTAG_ACCESS);
    if (a){
      XMLElement *exp = a->FirstChildElement(XMLTAG_EXPRESSION);
      while (exp){
	access.push_back(ExpressionsHandler::new_expression(exp));
	exp = exp->NextSiblingElement(XMLTAG_EXPRESSION);
      }
      sort(access.begin(),access.end(),Expression::Comparator());
    }
  
    XMLElement *t = elem->FirstChildElement(XMLTAG_TYPE);
    if (t)
      type = new ELEMENT_TYPE(t);

    type->set_dimension(type->get_dimension() - access.size());
  }

  Variable::~Variable(){
    for (auto it=access.begin(); it!=access.end(); ++it){
      if (*it)
	delete (*it);
      (*it) = nullptr;
    }
    
    if (type)
      delete type;
    type = nullptr;
  }
  
} // namespace LEAT
