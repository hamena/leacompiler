#include "../modules/strings.h"
#include "../include/elements/type.h"
#include "../../compiler/include/defstrings.h"

namespace LEAT{

  ELEMENT_TYPE::ELEMENT_TYPE(XMLElement* elem) :
    emptySetFlag(false),
    type(nullptr)
  {
    string str = elem->Attribute(ATTR_NAME);
    if (str == TYPE_LOGIC){
      name = NEW_TYPE_LOGIC;
      initValue = INIT_LOGIC;
    }
    else if (str == TYPE_INTEGER){
      name = NEW_TYPE_INTEGER;
      initValue = INIT_INTEGER;
    }
    else if (str == TYPE_FLOAT){
      name = NEW_TYPE_FLOAT;
      initValue = INIT_FLOAT;
    }
    else if (str == TYPE_CHARACTER){
      name = NEW_TYPE_CHARACTER;
      initValue = INIT_CHARACTER;
    }
    else if (str == TYPE_STRING){
      name = NEW_TYPE_STRING;
      initValue = INIT_STRING;
    }
    else if (str == TYPE_ARRAY){
      name = NEW_TYPE_ARRAY;
      initValue = INIT_ARRAY;
    }
    else if (str == TYPE_SET){
      name = NEW_TYPE_SET;
      initValue = INIT_SET;
    }
    else{
      name = "undef";
      initValue = "undef";
    }
  
    dimension = elem->IntAttribute(ATTR_DIM);
    XMLElement *aux = elem->FirstChildElement(XMLTAG_TYPE);
    if (aux)
      type = new ELEMENT_TYPE(aux);
  }

  ELEMENT_TYPE::ELEMENT_TYPE(const ELEMENT_TYPE& t) :
    emptySetFlag(false),
    name(t.name),
    dimension(t.dimension),
    type(nullptr),
    initValue(t.initValue)
  {
    if (t.type)
      type = new ELEMENT_TYPE(*t.type);
  }

  ELEMENT_TYPE::ELEMENT_TYPE(int dim, ELEMENT_TYPE *st) :
    emptySetFlag(false),
    name(NEW_TYPE_ARRAY),
    dimension (dim),
    type(st),
    initValue(INIT_ARRAY)
  {}

  ELEMENT_TYPE::ELEMENT_TYPE() :
    emptySetFlag(true),
    name("EMPTY_SET"),
    dimension(-1),
    type(nullptr),
    initValue(INIT_SET)
  {}

  void ELEMENT_TYPE::set_dimension(int dim){
    if (name == NEW_TYPE_ARRAY && dim == 0){
      name = type->name;
      dimension = type->dimension;
      ELEMENT_TYPE *aux = type;
      if (type->type)
	type = new ELEMENT_TYPE(*(type->type));
      else
	type = nullptr;
    
      if (aux)
	delete aux;
      aux = nullptr;
    }
    else
      dimension = dim;
  }

  bool ELEMENT_TYPE::is_logic() const{
    return name == NEW_TYPE_LOGIC;
  }

  bool ELEMENT_TYPE::is_integer() const{
    return name == NEW_TYPE_INTEGER;
  }

  bool ELEMENT_TYPE::is_float() const{
    return name == NEW_TYPE_FLOAT;
  }

  bool ELEMENT_TYPE::is_character() const{
    return name == NEW_TYPE_CHARACTER;
  }

  bool ELEMENT_TYPE::is_string() const{
    return name == NEW_TYPE_STRING;
  }

  bool ELEMENT_TYPE::is_set() const{
    return name == NEW_TYPE_SET;
  }

  bool ELEMENT_TYPE::is_array() const{
    return name == NEW_TYPE_ARRAY;
  }

  ELEMENT_TYPE::~ELEMENT_TYPE(){
    if (type)
      delete type;
    type = nullptr;
  }
} // namespace LEAT
