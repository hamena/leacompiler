#include "../include/expressionshandler.h"
#include "../include/instructionshandler.h"
#include "../include/elements/forallloop.h"
#include "../modules/strings.h"
#include "../../compiler/include/defstrings.h"
#include <algorithm>

using std::sort;

#include <iostream>
using namespace std;

namespace LEAT{

  ELEMENT_LOOP_FOR_ALL::ELEMENT_LOOP_FOR_ALL(XMLElement *elem) :
    Instruction(elem->IntAttribute(ATTR_ORDER)),
    iterator(nullptr),
    expression(nullptr)
  {
    XMLElement *it = elem->FirstChildElement(XMLTAG_ITERATOR);
    if (it)
      iterator = new Variable(it);

    XMLElement *exp = elem->FirstChildElement(XMLTAG_EXPRESSION);
    if (exp)
      expression = ExpressionsHandler::new_expression(exp);

    XMLElement *instrs = elem->FirstChildElement(XMLTAG_INSTRS);
    if (instrs){
      XMLElement *instruction = instrs->FirstChildElement(XMLTAG_INSTRUCTION);
      while (instruction){
	instructions.push_back(InstructionsHandler::new_instruction(instruction));
	instruction = instruction->NextSiblingElement(XMLTAG_INSTRUCTION);
      }
      exp = instrs->FirstChildElement(XMLTAG_EXPRESSION);
      while (exp){
	instructions.push_back(InstructionsHandler::new_instruction(exp));
	exp = exp->NextSiblingElement(XMLTAG_EXPRESSION);
      }
      sort(instructions.begin(), instructions.end(), Instruction::Comparator());
    }
  }
  
  ELEMENT_LOOP_FOR_ALL::~ELEMENT_LOOP_FOR_ALL(){
    if (iterator)
      delete iterator;
    iterator = nullptr;

    if (expression)
      delete expression;
    expression = nullptr;

    for (auto it=instructions.begin(); it!=instructions.end(); ++it){
      if (*it)
	delete (*it);
      (*it) = nullptr;
    }
  }

} // namespace LEAT
