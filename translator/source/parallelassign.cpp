#include "../include/expressionshandler.h"
#include "../include/elements/parallelassign.h"
#include "../modules/strings.h"
#include "../../compiler/include/defstrings.h"
#include <algorithm>

using std::sort;

namespace LEAT{

  ELEMENT_PARALLEL_ASSIGN::ELEMENT_PARALLEL_ASSIGN(XMLElement* elem) :
    Instruction(elem->IntAttribute(ATTR_ORDER))
  {
    XMLElement *vars = elem->FirstChildElement(XMLTAG_TUPLE_VARS);
    if (vars){
      XMLElement *var = vars->FirstChildElement(XMLTAG_VARIABLE);
      while(var){
	variables.push_back(new Variable(var));
	var = var->NextSiblingElement(XMLTAG_VARIABLE);
      }
      sort(variables.begin(),variables.end(),Variable::Comparator());
    }
  
    XMLElement *exps = elem->FirstChildElement(XMLTAG_TUPLE_EXPS);
    if (exps){
      XMLElement *exp = exps->FirstChildElement(XMLTAG_EXPRESSION);
      while(exp){
	expressions.push_back(ExpressionsHandler::new_expression(exp));
	exp = exp->NextSiblingElement(XMLTAG_EXPRESSION);
      }
      sort(expressions.begin(),expressions.end(),Expression::Comparator());
    }else{
      XMLElement *exp = elem->FirstChildElement(XMLTAG_EXPRESSION);
      if (exp){
	if ((string)exp->Attribute(ATTR_CLASS) == (string)CLASS_CALL){
	  functionCall = new ELEMENT_FINEXP_FUNCTION_CALL(exp);
	}
      }
    }
  }

  ELEMENT_PARALLEL_ASSIGN::~ELEMENT_PARALLEL_ASSIGN(){
    for (auto it = variables.begin(); it != variables.end(); ++it){
      if (*it)
	delete (*it);
      (*it) = nullptr;
    }
  
    for (auto it = expressions.begin(); it != expressions.end(); ++it){
      if (*it)
	delete (*it);
      (*it) = nullptr;
    }

    if (functionCall)
      delete functionCall;
    functionCall = nullptr;
  }

} // namespace LEAT
