#include "../include/instructionshandler.h"
#include "../include/elements/algorithm.h"
#include "../modules/util/safeuse.h"
#include "../../compiler/include/defstrings.h"
#include <sstream>
#include <algorithm>

using std::stringstream;
using std::sort;

namespace LEAT{

  ELEMENT_ALGORITHM::ELEMENT_ALGORITHM(XMLElement* elem) :
    BaseAlgorithm(elem->FirstChildElement(XMLTAG_HEADER)->Attribute(ATTR_ID))
  {
    // Cabecera
    XMLElement *header = elem->FirstChildElement(XMLTAG_HEADER);
    if (header){    
      XMLElement *arg = nullptr;
      // Argumentos de entrada
      XMLElement *input = header->FirstChildElement(XMLTAG_INPUT_ARGS);
      arg = input->FirstChildElement(XMLTAG_ARG);
      while (arg){
	inputArguments.push_back(new Argument(arg,get_input_name()));
	arg = arg->NextSiblingElement(XMLTAG_ARG);
      }

      // Argumentos de salida
      XMLElement *output = header->FirstChildElement(XMLTAG_OUTPUT_ARGS);
      arg = output->FirstChildElement(XMLTAG_ARG);
      while (arg){
	outputArguments.push_back(new Argument(arg,get_output_name()));
	arg = arg->NextSiblingElement(XMLTAG_ARG);
      }

      // Ordenar listas
      sort(inputArguments.begin(),inputArguments.end(),Argument::Comparator());
      sort(outputArguments.begin(),outputArguments.end(),Argument::Comparator());
    }

    // Instrucciones
    XMLElement *instrs = elem->FirstChildElement(XMLTAG_INSTRS);
    if (instrs){
      XMLElement *instruction = instrs->FirstChildElement();
      while (instruction){
	instructions.push_back(InstructionsHandler::new_instruction(instruction));
	instruction = instruction->NextSiblingElement();
      }
      sort(instructions.begin(),instructions.end(),Instruction::Comparator());
    }
  }

  ELEMENT_ALGORITHM::~ELEMENT_ALGORITHM(){
    for (auto it=inputArguments.begin(); it != inputArguments.end(); ++it){
      if (*it)
	delete (*it);
      (*it) = nullptr;
    }
    for (auto it=outputArguments.begin(); it != outputArguments.end(); ++it){
      if (*it)
	delete (*it);
      (*it) = nullptr;
    }
    for (auto it=instructions.begin(); it != instructions.end(); ++it){
      if (*it)
	delete (*it);
      (*it) = nullptr;
    }
  }

} // namespace LEAT
