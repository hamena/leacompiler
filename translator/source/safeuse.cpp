#include "../modules/util/safeuse.h"
#include <utility>
#include <sstream>

using std::make_pair;
using std::stringstream;

namespace LEAT{

  SafeUse::IDTable SafeUse::idtable;
  string SafeUse::prefix("");
  char SafeUse::detacher('_');
  int SafeUse::counter(0);

  string SafeUse::get_id(const string& id, const string& p){
    IDTable::iterator it = idtable.find(id);
    if (it == idtable.end()){
      string codedID = code_id(p);
      idtable.insert(make_pair(id,codedID));
      return codedID;
    }
    else{
      return it->second;
    }
  }

  string SafeUse::get_temp_id(){
    return code_id("temp");
  }

  string SafeUse::save_arg(const string& id, const string& structName){
    IDTable::iterator it = idtable.find(id);
    if (it == idtable.end()){
      string codedID = code_id("arg");
      idtable.insert(make_pair(id,structName+"."+codedID));
      return codedID;
    }
    else{
      return split(it->second,'.')[1];
    }
  }

  void SafeUse::set_prefix(const string& str){
    prefix = str + detacher;
  }

  void SafeUse::set_detacher(char d){
    detacher = d;
  }

  string SafeUse::code_id(string p){
    if (p != "")
      p += detacher;
    return prefix+p+"id"+detacher+std::to_string(counter++);
  }

  vector<string>& SafeUse::split(const string &s, char delim, vector<string> &elems) {
    stringstream ss(s);
    string item;
    while (getline(ss, item, delim)) {
      elems.push_back(item);
    }
    return elems;
  }
  
  
  vector<string> SafeUse::split(const string &s, char delim) {
    vector<string> elems;
    split(s, delim, elems);
    return elems;
  }
} // namespace LEAT
