#include "../include/instructionshandler.h"
#include "../include/elements/instruction.h"
#include "../include/elements/variabledeclaration.h"
#include "../include/elements/arraydeclaration.h"
#include "../include/elements/simpleassign.h"
#include "../include/elements/parallelassign.h"
#include "../include/elements/swapassign.h"
#include "../include/elements/conditional.h"
#include "../include/elements/whileloop.h"
#include "../include/elements/repeatloop.h"
#include "../include/elements/fromloop.h"
#include "../include/elements/forallloop.h"
#include "../include/elements/expressions/functioncall.h"
#include "../include/elements/expressions/stdcall.h"
#include "../modules/strings.h"
#include "../../compiler/include/defstrings.h"

namespace LEAT{

  Instruction* InstructionsHandler::new_instruction(XMLElement* elem){
    string instructionClass = elem->Attribute(ATTR_CLASS);
    if (instructionClass == INSTR_VAR_DEC)
      return new ELEMENT_VARIABLE_DECLARATION(elem);
    else if (instructionClass == INSTR_ARRAY_DEC)
      return new ELEMENT_ARRAY_DECLARATION(elem);
    else if (instructionClass == INSTR_SASSIGN)
      return new ELEMENT_SIMPLE_ASSIGN(elem);
    else if (instructionClass == INSTR_PASSIGN)
      return new ELEMENT_PARALLEL_ASSIGN(elem);
    else if (instructionClass == INSTR_SWAP)
      return new ELEMENT_SWAP_ASSIGN(elem);
    else if (instructionClass == INSTR_IF)
      return new ELEMENT_CONDITIONAL(elem);
    else if (instructionClass == INSTR_WHILE)
      return new ELEMENT_LOOP_WHILE(elem);
    else if (instructionClass == INSTR_REPEAT)
      return new ELEMENT_LOOP_REPEAT(elem);
    else if (instructionClass == INSTR_FROM)
      return new ELEMENT_LOOP_FROM(elem);
    else if (instructionClass == INSTR_FOR_ALL)
      return new ELEMENT_LOOP_FOR_ALL(elem);
    else if (instructionClass == CLASS_CALL){
      string name = elem->Attribute(ATTR_ID);
      if (name == STDALG_READ)
	return new ELEMENT_STDEXP_READ(elem);
      else if (name == STDALG_WRITE)
	return new ELEMENT_STDEXP_WRITE(elem);
      else if (name == STDALG_LENGTH)
	return new ELEMENT_STDEXP_LENGTH(elem);
      else if (name == STDALG_EXTRACT)
	return new ELEMENT_STDEXP_EXTRACT(elem);
      else
	return new ELEMENT_FINEXP_FUNCTION_CALL(elem);
    }
    else
      return nullptr;
  }
  
} // namespace LEAT
