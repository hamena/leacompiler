#include "../include/expressionshandler.h"
#include "../include/elements/expressions/parenthesis.h"
#include "../../compiler/include/defstrings.h"

namespace LEAT{

  ELEMENT_FINEXP_PARENTHESIS::ELEMENT_FINEXP_PARENTHESIS(XMLElement* elem) :
    Expression(elem->IntAttribute(ATTR_ORDER), elem->FirstChildElement(XMLTAG_TYPE)),
    expression(nullptr)
  {
    XMLElement *exp = elem->FirstChildElement(XMLTAG_EXPRESSION);
    if (exp)
      expression = ExpressionsHandler::new_expression(exp);
  }

  ELEMENT_FINEXP_PARENTHESIS::~ELEMENT_FINEXP_PARENTHESIS(){
    if (expression)
      delete expression;
    expression = nullptr;
  }

} // namespace LEAT
