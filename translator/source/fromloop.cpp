#include "../include/expressionshandler.h"
#include "../include/instructionshandler.h"
#include "../include/elements/fromloop.h"
#include "../include/elements/simpleassign.h"
#include "../modules/strings.h"
#include "../../compiler/include/defstrings.h"
#include <algorithm>

using std::sort;

namespace LEAT{

  ELEMENT_LOOP_FROM::ELEMENT_LOOP_FROM(XMLElement* elem) :
    Instruction(elem->IntAttribute(ATTR_ORDER)),
    withAssign(nullptr),
    incExpression(nullptr),
    numericFlag(false),
    decrementFlag(false)
  {
    XMLElement *instruction = elem->FirstChildElement(XMLTAG_INSTRUCTION);
    if (instruction)
      assign = new ELEMENT_SIMPLE_ASSIGN(instruction);

    XMLElement *instrs = elem->FirstChildElement(XMLTAG_INSTRS);
    if (instrs){
      XMLElement *instruction = instrs->FirstChildElement(XMLTAG_INSTRUCTION);
      while (instruction){
	instructions.push_back(InstructionsHandler::new_instruction(instruction));
	instruction = instruction->NextSiblingElement(XMLTAG_INSTRUCTION);
      }
      sort(instructions.begin(),instructions.end(),Instruction::Comparator());
    }

    XMLElement *fromGeneral = elem->FirstChildElement(XMLTAG_FROM_GENERAL);
    if (fromGeneral){
      XMLElement *exp = fromGeneral->FirstChildElement(XMLTAG_EXPRESSION);
      if (exp)
	expression = ExpressionsHandler::new_expression(exp);

      instruction = fromGeneral->FirstChildElement(XMLTAG_INSTRUCTION);
      if (instruction)
	withAssign = new ELEMENT_SIMPLE_ASSIGN(instruction);
    }
    else{
      XMLElement *fromNumeric = elem->FirstChildElement(XMLTAG_FROM_NUMERIC);
      if (fromNumeric){
	numericFlag = true;
	XMLElement *exp = fromNumeric->FirstChildElement(XMLTAG_EXPRESSION);
	if (exp)
	  expression = ExpressionsHandler::new_expression(exp);

	XMLElement *dec = fromNumeric->FirstChildElement(XMLTAG_DECREMENT);
	if (dec)
	  decrementFlag = true;
	else
	  dec = fromNumeric->FirstChildElement(XMLTAG_INCREMENT);

	if (dec){
	  exp = dec->FirstChildElement(XMLTAG_EXPRESSION);
	  if (exp)
	    incExpression = ExpressionsHandler::new_expression(exp);
	}
      }
    }
  }

  ELEMENT_LOOP_FROM::~ELEMENT_LOOP_FROM(){
    if (assign)
      delete assign;
    if (withAssign)
      delete withAssign;
    assign = withAssign = nullptr;

    if (expression)
      delete expression;
    if (incExpression)
      delete incExpression;
    expression = incExpression = nullptr;

    for (auto it=instructions.begin(); it != instructions.end(); ++it){
      if (*it)
	delete (*it);
      (*it) = nullptr;
    }
  }

} // namespace LEAT
