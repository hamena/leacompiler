#include "../include/expressionshandler.h"
#include "../../compiler/include/defstrings.h"
#include "../modules/strings.h"
#include "../include/elements/expressions/literals.h"
#include "../include/elements/expressions/immediatearray.h"
#include "../include/elements/expressions/immediateset.h"
#include "../include/elements/expressions/variable.h"
#include "../include/elements/expressions/emptyset.h"
#include "../include/elements/expressions/relationals.h"
#include "../include/elements/expressions/setbased.h"
#include "../include/elements/expressions/arithmetics.h"
#include "../include/elements/expressions/unary.h"
#include "../include/elements/expressions/parenthesis.h"
#include "../include/elements/expressions/functioncall.h"
#include "../include/elements/expressions/stdcall.h"

namespace LEAT{

  Expression* ExpressionsHandler::new_expression(XMLElement *elem){
    string expClass = elem->Attribute(ATTR_CLASS);

    if (expClass == CLASS_LITERAL)
      return literal_switch(elem);
    else if (expClass == CLASS_ARRAY)
      return new ELEMENT_IMMEXP_ARRAY(elem);
    else if (expClass == CLASS_SET)
      return new ELEMENT_IMMEXP_SET(elem);
    else if (expClass == CLASS_BINARY)
      return binary_switch(elem);
    else if (expClass == CLASS_UNARY)
      return unary_switch(elem);
    else if (expClass == CLASS_PAREN)
      return new ELEMENT_FINEXP_PARENTHESIS(elem);
    else if (expClass == CLASS_VAR)
      return new ELEMENT_FINEXP_VARIABLE(elem);
    else if (expClass == CLASS_CALL)
      return function_switch(elem);
    else if (expClass == CLASS_EMPTY_SET)
      return new ELEMENT_SETEXP_EMPTY_SET(elem);
    else
      return nullptr;
  }

  Expression* ExpressionsHandler::binary_switch(XMLElement* elem){
    string oper = elem->Attribute(ATTR_OPER);
    if (oper == OPER_EQ)
      return new ELEMENT_BINEXP_EQUALS(elem);
    else if (oper == OPER_NEQ)
      return new ELEMENT_BINEXP_NOT_EQUALS(elem);
    else if (oper == OPER_CONJ)
      return new ELEMENT_BINEXP_AND(elem);
    else if (oper == OPER_DISY)
      return new ELEMENT_BINEXP_OR(elem);
    else if (oper == OPER_LT)
      return new ELEMENT_BINEXP_LESSER(elem);
    else if (oper == OPER_LE)
      return new ELEMENT_BINEXP_LESSER_EQUALS(elem);
    else if (oper == OPER_GT)
      return new ELEMENT_BINEXP_GREATER(elem);
    else if (oper == OPER_GE)
      return new ELEMENT_BINEXP_GREATER_EQUALS(elem);
    else if (oper == OPER_IN)
      return new ELEMENT_BINEXP_IN(elem);
    else if (oper == OPER_CI)
      return new ELEMENT_BINEXP_CONTAINED_IN(elem);
    else if (oper == OPER_SCI)
      return new ELEMENT_BINEXP_SCONTAINED_IN(elem);
    else if (oper == OPER_C)
      return new ELEMENT_BINEXP_CONTAINS(elem);
    else if (oper == OPER_SC)
      return new ELEMENT_BINEXP_SCONTAINS(elem);
    else if (oper == OPER_UNION)
      return new ELEMENT_BINEXP_UNION(elem);
    else if (oper == OPER_INTERSECTION)
      return new ELEMENT_BINEXP_INTERSECTION(elem);
    else if (oper == OPER_PLUS)
      return new ELEMENT_BINEXP_PLUS(elem);
    else if (oper == OPER_MINUS)
      return new ELEMENT_BINEXP_MINUS(elem);
    else if (oper == OPER_SLASH)
      return new ELEMENT_BINEXP_SLASH(elem);
    else if (oper == OPER_STAR)
      return new ELEMENT_BINEXP_STAR(elem);
    else if (oper == OPER_MOD)
      return new ELEMENT_BINEXP_MODULE(elem);
    else if (oper == OPER_DIV)
      return new ELEMENT_BINEXP_DIVISION(elem);
    else 
      return nullptr;
  }

  Expression* ExpressionsHandler::unary_switch(XMLElement* elem){
    string oper = elem->Attribute(ATTR_OPER);
    if (oper == OPER_PLUS)
      return new ELEMENT_UNAEXP_PLUS(elem);
    else if (oper == OPER_MINUS)
      return new ELEMENT_UNAEXP_MINUS(elem);
    else if (oper == OPER_NOT)
      return new ELEMENT_UNAEXP_NOT(elem);
    else
      return nullptr;
  }

  Expression* ExpressionsHandler::literal_switch(XMLElement* elem){
    XMLElement *type = elem->FirstChildElement(XMLTAG_TYPE);
    if (type){
      int order = elem->IntAttribute(ATTR_ORDER);
      string value = elem->Attribute(ATTR_VALUE);
      string typeName = type->Attribute(ATTR_NAME);
      if (typeName == TYPE_LOGIC){
	value == LITERAL_TRUE ? value = LOGIC_TRUE : value = LOGIC_FALSE;
	return new ELEMENT_LITEXP_LOGIC(order,value,type);
      }else if (typeName == TYPE_INTEGER)
	return new ELEMENT_LITEXP_INTEGER(order,value,type);
      else if (typeName == TYPE_FLOAT)
	return new ELEMENT_LITEXP_FLOAT(order,value,type);
      else if (typeName == TYPE_CHARACTER)
	return new ELEMENT_LITEXP_CHARACTER(order,value,type);
      else if (typeName == TYPE_STRING)
	return new ELEMENT_LITEXP_STRING(order,value,type);
      else
	return nullptr;
    }
    return nullptr;    
  }

  Expression* ExpressionsHandler::function_switch(XMLElement* elem){
    string name = elem->Attribute(ATTR_ID);
    if (name == STDALG_READ)
      return new ELEMENT_STDEXP_READ(elem);
    else if (name == STDALG_WRITE)
      return new ELEMENT_STDEXP_WRITE(elem);
    else if (name == STDALG_LENGTH)
      return new ELEMENT_STDEXP_LENGTH(elem);
    else if (name == STDALG_EXTRACT)
      return new ELEMENT_STDEXP_EXTRACT(elem);
    else
      return new ELEMENT_FINEXP_FUNCTION_CALL(elem);
  }

} // namespace LEAT
