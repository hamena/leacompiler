#include "../modules/util/declared.h"

namespace LEAT{

  set<string> Declared::IDset;

  void Declared::save(const string& id){
    IDset.insert(id);
  }

  bool Declared::is_declared(const string& id){
    return IDset.find(id) != IDset.end();
  }
  
} // namespace LEAT
