#include "../modules/util/tabs.h"

namespace LEAT{

  int Tabs::counter(0);

  string Tabs::get_tabs(){
    string t = "";
    for (int i=0; i<counter; ++i)
      t += "\t";
    return t;
  }

  void Tabs::increment(){
    ++counter;
  }

  void Tabs::decrement(){
    --counter;
  }
} // namespace LEAT
