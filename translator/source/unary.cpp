#include "../include/expressionshandler.h"
#include "../include/elements/expressions/unary.h"
#include "../../compiler/include/defstrings.h"

namespace LEAT{

  UnaryExpression::UnaryExpression(XMLElement *elem) :
    Expression(elem->IntAttribute(ATTR_ORDER), elem->FirstChildElement(XMLTAG_TYPE)),
    expression(nullptr)
  {
    XMLElement *exp = elem->FirstChildElement(XMLTAG_EXPRESSION);
    if (exp){
      expression = ExpressionsHandler::new_expression(exp);
    }
  }

  UnaryExpression::~UnaryExpression(){
    if (expression)
      delete expression;
    expression = nullptr;
  }
} // namespace LEAT
