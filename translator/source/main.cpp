#include <iostream>
#include <fstream>
#include <sstream>
#include <list>
#include <unistd.h>
#include "../include/tinyxml2.h"
#include "../include/elements/start.h"
#include "../modules/util/safeuse.h"
#include "../../compiler/include/defstrings.h"

using namespace tinyxml2;
using std::stringstream;
using std::ofstream;
using std::list;

using std::cout;
using std::endl;

int main(int argc, char **argv){
  string inputFile, outputFile="output.cpp";
  bool help = true, coutFlag = false;
  int opt;
  while ((opt = getopt(argc, argv, ":t:o:hme")) != -1){
    switch (opt){
    case 't':
      help = false;
      inputFile = optarg;
      break;
    case 'o':
      outputFile = optarg;
      break;
    case 'h':
      help = true;
      break;
    case 'm':
      LEAT::SafeUse::set_prefix("montagon");
      break;
    case 'e':
      coutFlag = true;
      break;
    default:
      help = true;
    }
  }

  if (help){
    cout << "# Ayuda Traductor de LEA #" << endl;
    cout << "Opciones:" << endl;
    cout << "\t-t file.lea\tIndica fichero a traducir." << endl;
    cout << "\t[-o file.xml]\tEspecifica el fichero de salida del programa." << endl;
    cout << "\t[-e]\t\tActiva la salida estándar." << endl;
    cout << "\t[-h]\t\tMuestra esta ayuda." << endl;
  }
  else{
    XMLDocument doc;
    doc.LoadFile(inputFile.c_str());
    XMLElement *algs = doc.FirstChildElement(XMLTAG_ALGS);
    LEAT::ELEMENT_START *start = nullptr;
  
    if (algs){
      start = new LEAT::ELEMENT_START(algs);
    }

    if (start){
      stringstream line, defs;
      start->print(line,defs);

      if (coutFlag){
	cout << defs.str() << endl << endl;
	cout << line.str() << endl;
      }

      ofstream out;
      out.open(outputFile);
      out << defs.str() << endl << endl << line.str() << endl;
      out.close();
    
      delete start;
      start = nullptr;
    }
  }
  return 0;
}
