#include "../include/elements/arraydeclaration.h"
#include "../modules/util/safeuse.h"
#include "../modules/strings.h"
#include "../../compiler/include/defstrings.h"
#include "../include/expressionshandler.h"
#include <algorithm>

using std::sort;

namespace LEAT{

  ELEMENT_ARRAY_DECLARATION::ELEMENT_ARRAY_DECLARATION(XMLElement* elem) :
    Instruction(elem->IntAttribute(ATTR_ORDER))
  {
    XMLElement *access = elem->FirstChildElement(XMLTAG_ACCESS);
    if (access){
      XMLElement *exp = access->FirstChildElement(XMLTAG_EXPRESSION);
      while (exp){
	sizes.push_back(ExpressionsHandler::new_expression(exp));
	exp = exp->NextSiblingElement(XMLTAG_EXPRESSION);
      }
      sort(sizes.begin(),sizes.end(),Expression::Comparator());
    }

    XMLElement *t = elem->FirstChildElement(XMLTAG_TYPE);
    if (t){
      type = new ELEMENT_TYPE(sizes.size(),new ELEMENT_TYPE(t));
    }
  
    XMLElement *newArray = elem->FirstChildElement(XMLTAG_NEW_ARRAY);
    while (newArray){
      ids.push_back(SafeUse::get_id(newArray->Attribute(ATTR_ID),"vector"));
      newArray = newArray->NextSiblingElement(XMLTAG_NEW_ARRAY);
    }
  }

  ELEMENT_ARRAY_DECLARATION::~ELEMENT_ARRAY_DECLARATION(){
    for (auto it = sizes.begin(); it != sizes.end(); ++it){
      if (*it)
	delete (*it);
      (*it) = nullptr;
    }
  
    if (type)
      delete type;
    type = nullptr;
  }

} // namespace LEAT
