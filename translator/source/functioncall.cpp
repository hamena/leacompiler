#include "../include/expressionshandler.h"
#include "../include/elements/expressions/functioncall.h"
#include "../modules/util/safeuse.h"
#include "../../compiler/include/defstrings.h"
#include <algorithm>

namespace LEAT{

  ELEMENT_FINEXP_FUNCTION_CALL::ELEMENT_FINEXP_FUNCTION_CALL(XMLElement* elem) :
    Expression(elem->IntAttribute(ATTR_ORDER), elem->FirstChildElement(XMLTAG_TYPE)),
    Instruction(elem->IntAttribute(ATTR_ORDER)),
    BaseAlgorithm(elem->Attribute(ATTR_ID))
  {
    XMLElement *tuple = elem->FirstChildElement(XMLTAG_TUPLE_EXPS);
    if (tuple){
      XMLElement *exp = tuple->FirstChildElement(XMLTAG_EXPRESSION);
      while (exp){
	expressions.push_back(ExpressionsHandler::new_expression(exp));
	exp = exp->NextSiblingElement(XMLTAG_EXPRESSION);
      }
      std::sort(expressions.begin(),expressions.end(),Expression::Comparator());
    }
    XMLElement *args = elem->FirstChildElement(XMLTAG_OUTPUT_ARGS);
    if (args){
      XMLElement *arg = args->FirstChildElement(XMLTAG_ARG);
      while (arg){
	outputArgs.push_back(new Argument(arg,get_output_name()));
	arg = arg->NextSiblingElement(XMLTAG_ARG);
      }
    }
  }

  ELEMENT_FINEXP_FUNCTION_CALL::~ELEMENT_FINEXP_FUNCTION_CALL(){
    for (auto it=expressions.begin(); it!=expressions.end(); ++it){
      if (*it)
	delete (*it);
      (*it) = nullptr;
    }
  }
} // namespace LEAT
