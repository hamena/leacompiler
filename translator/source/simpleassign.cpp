#include "../include/elements/simpleassign.h"
#include "../include/expressionshandler.h"
#include "../modules/strings.h"
#include "../../compiler/include/defstrings.h"

namespace LEAT{

  ELEMENT_SIMPLE_ASSIGN::ELEMENT_SIMPLE_ASSIGN(XMLElement* elem) :
    Instruction(elem->IntAttribute(ATTR_ORDER))
  { 
    XMLElement *exp = elem->FirstChildElement(XMLTAG_EXPRESSION);
    if (exp){
      expression = ExpressionsHandler::new_expression(exp);
    }
  
    XMLElement *var = elem->FirstChildElement(XMLTAG_VARIABLE);
    if (var){
      variable = new Variable(var);
      variable->get_type()->set_dimension(expression->get_dimension());
    }
  }

  ELEMENT_SIMPLE_ASSIGN::~ELEMENT_SIMPLE_ASSIGN(){
    if (variable)
      delete variable;
    variable = nullptr;
  
    if (expression)
      delete expression;
    expression = nullptr;
  }
} // namespace LEAT
