#include "../include/elements/start.h"
#include "../../compiler/include/defstrings.h"

namespace LEAT{

  ELEMENT_START::ELEMENT_START(XMLElement *elem){
    XMLElement *alg = elem->FirstChildElement(XMLTAG_ALG);
    while (alg){
      algorithms.push_back(new ELEMENT_ALGORITHM(alg));
      alg = alg->NextSiblingElement(XMLTAG_ALG);
    }
  }

  ELEMENT_START::~ELEMENT_START(){
    for (auto it=algorithms.begin(); it!=algorithms.end(); ++it){
      if (*it)
	delete (*it);
      (*it) = nullptr;
    }
  }

} // namespace LEAT
