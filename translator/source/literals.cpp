#include "../include/elements/expressions/literals.h"
#include "../../compiler/include/defstrings.h"
#include "../modules/util/safeuse.h"

namespace LEAT{

  ELEMENT_LITEXP_LOGIC::ELEMENT_LITEXP_LOGIC(int o, const string& v, XMLElement* t) :
    LiteralExpression(o,v,t)
  {}

  ELEMENT_LITEXP_INTEGER::ELEMENT_LITEXP_INTEGER(int o, const string& v, XMLElement* t) :
    LiteralExpression(o,v,t)
  {}

  ELEMENT_LITEXP_FLOAT::ELEMENT_LITEXP_FLOAT(int o, const string& v, XMLElement* t) :
    LiteralExpression(o,v,t)
  {}

  ELEMENT_LITEXP_CHARACTER::ELEMENT_LITEXP_CHARACTER(int o, const string& v, XMLElement* t) :
    LiteralExpression(o,v,t)
  {}
  
  ELEMENT_LITEXP_STRING::ELEMENT_LITEXP_STRING(int o, const string& v, XMLElement* t) :
    LiteralExpression(o,v,t)
  {}
} // namespace LEAT
