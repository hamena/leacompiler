#include "../include/elements/variabledeclaration.h"
#include "../modules/strings.h"
#include "../modules/util/safeuse.h"
#include "../../compiler/include/defstrings.h"

namespace LEAT{

  ELEMENT_VARIABLE_DECLARATION::ELEMENT_VARIABLE_DECLARATION(XMLElement* elem):
    Instruction(elem->IntAttribute(ATTR_ORDER))
  {
    XMLElement *t = elem->FirstChildElement(XMLTAG_TYPE);
    if (t)
      type = new ELEMENT_TYPE(t);

    XMLElement *newVariable = elem->FirstChildElement(XMLTAG_NEW_VAR);
    while (newVariable){
      ids.push_back(SafeUse::get_id(newVariable->Attribute(ATTR_ID),"var"));
      newVariable = newVariable->NextSiblingElement(XMLTAG_NEW_VAR);
    }
  }

  ELEMENT_VARIABLE_DECLARATION::~ELEMENT_VARIABLE_DECLARATION(){
    if (type)
      delete type;
    type = nullptr;
  }
} // namespace LEAT
