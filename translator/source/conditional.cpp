#include "../include/expressionshandler.h"
#include "../include/instructionshandler.h"
#include "../include/elements/conditional.h"
#include "../modules/strings.h"
#include "../../compiler/include/defstrings.h"
#include <algorithm>

using std::sort;

namespace LEAT{

  ELEMENT_CONDITIONAL::ELEMENT_CONDITIONAL(XMLElement* elem) :
    Instruction(elem->IntAttribute(ATTR_ORDER))
  {
    XMLElement *exp = elem->FirstChildElement(XMLTAG_EXPRESSION);
    if (exp)
      expression = ExpressionsHandler::new_expression(exp);

    XMLElement *instrs = elem->FirstChildElement(XMLTAG_INSTRS);
    if (instrs){
      XMLElement *instruction = instrs->FirstChildElement(XMLTAG_INSTRUCTION);
      while (instruction){
	instructions.push_back(InstructionsHandler::new_instruction(instruction));
	instruction = instruction->NextSiblingElement(XMLTAG_INSTRUCTION);
      }
      sort(instructions.begin(),instructions.end(),Instruction::Comparator());
    }
    XMLElement *elseblock = elem->FirstChildElement(XMLTAG_ELSE);
    if (elseblock){
      XMLElement *instrs = elseblock->FirstChildElement(XMLTAG_INSTRS);
      if (instrs){
	XMLElement *instruction = instrs->FirstChildElement(XMLTAG_INSTRUCTION);;
	while (instruction){
	  elseInstructions.push_back(InstructionsHandler::new_instruction(instruction));
	  instruction = instruction->NextSiblingElement(XMLTAG_INSTRUCTION);
	}
	sort(elseInstructions.begin(),elseInstructions.end(),Instruction::Comparator());
      }
    }
  }

  ELEMENT_CONDITIONAL::~ELEMENT_CONDITIONAL(){
    if (expression)
      delete expression;
    expression = nullptr;

    for (auto it=instructions.begin(); it != instructions.end(); ++it){
      if (*it)
	delete (*it);
      (*it) = nullptr;
    }

    for (auto it=elseInstructions.begin(); it != elseInstructions.end(); ++it){
      if (*it)
	delete (*it);
      (*it) = nullptr;
    }
  }

} // namespace LEAT
