#include "../include/elements/expressions/variable.h"
#include "../modules/strings.h"
#include "../../compiler/include/defstrings.h"

namespace LEAT{

  ELEMENT_FINEXP_VARIABLE::ELEMENT_FINEXP_VARIABLE(XMLElement* elem) :
    Expression(elem->IntAttribute(ATTR_ORDER),elem->FirstChildElement(XMLTAG_TYPE)),
    Variable(elem)
  {

  }

  ELEMENT_FINEXP_VARIABLE::~ELEMENT_FINEXP_VARIABLE(){}
  
} // namespace LEAT
