#include "../include/elements/expressions/binaryexpression.h"
#include "../include/expressionshandler.h"
#include "../../compiler/include/defstrings.h"

namespace LEAT{

  BinaryExpression::BinaryExpression(XMLElement* elem) :
    Expression(elem->IntAttribute(ATTR_ORDER),elem->FirstChildElement(XMLTAG_TYPE)),
    leftExpression(nullptr),
    rightExpression(nullptr)
  {
    XMLElement *exp = elem->FirstChildElement(XMLTAG_EXPRESSION);

    if (exp){
      XMLElement *le = exp;
      exp = exp->NextSiblingElement(XMLTAG_EXPRESSION);
      //ExpressionsHandler::new_expression();
      XMLElement *re = exp;
      int lorder = le->IntAttribute(ATTR_ORDER);
      int rorder = re->IntAttribute(ATTR_ORDER);
      if (le && re){
	if (rorder < lorder){
	  exp = le;
	  le = re;
	  re = exp;
	}
	leftExpression = ExpressionsHandler::new_expression(le);
	rightExpression = ExpressionsHandler::new_expression(re);
	le = re = exp = nullptr;
      }
    }
  }

  BinaryExpression::~BinaryExpression(){
    if (leftExpression)
      delete leftExpression;
    if (rightExpression)
      delete rightExpression;

    leftExpression = rightExpression = nullptr;
  }
} // namespace LEAT
