#include "../include/basealgorithm.h"
#include "../modules/util/safeuse.h"
#include "../../compiler/include/defstrings.h"

namespace LEAT{

  const set<string> BaseAlgorithm::nameWhitelist({ALG_MAIN,STDALG_READ, STDALG_WRITE,
	STDALG_LENGTH, STDALG_EXTRACT});

  BaseAlgorithm::BaseAlgorithm(const string& name){
    if (nameWhitelist.find(name) == nameWhitelist.cend()){
      algorithmName = SafeUse::get_id(name, "funcion");
    }
    else
      algorithmName = name;
  }

} // namespace LEAT
