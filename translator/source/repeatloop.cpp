#include "../include/expressionshandler.h"
#include "../include/instructionshandler.h"
#include "../include/elements/repeatloop.h"
#include "../modules/strings.h"
#include "../../compiler/include/defstrings.h"
#include <algorithm>

using std::sort;

namespace LEAT{

  ELEMENT_LOOP_REPEAT::ELEMENT_LOOP_REPEAT(XMLElement* elem) :
    Instruction(elem->IntAttribute(ATTR_ORDER)), untilFlag(false)
  {
    XMLElement *instrs = elem->FirstChildElement(XMLTAG_INSTRS);
    if (instrs){
      XMLElement *instruction = instrs->FirstChildElement(XMLTAG_INSTRUCTION);
      while (instruction){
	instructions.push_back(InstructionsHandler::new_instruction(instruction));
	instruction = instruction->NextSiblingElement(XMLTAG_INSTRUCTION);
      }
      sort(instructions.begin(),instructions.end(),Instruction::Comparator());
    }

    XMLElement *repeatType = elem->FirstChildElement(XMLTAG_REPEAT_UNTIL);
    if (repeatType)
      untilFlag = true;
    else
      repeatType = elem->FirstChildElement(XMLTAG_REPEAT_WHILE);
  
    if (repeatType){
      XMLElement *exp = repeatType->FirstChildElement(XMLTAG_EXPRESSION);
      expression = ExpressionsHandler::new_expression(exp);
    }
  }

  ELEMENT_LOOP_REPEAT::~ELEMENT_LOOP_REPEAT(){
    if (expression)
      delete expression;
    expression = nullptr;

    for (auto it=instructions.begin(); it != instructions.end(); ++it){
      if (*it)
	delete (*it);
      (*it) = nullptr;
    }
  }
} // namespace LEAT
