#include "../include/elements/expressions/expression.h"
#include "../include/elements/type.h"
#include "../modules/strings.h"
#include "../modules/util/safeuse.h"

namespace LEAT{

  Expression::Expression(int o, XMLElement *t) :
    order(o)
  {
    if (t)
      type = new ELEMENT_TYPE(t);
    else
      type = new ELEMENT_TYPE;
  }

  Expression::~Expression(){
    if (type)
      delete type;
    type = nullptr;
  }
  
} // namespace LEAT
