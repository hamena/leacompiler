#include "../include/elements/expressions/immediate.h"
#include "../include/expressionshandler.h"
#include "../modules/util/safeuse.h"
#include "../../compiler/include/defstrings.h"
#include <algorithm>

using std::sort;

namespace LEAT{

  Immediate::Immediate(XMLElement* elem) :
    Expression(elem->IntAttribute(ATTR_ORDER), elem->FirstChildElement(XMLTAG_TYPE))
  {
    XMLElement* exp = elem->FirstChildElement(XMLTAG_EXPRESSION);
    while(exp){
      expressions.push_back(ExpressionsHandler::new_expression(exp));
      exp = exp->NextSiblingElement(XMLTAG_EXPRESSION);
    }
    sort(expressions.begin(),expressions.end(),Expression::Comparator());
  }

  Immediate::~Immediate(){
    for (auto it = expressions.begin(); it != expressions.end(); ++it){
      if (*it)
	delete (*it);
      (*it) = nullptr;
    }
  }

} // namespace LEAT
