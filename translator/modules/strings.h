#ifndef CLASSNAMES_H_
#define CLASSNAMES_H_

// *** ZONA CONFIGURABLE ***
#define LOGIC_TRUE                          "true"
#define LOGIC_FALSE                         "false"

#define NEW_TYPE_LOGIC                     "bool"
#define NEW_TYPE_INTEGER                   "int"
#define NEW_TYPE_FLOAT                     "float"
#define NEW_TYPE_CHARACTER                 "char"
#define NEW_TYPE_STRING                    "string"
#define NEW_TYPE_ARRAY                     "vector"
#define NEW_TYPE_SET                       "set"

#define INIT_LOGIC                         "false"
#define INIT_INTEGER                       "0"
#define INIT_FLOAT                         "0.0"
#define INIT_CHARACTER                     "' '"
#define INIT_STRING                        "\"\""
#define INIT_ARRAY                         "{}"
#define INIT_SET                           "{}"
// * Fin zona configurable *

// Definciones de includes
#define INCLUDE_START                      "../../include/elements/start.h"
#define INCLUDE_ALGORITHM                  "../../include/elements/algorithm.h"
#define INCLUDE_TYPE                       "../../include/elements/type.h"
#define INCLUDE_VARIABLE_DECLARATION       "../../include/elements/variabledeclaration.h"
#define INCLUDE_ARRAY_DECLARATION          "../../include/elements/arraydeclaration.h"
#define INCLUDE_SIMPLE_ASSIGN              "../../include/elements/simpleassign.h"
#define INCLUDE_PARALLEL_ASSIGN            "../../include/elements/parallelassign.h"
#define INCLUDE_SWAP_ASSIGN                "../../include/elements/swapassign.h"
#define INCLUDE_CONDITIONAL                "../../include/elements/conditional.h"
#define INCLUDE_LOOP_WHILE                 "../../include/elements/whileloop.h"
#define INCLUDE_LOOP_REPEAT                "../../include/elements/repeatloop.h"
#define INCLUDE_LOOP_FROM                  "../../include/elements/fromloop.h"
#define INCLUDE_LOOP_FOR_ALL               "../../include/elements/forallloop.h"
#define INCLUDE_VARIABLE                   "../../include/elements/expressions/variable.h"
#define INCLUDE_LITERALS                   "../../include/elements/expressions/literals.h"
#define INCLUDE_IMMEDIATE_ARRAY            "../../include/elements/expressions/immediatearray.h"
#define INCLUDE_IMMEDIATE_SET              "../../include/elements/expressions/immediateset.h"
#define INCLUDE_EMPTY_SET                  "../../include/elements/expressions/emptyset.h"
#define INCLUDE_RELATIONALS                "../../include/elements/expressions/relationals.h"
#define INCLUDE_SET_BASED                  "../../include/elements/expressions/setbased.h"
#define INCLUDE_ARITHMETICS                "../../include/elements/expressions/arithmetics.h"
#define INCLUDE_UNARY                      "../../include/elements/expressions/unary.h"
#define INCLUDE_PARENTHESIS                "../../include/elements/expressions/parenthesis.h"
#define INCLUDE_FUNCTION_CALL              "../../include/elements/expressions/functioncall.h"
#define INCLUDE_STD                        "../../include/elements/expressions/stdcall.h"

// Nombres de las clases
#define ELEMENT_START                      Start
#define ELEMENT_ALGORITHM                  Algorithm
#define ELEMENT_TYPE                       TypeElement
#define ELEMENT_VARIABLE_DECLARATION       VariableDeclaration
#define ELEMENT_ARRAY_DECLARATION          ArrayDeclaration
#define ELEMENT_SIMPLE_ASSIGN              SimpleAssign
#define ELEMENT_PARALLEL_ASSIGN            ParallelAssign
#define ELEMENT_SWAP_ASSIGN                SwapAssign
#define ELEMENT_CONDITIONAL                Conditional
#define ELEMENT_LOOP_WHILE                 WhileLoop
#define ELEMENT_LOOP_REPEAT                RepeatLoop
#define ELEMENT_LOOP_FROM                  FromLoop
#define ELEMENT_LOOP_FOR_ALL               ForAllLoop

#define ELEMENT_BINEXP_EQUALS              ExpressionEquals
#define ELEMENT_BINEXP_NOT_EQUALS          ExpressionNotEquals
#define ELEMENT_BINEXP_AND                 ExpressionAnd
#define ELEMENT_BINEXP_OR                  ExpressionOr
#define ELEMENT_BINEXP_IN                  ExpressionIn
#define ELEMENT_BINEXP_LESSER              ExpressionLesser
#define ELEMENT_BINEXP_LESSER_EQUALS       ExpressionLesserEquals
#define ELEMENT_BINEXP_GREATER             ExpressionGreater
#define ELEMENT_BINEXP_GREATER_EQUALS      ExpressionGreaterEquals
#define ELEMENT_BINEXP_CONTAINED_IN        ExpressionContainedIn
#define ELEMENT_BINEXP_SCONTAINED_IN       ExpressionStrictContainedIn
#define ELEMENT_BINEXP_CONTAINS            ExpressionContains
#define ELEMENT_BINEXP_SCONTAINS           ExpressionStrictContains
#define ELEMENT_BINEXP_PLUS                ExpressionPlus
#define ELEMENT_BINEXP_MINUS               ExpressionMinus
#define ELEMENT_BINEXP_SLASH               ExpressionSlash
#define ELEMENT_BINEXP_STAR                ExpressionStar
#define ELEMENT_BINEXP_MODULE              ExpressionModule
#define ELEMENT_BINEXP_DIVISION            ExpressionDivision

#define ELEMENT_UNAEXP_PLUS                ExpressionUnaryPlus
#define ELEMENT_UNAEXP_MINUS               ExpressionUnaryMinus
#define ELEMENT_UNAEXP_NOT                 ExpressionUnaryNot

#define ELEMENT_FINEXP_VARIABLE            ExpressionVariable
#define ELEMENT_FINEXP_FUNCTION_CALL       ExpressionFunctionCall
#define ELEMENT_FINEXP_PARENTHESIS         ExpressionParenthesis
#define ELEMENT_IMMEXP_ARRAY               ExpressionImmediateArray
#define ELEMENT_IMMEXP_SET                 ExpressionImmediateSet
#define ELEMENT_LITEXP_LOGIC               ExpressionLiteralLogic
#define ELEMENT_LITEXP_INTEGER             ExpressionLiteralInteger
#define ELEMENT_LITEXP_FLOAT               ExpressionLiteralFloat
#define ELEMENT_LITEXP_CHARACTER           ExpressionLiteralCharacter
#define ELEMENT_LITEXP_STRING              ExpressionLiteralString
#define ELEMENT_SETEXP_EMPTY_SET           ExpressionEmptySet

#define ELEMENT_STDEXP_WRITE               ExpressionStdWrite
#define ELEMENT_STDEXP_READ                ExpressionStdRead
#define ELEMENT_STDEXP_LENGTH              ExpressionStdLength
#define ELEMENT_STDEXP_EXTRACT             ExpressionStdExtract

#endif // CLASSNAMES_H_
