#include "../util/safeuse.h"
#include "../util/declared.h"
#include "../util/tabs.h"
#include "../strings.h"
#include INCLUDE_PARENTHESIS

namespace LEAT{

  void ELEMENT_FINEXP_PARENTHESIS::to_stream(Stream& line, Stream& defs) const{
    line << "(";
    get_expression()->print(line,defs);
    line << ")";
  }

} // namespace LEAT
