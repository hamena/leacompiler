#include "../util/safeuse.h"
#include "../util/declared.h"
#include "../util/tabs.h"
#include "../strings.h"
#include INCLUDE_FUNCTION_CALL

namespace LEAT{

  void ELEMENT_FINEXP_FUNCTION_CALL::to_stream(Stream& line, Stream& defs) const{
    string tail = "";
    if (get_output_arguments().size() == 1){
      tail = ".";
      tail += get_output_arguments().front()->get_id();
    }

    line << get_name() << "(";
    auto exp = get_expressions().cbegin();
    if (exp != get_expressions().cend()){
      line << "{";
      (*exp)->print(line,defs);
      for (exp = ++exp; exp != get_expressions().cend(); ++exp){
	line << ", ";
	(*exp)->print(line,defs);
      }
      line << "}";
    }
    line << ")" << tail;
  }

} // namespace LEAT
