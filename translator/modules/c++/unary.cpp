#include "../util/safeuse.h"
#include "../util/declared.h"
#include "../util/tabs.h"
#include "../strings.h"
#include INCLUDE_UNARY

namespace LEAT{

  void ELEMENT_UNAEXP_PLUS::to_stream(Stream& line, Stream& defs) const{
    line << "+";
    get_expression()->print(line,defs);
  }

  void ELEMENT_UNAEXP_MINUS::to_stream(Stream& line, Stream& defs) const{
    line << "-";
    get_expression()->print(line,defs);
  }

  void ELEMENT_UNAEXP_NOT::to_stream(Stream& line, Stream& defs) const{
    line << "!";
    get_expression()->print(line,defs);
  }

} // namespace LEAT
