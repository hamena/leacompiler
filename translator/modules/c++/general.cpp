#include <iostream> 
#include "../strings.h"
#include "../util/safeuse.h"
#include "../util/tabs.h"
#include "../util/declared.h"

#include INCLUDE_ALGORITHM
#include INCLUDE_TYPE

using namespace std;

namespace LEAT{

  void ELEMENT_ALGORITHM::to_stream(Stream& line, Stream& defs) const{
    if (!this->is_main()){
      string safeAlgName = get_name();
      string headerInput = "void";
      string headerOutput = "void";
      string functionInput = "void";
      string functionOutput = "void";
      bool inputEmpty = get_input_arguments().empty();
      bool outputEmpty = get_output_arguments().empty();

      string inputStruct = get_input_type_name();
      if (!inputEmpty){
	headerInput = "const "+inputStruct+"&";
	functionInput = "const "+ inputStruct+"& "+get_input_name();
	defs << "struct " << inputStruct << "{" << endl;
	Tabs::increment();
	for (auto& it : get_input_arguments()){
	  defs << Tabs::get_tabs();
	  it->get_type()->print(defs, defs);
	  defs << " " << it->get_id() //<< " = " << it->get_type()->get_init_value()
	       << ";" << endl;
	  Declared::save(get_input_name()+"."+it->get_id());
	}
	Tabs::decrement();
	defs << "};" << endl;
      }
    
      string outputStruct = get_output_type_name();
      if (!outputEmpty){
	headerOutput = outputStruct;
	functionOutput = outputStruct;
	defs << "struct " << outputStruct << "{" << endl;
	Tabs::increment();
	for (auto& it : get_output_arguments()){
	  defs << Tabs::get_tabs();
	  it->get_type()->print(defs, defs);
	  defs << " " << it->get_id() //<< " = " << it->get_type()->get_init_value()
	       << ";" << endl;
	  Declared::save(get_output_name()+"."+it->get_id());
	}
	Tabs::decrement();
	defs << "};" << endl;
      }

      defs << headerOutput << " " << safeAlgName << "("
	   << headerInput << ");" << endl;
    
      line << functionOutput << " " << safeAlgName
	   << "(" << functionInput << "){" << endl;

      Tabs::increment();
      if (!outputEmpty)
	line << Tabs::get_tabs() << get_output_type_name() << " "
	     << get_output_name() << ";" << endl;
      for (auto& it : get_instructions()){
	line << Tabs::get_tabs();
	it->print(line,defs);
	line << ";" << endl;
      }
      if (!outputEmpty)
	line << Tabs::get_tabs() << "return " << get_output_name() << ";" << endl;
      Tabs::decrement();

      line << "}" << endl;
    }
    else{ // main
      line << "int main(){" << endl;
      Tabs::increment();
      for (auto& it : get_instructions()){
	line << Tabs::get_tabs();
	it->print(line,defs);
	line << ";" << endl;
      }
      Tabs::decrement();
      line << "}" << endl;
    }
  }

  void ELEMENT_TYPE::to_stream(Stream& line, Stream& defs) const{
    string typeName = get_name();
    int dim = get_dimension();
    const ELEMENT_TYPE *t = get_type();
    string subtypes = "";
    string tail = "";
    for (int i=1; i<dim; ++i){
      tail += "> ";
      subtypes += "<" + typeName;
    }
    
    while(t){
      subtypes += "<" + t->get_name() + "> ";
      t = t->get_type();  
    }
    line << typeName << subtypes << tail;
  }

} // namespace LEAT
