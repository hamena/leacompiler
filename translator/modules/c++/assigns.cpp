#include <iostream>
#include <string>
#include <list>
#include "../util/safeuse.h"
#include "../util/declared.h"
#include "../util/tabs.h"
#include "../strings.h"
#include INCLUDE_SIMPLE_ASSIGN
#include INCLUDE_PARALLEL_ASSIGN
#include INCLUDE_SWAP_ASSIGN

using std::endl;
using std::string;
using std::list;

namespace LEAT{

  void ELEMENT_SIMPLE_ASSIGN::to_stream(Stream& line, Stream& defs) const{
    if (!get_expression()->get_type()->is_empty_set()){
      string access = "";
      Stream auxLine,auxDefs;
      for (auto& it : get_variable()->get_access()){
	it->print(auxLine,auxDefs);
	access += "[" + auxLine.str() + "]";
	auxLine.flush();
      }
      defs << auxDefs.str();
    
      if (!Declared::is_declared(get_variable()->get_id())){
	Declared::save(get_variable()->get_id());
	get_variable()->get_type()->print(line,defs);
	line << " ";
      }
      line << get_variable()->get_id() << access << " = ";
      get_expression()->print(line,defs);
    }
    else{
      if (!Declared::is_declared(get_variable()->get_id())){
	get_variable()->get_type()->print(line,defs);
	line << " " << get_variable()->get_id() << " = {}";
      }else
	line << "//Omitida asignacion de conjunto vacio";
    }
  }

  void ELEMENT_PARALLEL_ASSIGN::to_stream(Stream& line, Stream& defs) const{
    line << "// Asignacion paralela" << endl;
  
    vector<string> temporals;
    if (get_function_call()){
      string tempStruct = SafeUse::get_temp_id();
      auto function = get_function_call();
      line << Tabs::get_tabs() << function->get_output_type_name() << " " << tempStruct << " = ";
      function->print(line,defs);
      line << ";" << endl;
      for (auto& arg : function->get_output_arguments())
	if (!arg->get_type()->is_empty_set())
	  temporals.push_back(tempStruct+"."+arg->get_id());
	else
	  temporals.push_back("");
    }
    else{
      for (auto& exp : get_expressions()){
	if (!exp->get_type()->is_empty_set()){
	  temporals.push_back(SafeUse::get_temp_id());
	  line << Tabs::get_tabs();
	  exp->get_type()->print(line,defs);
	  line << " " << temporals.back() << " = ";
	  exp->print(line,defs);
	  line << ";" << endl;
	}
	else
	  temporals.push_back("");
      }
    }

    int i=0;
    for (auto var=get_variables().cbegin(); var!=get_variables().cend(); ++var){
      if (temporals[i] != ""){
	string access = "";
	Stream auxLine,auxDefs;
	for (auto& it : (*var)->get_access()){
	  it->print(auxLine,auxDefs);
	  access += "[" + auxLine.str() + "]";
	  auxLine.flush();
	}
	defs << auxDefs.str();

	line << Tabs::get_tabs();
	if (!Declared::is_declared((*var)->get_id())){
	  Declared::save((*var)->get_id());
	  (*var)->get_type()->print(line,defs);
	  line << " ";
	}
	line << (*var)->get_id() << access << " = "
	     << temporals[i];
	if (var != --get_variables().cend())
	  line << ";" << endl;
      }
      ++i;
    }
  }

  void ELEMENT_SWAP_ASSIGN::to_stream(Stream& line, Stream& defs) const{
    line << "// Asignacion intercambio" << endl;
    string safeTemp = SafeUse::get_temp_id();
    string safeLeft = get_left_variable()->get_id();
    string safeRight = get_right_variable()->get_id();

    string leftAccess = "";
    Stream leftLine, leftDefs;
    for (auto& it : get_left_variable()->get_access()){
      it->print(leftLine,leftDefs);
      leftAccess += "[" + leftLine.str() + "]";
      leftLine.flush();
    }
    defs << leftDefs.str();

    string rightAccess = "";
    Stream rightLine, rightDefs;
    for (auto& it : get_right_variable()->get_access()){
      it->print(rightLine,rightDefs);
      rightAccess += "[" + rightLine.str() + "]";
      rightLine.flush();
    }
    defs << rightDefs.str();

    safeLeft += leftAccess;
    safeRight += rightAccess;

    line << Tabs::get_tabs();
    get_left_variable()->get_type()->print(line,defs);
    line << " " << safeTemp << " = " << safeLeft << ";" << endl;

    line << Tabs::get_tabs();
    line << safeLeft << " = " << safeRight << ";" << endl;

    line << Tabs::get_tabs();
    line << safeRight << " = " << safeTemp;
  }
} // namespace LEAT
