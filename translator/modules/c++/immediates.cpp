#include "../strings.h"
#include "../util/safeuse.h"
#include "../util/tabs.h"
#include INCLUDE_IMMEDIATE_ARRAY
#include INCLUDE_IMMEDIATE_SET

namespace LEAT{

  void ELEMENT_IMMEXP_ARRAY::to_stream(Stream& line, Stream& defs) const{
    line << "{";
    auto exp = get_expressions().cbegin();
    (*exp)->print(line,defs);

    for (exp=++get_expressions().cbegin(); exp!=get_expressions().cend(); ++exp){
      line << ", ";
      (*exp)->print(line,defs);
    }
    line << "}";
  }

  void ELEMENT_IMMEXP_SET::to_stream(Stream& line, Stream& defs) const{
    line << "{";
    auto exp = get_expressions().cbegin();
    (*exp)->print(line,defs);
    for (exp=++get_expressions().cbegin(); exp!=get_expressions().cend(); ++exp){
      line << ", ";
      (*exp)->print(line,defs);    
    }
    line << "}";
  }

} // namespace LEAT
