#include <iostream>
#include "../util/declared.h"
#include "../util/tabs.h"
#include "../util/safeuse.h"
#include "../strings.h"
#include INCLUDE_STD

using namespace std;

namespace LEAT{

  void ELEMENT_STDEXP_READ::to_stream(Stream& line, Stream& defs) const{
    if (!Declared::is_declared(get_name())){
      Declared::save(get_name());
      defs << "// Funcion estandar leer" << endl;
      defs << "template<typename T>" << endl;
      defs << "void " << get_name() << "(T& e){" << endl;
      defs << "\tcin>>e;" << endl;
      defs << "}" << endl << endl;
    }
  
    line << get_name() << "(";
    get_expressions().front()->print(line,defs);
    line << ")";
  }

  void ELEMENT_STDEXP_WRITE::to_stream(Stream& line, Stream& defs) const{
    if (!Declared::is_declared(get_name())){
      Declared::save(get_name());
      defs << "// Funcion estandar escribir" << endl;
      defs << "template<typename T>" << endl;
      defs << "void " << get_name() << "(const T& e){" << endl;
      defs << "\tcout << e;" << endl;
      defs << "}" << endl << endl;
    }

    line << get_name() << "(";
    get_expressions().front()->print(line,defs);
    line << ")";
  }

  void ELEMENT_STDEXP_LENGTH::to_stream(Stream& line, Stream& defs) const{
    if (!Declared::is_declared(get_name())){
      Declared::save(get_name());
      defs << "// Funcion estandar longitud" << endl;
      defs << "template <typename T>" << endl;
      defs << "int " << get_name() << "(const vector<T>& v){ return v.size(); }" << endl;
    }

    line << get_name() << "(";
    get_expressions().front()->print(line,defs);
    line << ")";
  }

  void ELEMENT_STDEXP_EXTRACT::to_stream(Stream& line, Stream& defs) const{
    if (!Declared::is_declared(get_name())){
      Declared::save(get_name());
      defs << "// Funcion estandar extraer" << endl;
      defs << "template <typename T>" << endl;
      defs << "T " << get_name() << "(set<T>& s){" << endl;
      defs << "\tT e = *s.begin();" << endl;
      defs << "\ts.erase(s.begin());" << endl;
      defs << "\treturn e;" << endl;
      defs << "}" << endl;
    }
  
    line << get_name() << "(";
    get_expressions().front()->print(line,defs);
    line << ")";
  }

} // namespace LEAT
