#include "../util/safeuse.h"
#include "../util/tabs.h"
#include "../util/declared.h"
#include "../strings.h"
#include INCLUDE_RELATIONALS

namespace LEAT{

  void ELEMENT_BINEXP_EQUALS::to_stream(Stream& line, Stream& defs) const{
    bool emptyLeft = get_left_expression()->get_type()->is_empty_set();
    bool emptyRight = get_right_expression()->get_type()->is_empty_set();
    if (!emptyLeft && !emptyRight){
      get_left_expression()->print(line,defs);
      line << " == ";
      get_right_expression()->print(line,defs);
    }else{
      if (emptyLeft){
	get_right_expression()->print(line,defs);
	line << ".empty()";
      }else if (emptyRight){
	get_left_expression()->print(line,defs);
	line << ".empty()"; 
      }
    }
  }

  void ELEMENT_BINEXP_NOT_EQUALS::to_stream(Stream& line, Stream& defs) const{
    bool emptyLeft = get_left_expression()->get_type()->is_empty_set();
    bool emptyRight = get_right_expression()->get_type()->is_empty_set();
    if (!emptyLeft && !emptyRight){
      get_left_expression()->print(line,defs);
      line << " != ";
      get_right_expression()->print(line,defs);
    }else{
      if (emptyLeft){
	line << "!";
	get_right_expression()->print(line,defs);
	line << ".empty()";
      }else if (emptyRight){
	line << "!";
	get_left_expression()->print(line,defs);
	line << ".empty()";
      }
    }
  }

  void ELEMENT_BINEXP_AND::to_stream(Stream& line, Stream& defs) const{
    get_left_expression()->print(line,defs);
    line << " && ";
    get_right_expression()->print(line,defs);
  }

  void ELEMENT_BINEXP_OR::to_stream(Stream& line, Stream& defs) const{
    get_left_expression()->print(line,defs);
    line << " || ";
    get_right_expression()->print(line,defs);
  }

  void ELEMENT_BINEXP_LESSER::to_stream(Stream& line, Stream& defs) const{
    get_left_expression()->print(line,defs);
    line << " < ";
    get_right_expression()->print(line,defs);
  }

  void ELEMENT_BINEXP_LESSER_EQUALS::to_stream(Stream& line, Stream& defs) const{
    get_left_expression()->print(line,defs);
    line << " <= ";
    get_right_expression()->print(line,defs);
  }

  void ELEMENT_BINEXP_GREATER::to_stream(Stream& line, Stream& defs) const{
    get_left_expression()->print(line,defs);
    line << " > ";
    get_right_expression()->print(line,defs);
  }

  void ELEMENT_BINEXP_GREATER_EQUALS::to_stream(Stream& line, Stream& defs) const{
    get_left_expression()->print(line,defs);
    line << " >= ";
    get_right_expression()->print(line,defs);
  }

} // namespace LEAT
