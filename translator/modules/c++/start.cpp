#include <iostream>
#include "../strings.h"
#include INCLUDE_START

using namespace std;

namespace LEAT{

  void ELEMENT_START::to_stream(Stream& line, Stream& defs) const{
    defs << "#include <iostream>" << endl;
    defs << "#include <string>" << endl;
    defs << "#include <vector>" << endl;
    defs << "#include <set>" << endl;
    defs << "using namespace std;" << endl;

    for (auto& alg : get_algorithms())
      alg->print(line,defs);
  }

} // namespace LEAT
