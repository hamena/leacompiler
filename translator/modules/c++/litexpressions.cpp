#include <iostream>
#include "../util/safeuse.h"
#include "../util/tabs.h"
#include "../strings.h"
#include INCLUDE_LITERALS

using std::endl;

namespace LEAT{

  void ELEMENT_LITEXP_LOGIC::to_stream(Stream& line, Stream& defs) const{
    line << get_value();
  }

  void ELEMENT_LITEXP_INTEGER::to_stream(Stream& line, Stream& defs) const{
    line << get_value();
  }

  void ELEMENT_LITEXP_FLOAT::to_stream(Stream& line, Stream& defs) const{
    line << get_value();
  }

  void ELEMENT_LITEXP_CHARACTER::to_stream(Stream& line, Stream& defs) const{
    line << "'" << get_value() << "'";
  }

  void ELEMENT_LITEXP_STRING::to_stream(Stream& line, Stream& defs) const{
    line << "\"" << get_value() << "\"";
  }

} // namespace LEAT
