#include <iostream>
#include "../util/safeuse.h"
#include "../util/declared.h"
#include "../util/tabs.h"
#include "../strings.h"
#include INCLUDE_SET_BASED

using std::endl;

namespace LEAT{

  void ELEMENT_BINEXP_IN::to_stream(Stream& line, Stream& defs) const{
    if (!Declared::is_declared("IN_EXPRESSION")){
      Declared::save("IN_EXPRESSION");
      defs << "template <typename T, typename S>" << endl;
      defs << "bool en(const T& elem, const S& conjunto){" << endl;
      defs << "\treturn conjunto.find(elem) != conjunto.end();" << endl;
      defs << "}" << endl;
    }
    line << "en(";
    get_left_expression()->print(line,defs);
    line << ", ";
    get_right_expression()->print(line,defs);
    line << ")";
  }

  void print_aux_contains(Stream& defs){
    if (!Declared::is_declared("CONTIENE_AUXILIAR")){
      Declared::save("CONTIENE_AUXILIAR");
      defs << "template <typename T>" << endl;
      defs << "bool aux_contiene(const set<T>& c1, const set<T>& c2){" << endl;
      defs << "\tbool res = true;" << endl;
      defs << "\tfor (auto& it : c2)" << endl;
      defs << "\t\tres = res && (c1.find(it) != c1.end());" << endl;
      defs << "\treturn res;" << endl;
      defs << "}" << endl;
    }
  }

  void ELEMENT_BINEXP_CONTAINED_IN::to_stream(Stream& line, Stream& defs) const{
    print_aux_contains(defs);
  
    if (!Declared::is_declared("CONTAINED_IN_EXPRESSION")){
      Declared::save("CONTAINED_IN_EXPRESSION");
      defs << "template <typename T>" << endl;
      defs << "bool contenido_en(const set<T>& c1, const set<T>& c2){" << endl;
      defs << "\treturn aux_contiene(c2,c1) || c1 == c2;" << endl;
      defs << "}" << endl;
    }
    line << "contenido_en(";
    get_left_expression()->print(line,defs);
    line << ", ";
    get_right_expression()->print(line,defs);
    line << ")";
  }

  void ELEMENT_BINEXP_SCONTAINED_IN::to_stream(Stream& line, Stream& defs) const{
    print_aux_contains(defs);
  
    if (!Declared::is_declared("STRICTLY_CONTAINED_IN_EXPRESSION")){
      Declared::save("STRICTLY_CONTAINED_IN_EXPRESSION");
      defs << "template <typename T>" << endl;
      defs << "bool contenido_estricto_en(const set<T>& c1, const set<T>& c2){" << endl;
      defs << "\treturn aux_contiene(c2,c1) && c1 != c2;" << endl;
      defs << "}" << endl;
    }
    line << "contenido_estricto_en(";
    get_left_expression()->print(line,defs);
    line << ", ";
    get_right_expression()->print(line,defs);
    line << ")";
  }

  void ELEMENT_BINEXP_CONTAINS::to_stream(Stream& line, Stream& defs) const{
    print_aux_contains(defs);

    if (!Declared::is_declared("CONTAINS_EXPRESSION")){
      Declared::save("CONTAINS_EXPRESSION");
      defs << "template <typename T>" << endl;
      defs << "bool contiene(const set<T>& c1, const set<T>& c2){" << endl;
      defs << "\treturn aux_contiene(c1,c2) || c1 == c2;" << endl;
      defs << "}" << endl;
    }
    line << "contiene(";
    get_left_expression()->print(line,defs);
    line << ", ";
    get_right_expression()->print(line,defs);
    line << ")";
  }

  void ELEMENT_BINEXP_SCONTAINS::to_stream(Stream& line, Stream& defs) const{
    print_aux_contains(defs);
  
    if (!Declared::is_declared("STRICTLY_CONTAINS_EXPRESSION")){
      Declared::save("STRICTLY_CONTAINS_EXPRESSION");
      defs << "template <typename T>" << endl;
      defs << "bool contiene_estricto(const set<T>& c1, const set<T>& c2){" << endl;
      defs << "\treturn aux_contiene(c1,c2) && c1 != c2;" << endl;
      defs << "}" << endl;
    }
    line << "contiene_estricto(";
    get_left_expression()->print(line,defs);
    line << ", ";
    get_right_expression()->print(line,defs);
    line << ")";
  }

  void ELEMENT_BINEXP_UNION::to_stream(Stream& line, Stream& defs) const{
    bool emptyLeft = get_left_expression()->get_type()->is_empty_set();
    bool emptyRight = get_right_expression()->get_type()->is_empty_set();
    if (emptyLeft)
      get_right_expression()->print(line,defs);
    else if (emptyRight)
      get_left_expression()->print(line,defs);
    else if (!emptyLeft && !emptyRight){
      if (!Declared::is_declared("UNION_EXPRESSION")){
	Declared::save("UNION_EXPRESSION");
	defs << "template <typename T>" << endl;
	defs << "set<T> opunion(const set<T>& c1, const set<T>& c2){" << endl;
	defs << "\tset<T> res = c1;" << endl;
	defs << "\tfor (auto& it : c2)" << endl;
	defs << "\t\t res.insert(it);" << endl;
	defs << "\treturn res;" << endl;
	defs << "}" << endl;
      }
      line << "opunion(";
      get_left_expression()->print(line,defs);
      line << ", ";
      get_right_expression()->print(line,defs);
      line << ")";
    }
  }

  void ELEMENT_BINEXP_INTERSECTION::to_stream(Stream& line, Stream& defs) const{
    bool emptyLeft = get_left_expression()->get_type()->is_empty_set();
    bool emptyRight = get_right_expression()->get_type()->is_empty_set();
    if (emptyLeft)
      get_right_expression()->print(line,defs);
    else if (emptyRight)
      get_left_expression()->print(line,defs);
    else if (!emptyLeft && !emptyRight){
      if (!Declared::is_declared("INTERSECTION_EXPRESSION")){
	Declared::save("INTERSECTION_EXPRESSION");
	defs << "template <typename T>" << endl;
	defs << "set<T> opinterseccion(const set<T>& c1, const set<T>& c2){" << endl;
	defs << "\tset<T> res;" << endl;
	defs << "\tfor (auto& e : c1){" << endl;
	defs << "\t\tif (c2.find(e) != c2.end())" << endl;
	defs << "\t\t\tres.insert(e);" << endl;
	defs << "\t}" << endl;
	defs << "\tfor (auto& e : c2){" << endl;
	defs << "\t\tif (c1.find(e) != c1.end())" << endl;
	defs << "\t\t\tres.insert(e);" << endl;
	defs << "\t}" << endl;
	defs << "\treturn res;" << endl;
	defs << "}" << endl;
      }
      line << "opinterseccion(";
      get_left_expression()->print(line,defs);
      line << ", ";
      get_right_expression()->print(line,defs);
      line << ")";
    }
  }

} // namespace LEAT
