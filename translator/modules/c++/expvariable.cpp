#include <iostream>
#include "../strings.h"
#include "../util/safeuse.h"
#include "../util/tabs.h"
#include "../util/declared.h"
#include INCLUDE_VARIABLE

using std::endl;

namespace LEAT{

  void ELEMENT_FINEXP_VARIABLE::to_stream(Stream& line, Stream& defs) const{
    string access = "";
    Stream auxDefs;
    for (auto& it : get_access()){
      Stream auxLine;
      it->print(auxLine,auxDefs);
      access += "[" + auxLine.str() + " - 1]";
      auxLine.flush();
    }
    defs << auxDefs.str();
    line << get_id() << access;
  }
  
} // namespace LEAT
