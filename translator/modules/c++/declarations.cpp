#include <iostream>
#include "../strings.h"
#include "../util/safeuse.h"
#include "../util/tabs.h"
#include INCLUDE_VARIABLE_DECLARATION
#include INCLUDE_ARRAY_DECLARATION

using namespace std;

namespace LEAT{

  void ELEMENT_VARIABLE_DECLARATION::to_stream(Stream& line, Stream& defs) const
  {
    string initValue =  get_type()->get_init_value();
    line << "// Declaracion/es de variable" << endl;
    line << Tabs::get_tabs();
    get_type()->print(line,defs);
    line << " " << get_ids().front() << "=" << initValue;
    for (auto& it= ++get_ids().begin(); it != get_ids().end(); ++it)
      line << ", " << *it << "=" << initValue;
  }

  void ELEMENT_ARRAY_DECLARATION::to_stream(Stream& line, Stream& defs) const
  {
    line << "// Declaracion/es de array" << endl;
    for (auto it=get_ids().begin(); it!=get_ids().end(); ++it){
      string tail = ")";
      line << Tabs::get_tabs();
      auto t = *get_type();
      t.print(line,defs);
      line << *it << "(";
      get_dimensions().front()->print(line,defs);
      Stream auxLine, auxDefs;
      for (auto& dim = ++get_dimensions().cbegin(); dim != get_dimensions().cend(); ++dim){
	tail += ")";
	line << ",";
	t.set_dimension(t.get_dimension()-1);
	t.print(line,defs);
	(*dim)->print(auxLine,auxDefs);
	line << "(" << auxLine.str();
	auxLine.flush();
      }
      line << tail;
      if (it != --get_ids().end())
	line << ";" << endl;
      defs << auxDefs.str();
      auxDefs.flush();
    }
  }
} // namespace LEAT
