#include <iostream>
#include "../util/safeuse.h"
#include "../util/tabs.h"
#include "../strings.h"
#include INCLUDE_CONDITIONAL
#include INCLUDE_LOOP_WHILE
#include INCLUDE_LOOP_REPEAT
#include INCLUDE_LOOP_FROM
#include INCLUDE_LOOP_FOR_ALL

using std::endl;

namespace LEAT{

  void ELEMENT_CONDITIONAL::to_stream(Stream& line, Stream& defs) const{
    line << "// condicional" << endl;

    line << Tabs::get_tabs() << "if (";
    get_expression()->print(line,defs);
    line << "){" << endl;
    Tabs::increment();
    for (auto& it : get_instructions()){
      line << Tabs::get_tabs();
      it->print(line,defs);
      line << ";" << endl;
    }
    Tabs::decrement();
    line << Tabs::get_tabs() << "}";
  
    if (exists_else()){
      line << endl << Tabs::get_tabs() << "else {" << endl;
      Tabs::increment();
      for (auto& it : get_else_instructions()){
	line << Tabs::get_tabs();
	it->print(line,defs);
	line << ";" << endl;
      }
      Tabs::decrement();
      line << Tabs::get_tabs() << "}";
    }
  }

  void ELEMENT_LOOP_WHILE::to_stream(Stream& line, Stream& defs) const{
    line << "// bucle mientras" << endl;

    line << Tabs::get_tabs() << "while (";
    get_expression()->print(line,defs);
    line << "){" << endl;
    Tabs::increment();
    for (auto& it : get_instructions()){
      line << Tabs::get_tabs();
      it->print(line,defs);
      line << ";" << endl;
    }
    Tabs::decrement();
    line << Tabs::get_tabs() << "}";
  }

  void ELEMENT_LOOP_REPEAT::to_stream(Stream& line, Stream& defs) const{
    line << "// bucle repite" << endl;
    string until = "";
    if (is_until())
      until = "!";
  
    line << Tabs::get_tabs() << "do{" << endl;
    Tabs::increment();
    for(auto& it : get_instructions()){
      line << Tabs::get_tabs();
      it->print(line,defs);
      line << ";" << endl;
    }
    Tabs::decrement();
    line << Tabs::get_tabs() << "}while (" << until;
    get_expression()->print(line,defs);
    line << ")";
  }

  void ELEMENT_LOOP_FROM::to_stream(Stream& line, Stream& defs) const{
    line << "// bucle desde" << endl;
    line << Tabs::get_tabs() << "for (";
    get_assign()->print(line,defs);
    line << "; ";
    if (is_numeric()){
      line << get_assign()->get_variable()->get_id() << " != ";
      get_expression()->print(line,defs);
      line << "; ";

      if (get_increment_expression()){
	if (is_decrement()){
	  line << get_assign()->get_variable()->get_id() << " -= ";
	  get_increment_expression()->print(line,defs);
	}
	else{
	  line << get_assign()->get_variable()->get_id() << " += ";
	  get_increment_expression()->print(line,defs);
	}
      }
      else
	line << "++" << get_assign()->get_variable()->get_id();
    }
    else{
      get_expression()->print(line,defs);
      line << "; ";
      get_with_assign()->print(line,defs);
    }
    line << "){" << endl;
    Tabs::increment();
    for (auto& it : get_instructions()){
      line << Tabs::get_tabs();
      it->print(line,defs);
      line << ";" << endl;
    }
    Tabs::decrement();
    line << Tabs::get_tabs() << "}";
  }

  void ELEMENT_LOOP_FOR_ALL::to_stream(Stream& line, Stream& defs) const{
    line << "for (auto& " << get_iterator()->get_id() << " : ";
    get_expression()->print(line,defs);
    line << "){" << endl;

    Tabs::increment();
    for (auto& it : get_instructions()){
      line << Tabs::get_tabs();
      it->print(line,defs);
      line << ";" << endl;
    }
    Tabs::decrement();
  
    line << Tabs::get_tabs() << "}";  
  }
} // namespace LEAT
