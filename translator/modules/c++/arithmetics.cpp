#include <iostream>
#include "../util/safeuse.h"
#include "../util/declared.h"
#include "../util/tabs.h"
#include "../strings.h"
#include INCLUDE_ARITHMETICS

using std::endl;

namespace LEAT{

  void ELEMENT_BINEXP_PLUS::to_stream(Stream& line, Stream& defs) const{
    get_left_expression()->print(line,defs);
    line << " + ";
    get_right_expression()->print(line,defs);
  }

  void ELEMENT_BINEXP_MINUS::to_stream(Stream& line, Stream& defs) const{
    bool setLeft = get_left_expression()->get_type()->is_set();
    bool setRight = get_right_expression()->get_type()->is_set();
    if (!setLeft && !setRight){
      get_left_expression()->print(line,defs);
      line << " - ";
      get_right_expression()->print(line,defs);
    }
    else{
      if (!Declared::is_declared("MINUS_EXPRESSION")){
	Declared::save("MINUS_EXPRESSION");
	defs << "template <typename T>" << endl;
	defs << "set<T> opminus(const set<T>& c1, const set<T>& c2){" << endl;
	defs << "\tset<T> res = c1;" << endl;
	defs << "\tfor (auto& it : c2)" << endl;
	defs << "\t\tres.erase(it);" << endl;
	defs << "\treturn res;" << endl;
	defs << "}" << endl;
      }

      line << "opminus(";
      get_left_expression()->print(line,defs);
      line << ", ";
      get_right_expression()->print(line,defs);
      line << ")";
    }
  }

  void ELEMENT_BINEXP_SLASH::to_stream(Stream& line, Stream& defs) const{
    get_left_expression()->print(line,defs);
    line << " / ";
    get_right_expression()->print(line,defs);
  }

  void ELEMENT_BINEXP_STAR::to_stream(Stream& line, Stream& defs) const{
    get_left_expression()->print(line,defs);
    line << " * ";
    get_right_expression()->print(line,defs);
  }

  void ELEMENT_BINEXP_MODULE::to_stream(Stream& line, Stream& defs) const{
    get_left_expression()->print(line,defs);
    line << " % ";
    get_right_expression()->print(line,defs);
  }

  void ELEMENT_BINEXP_DIVISION::to_stream(Stream& line, Stream& defs) const{
    line << "(int)(";
    get_left_expression()->print(line,defs);
    line << " / ";
    get_right_expression()->print(line,defs);
    line << ")";
  }
}
