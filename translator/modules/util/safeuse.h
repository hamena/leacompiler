#ifndef SAFE_USE_H_
#define SAFE_USE_H_

#include <map>
#include <set>
#include <string>
#include <vector>

using std::map;
using std::set;
using std::string;
using std::vector;

namespace LEAT{

  /** Clase auxiliar empleada para generar identificadores seguros que no van a causar
   * conflictos en el código final. */
  class SafeUse{
  public:
    /** Obtienes una nueva id a partir de un identificador, opcionalmente se le proporciona un
     * prefijo.
     * @return nuevo identificador. */
    static string get_id(const string&, const string& p="");
    /** Obtienes un nuevo identificador sin necesidad de otro identificador, es usado para
     * crear variables temporales en el código resultante.
     * @return nuevo identificador. */
    static string get_temp_id();
    /** Guarda argumentos y además obtienes su id correspondiente.
     * @return nuevo identificador. */
    static string save_arg(const string&, const string&);
    /** Comprueba si un identificador ya existe. */
    static bool exists(const string&);
    /** Guarda un nuevo prefijo para los identificadores. */
    static void set_prefix(const string&);
    /** Guarda un nuevo separador para los identificadores. */
    static void set_detacher(char);
  private:
    /** Definicion de tipo de tabla de indentificadores. */
    typedef map<string,string> IDTable; 
    static IDTable idtable; ///< Tabla de identificadores.
    static string prefix;   ///< Prefijo de identificadores.
    static char detacher;   ///< Separador de identificadores.
    static int counter;     ///< Contador privado.

    /** Función auxiliar para codificar identificadores. */
    static string code_id(string);
    /** Función auxiliar para trocear cadenas según un token. */
    static vector<string> &split(const string &s, char delim, vector<string> &elems);
    /** Funcion auxiliar para trocear cadenas según un token. */
    static vector<string> split(const string &s, char delim);
  };

} // namespace LEAT
  
#endif // SAFE_USE_H_
