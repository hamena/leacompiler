#ifndef TABS_H_
#define TABS_H_

#include <string>

using std::string;

namespace LEAT{

  /** Clase auxiliar empleada para facilitar la indentación en la 
   * salida del código resultante. */
  class Tabs{
  public:
    /** Obtienes una cadena con las tabulaciones necesarias.
     * @return cadena rellena de tabulaciones. */
    static string get_tabs();
    /** Incrementa el número de tabulaciones a escribir. */
    static void increment();
    /** Decrementa el número de tabulaciones a escribir. */
    static void decrement();
  private:
    static int counter; ///< Contador privado.
  };

} // namespace LEAT
  
#endif // TABS_H_
