#ifndef DECLARED_H_
#define DECLARED_H_

#include <set>
#include <string>

using std::set;
using std::string;

namespace LEAT{

  /** Clase auxiliar utilizada para saber si algo ha sido declarado con anterioridad. */
  class Declared{
  public:
    /** Guarda un identificador en la lista. */
    static void save(const string&);
    /** Comprueba si el identificador proporcionado existe en la lista o no.
     * @return Verdadero si está en la lista, falso en otro caso. */
    static bool is_declared(const string&);
  private:
    static set<string> IDset; ///< Conjunto de identificadores.
  };

} // namespace LEAT
  
#endif // DECLARED_H_
