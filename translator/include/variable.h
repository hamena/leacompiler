#ifndef VARIABLE_H_
#define VARIABLE_H_

#include <string>
#include <vector>
#include "tinyxml2.h"
#include "elements/type.h"
#include "elements/expressions/expression.h"
#include "../modules/strings.h"

using std::string;
using std::vector;

namespace LEAT{

  /** Clase contenedora variable. Esta clase sirve de contenedor y de base para los
   * elementos: expresión variable y argumentos.
   * @see ELEMENT_FINEXP_VARIABLE 
   * @see Argument
   */
  class Variable{
  public:
    /** Constructor de variable. Crea una variable a partir de un nodo XML. */
    Variable(XMLElement*);
    /** Constructor de copia. */
    Variable(const Variable&);
    /** Constructor de movimiento. */
    Variable(Variable&&);
    /** Obtienes el identificador de la variable.
     *  @return cadena identificador. */
    string get_id() const { return id; }
    /** Obtienes las expresiones que forman el acceso a la variable.
     *  @return vector de expresiones. */
    const vector<Expression*>& get_access() const{ return access; }
    /** Obtienes el tipo de la variable.
     *  @return puntero constante a elemento tipo. */
    const ELEMENT_TYPE* get_type() const { return type; }
    /** Obtienes el tipo de la variable.
     *  @return puntero a elemento tipo. */
    ELEMENT_TYPE* get_type(){ return type; }
    /** Obtienes la dimensión de la variable contando con el acceso si lo hubiese.
     *  @return dimension menos el acceso a la variable. */
    int get_dimension() const{ return type->get_dimension() - access.size(); }

    /** Destructor virtual de variable. */
    virtual ~Variable();

    /** Estructura función comparadora. */
    struct Comparator {
      bool operator()(const Variable* a1, const Variable* a2) const{
        return a1->order < a2->order;
      }
    };
  protected:
    /** Constructor protegido para crear argumentos. */
    Variable(XMLElement*, const string&);
  private:
    /** Método auxiliar que describe el comportamiento repetido entre constructores. */
    void construct(XMLElement*);
    /** Atributo para establecer una relación de orden entre variables. */
    int order;
    /** Identificador de la variable. */
    string id;
    /** Expresiones que conforman el acceso a la variable si lo hubiese. */
    vector<Expression*> access;
    /** Puntero al tipo de la variable. */
    ELEMENT_TYPE *type;
  };

} // namespace LEAT

#endif // VARIABLE_H_
