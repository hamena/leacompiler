#ifndef INSTRUCTIONS_HANDLER_H_
#define INSTRUCTIONS_HANDLER_H_

#include "tinyxml2.h"
#include "elements/instruction.h"

using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase auxiliar para identificar las instrucciones. Esta clase es un multiplexador
   * de elementos instrucción a partir de un nodo XML. */
  class InstructionsHandler{
  public:
    /** Función principal para identificar y crear un elemento instrucción. */
    static Instruction* new_instruction(XMLElement*);
  };

} // namespace LEAT
  
#endif // INSTRUCTIONS_HANDLER_H_
