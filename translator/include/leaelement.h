#ifndef LEAELEMENT_H_
#define LEAELEMENT_H_

#include <sstream>
#include <string>

using std::stringstream;
using std::string;
using std::to_string;

typedef stringstream Stream;

namespace LEAT{

  /** Clase base de todos los elementos LEA. Esta clase registra el comportamiento
   * mínimo esperado por todos los elementos del lenguaje que es la salida. */
  class LEAElement{
  public:
    /** Función print. Esta función gestiona los búfers de escritura para mantener
     * la coherencia entre la jerarquía de clases.
     * @param line búfer para escribir en la misma línea.
     * @param defs búfer para escribir en la zona de definiciones. */
    virtual void print(Stream& line, Stream& defs) const{
      to_stream(line,defs);
    }
    /** Destructor virtual de elementos. */
    virtual ~LEAElement() {};
  private:
    /** Función virtual pura que definirá la escritura en los búfers. */
    virtual void to_stream(Stream&, Stream&) const = 0;
  };

} // namespace LEAT
  
#endif // LEAELEMENT_H_
