#ifndef BASE_INSTRUCTION_H_
#define BASE_INSTRUCTION_H_

#include "../leaelement.h"

namespace LEAT{

  /** Clase base instrucción. Esta clase agrupa a todas sus derivadas como 
   * instrucciones de LEA.
   * @see LEAElement */
  class Instruction : public LEAElement{
  public:
    /** Constructor de instrucción. */
    Instruction(int o) : order(o) {}

    /** Estructura función auxiliar usada para establecer un orden entre instrucciones. */
    struct Comparator {
      bool operator()(const Instruction* i1, const Instruction* i2) const{
        return i1->order < i2->order;
      }
    };
  private:
    int order;  ///< Indica la posición en la lista de instrucciones a la que pertenezca.
  };

} // namespace LEAT

#endif // BASE_INSTRUCTION_H_
