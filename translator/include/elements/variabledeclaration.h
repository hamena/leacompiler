#ifndef ELEMENT_VARIABLE_DECLARATION_H_
#define ELEMENT_VARIABLE_DECLARATION_H_

#include "instruction.h"
#include "type.h"
#include "../tinyxml2.h"
#include "../../modules/strings.h"
#include <vector>
#include <string>
#include <sstream>

using std::vector;
using std::string;
using std::stringstream;

namespace LEAT{

  /** Clase elemento declaración de variable. Esta clase representa el elemento declaración
   *  de variable, por cada declaración de variable definida en código LEA se crea un 
   *  nuevo elemento de declaración de variable. */
  class ELEMENT_VARIABLE_DECLARATION : public Instruction{
  public:
    /** Constructor de elemento declaración de variable a partir de un nodo XML. */
    ELEMENT_VARIABLE_DECLARATION(XMLElement*);
    /** Destructor de elemento declaración de variable. */
    ~ELEMENT_VARIABLE_DECLARATION();

    /** Obtienes la lista de identificadores de nuevas variables. 
     *  @return vector de identificadores. */
    const vector<string>& get_ids() const{ return ids; }
    /** Obtienes el tipo de las nuevas variables.
     *  @return puntero constante a elemento tipo. */
    const ELEMENT_TYPE* get_type() const{ return type; }
  private:
    void to_stream(Stream& line, Stream& defs) const;
    vector<string> ids; ///< Identificadores de nuevas variables.
    ELEMENT_TYPE *type; ///< Tipo de las nuevas variables.
  };

} // namespace LEAT
  
#endif // ELEMENT_VARIABLE_DECLARATION_H_
