
#ifndef ELEMENT_ARRAY_DECLARATION_H_
#define ELEMENT_ARRAY_DECLARATION_H_

#include "instruction.h"
#include "type.h"
#include "expressions/expression.h"
#include "../tinyxml2.h"
#include "../../modules/strings.h"
#include <sstream>
#include <vector>
#include <string>

using namespace tinyxml2;
using std::stringstream;
using std::vector;
using std::string;

namespace LEAT{

  /** Clase elemento declaración de array. Esta clase representa la instrucción LEA
   * de declaración de array. Por cada array declarado en código LEA se crea un elemento
   * declaración de array.
   * @see Instruction */
  class ELEMENT_ARRAY_DECLARATION : public Instruction{
  public:
    /** Constructor de elemento declaración de array a partir de un nodo XML. */
    ELEMENT_ARRAY_DECLARATION(XMLElement*);
    /** Destructor de elemento declaración de array. */
    ~ELEMENT_ARRAY_DECLARATION();

    /** Obtienes la lista de identificadores de nuevos arrays.
     *  @return vector de identificadores. */
    const vector<string>& get_ids() const{ return ids; }
    /** Obtienes las dimensiones que han de tener los arrays declarados.
     *  @return vector de punteros a expresión, dimensiones. */
    const vector<Expression*>& get_dimensions() const{ return sizes; }
    /** Obtienes el tipo de los arrays que se están declarando.
     *  @return puntero constante al tipo. */
    const ELEMENT_TYPE* get_type() const{ return type; }
  private:
    void to_stream(Stream& line, Stream& defs) const;
    vector<string> ids;        ///< Identificadores de los nuevos arrays.
    vector<Expression*> sizes; ///< Dimensión de los nuevos arrays.
    ELEMENT_TYPE *type;        ///< Subtipo del array.
  };

} // namespace LEAT
  
#endif // ELEMENT_ARRAY_DECLARATION_H_
