#ifndef ELEMENT_PARALLEL_ASSIGN_H_
#define ELEMENT_PARALLEL_ASSIGN_H_

#include <vector>
#include "instruction.h"
#include "expressions/expression.h"
#include "expressions/functioncall.h"
#include "../tinyxml2.h"
#include "../variable.h"
#include "../../modules/strings.h"

using std::vector;
using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase elemento asignación paralela. Esta clase representa la instrucción asignación
   * paralela, por cada asignación paralela realizada en código LEA se crea un nuevo
   * elemento asignación paralela. */
  class ELEMENT_PARALLEL_ASSIGN : public Instruction{
  public:
    /** Constructor de elemento asignación paralela a partir de un nodo XML. */
    ELEMENT_PARALLEL_ASSIGN(XMLElement*);
    /** Destructor del elemento asignación paralela. */
    ~ELEMENT_PARALLEL_ASSIGN();
    /** Obtienes las variables que van a ser asignadas.
     *  @return vector ordenado de punteros a variable. */
    const vector<Variable*>& get_variables() const{ return variables; }
    /** Obtienes las expresiones que van a ser asignadas a las variables.
     *  @return vector ordenado de punteros a expresión. */
    const vector<Expression*>& get_expressions() const{ return expressions; }
    /** Obtienes la llamada a función que va a ser asignada a las variables.
     *  @return puntero al elemento llamada a funcion, nullptr si no existe tal llamada. */
    const ELEMENT_FINEXP_FUNCTION_CALL* get_function_call() const{ return functionCall; }
  private:
    void to_stream(Stream&, Stream&) const;
  
    vector<Variable*> variables;                ///< Variables que serán asignadas.
    vector<Expression*> expressions;            ///< Expresiones a asignar.
    ELEMENT_FINEXP_FUNCTION_CALL* functionCall; ///< LLamada a función si existe.
  };

} // namespace LEAT
  
#endif // ELEMENT_PARALLEL_ASSIGN_H_

