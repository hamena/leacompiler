#ifndef ELEMENT_CONDITIONAL_H_
#define ELEMENT_CONDITIONAL_H_

#include <vector>
#include "expressions/expression.h"
#include "instruction.h"
#include "../tinyxml2.h"
#include "../../modules/strings.h"

using std::vector;
using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase elemento condicional. Esta clase representa la estructura de control if (si), 
   * por cada condicional en código LEA se crea un elemento condicional.
   * @see Instruction */
  class ELEMENT_CONDITIONAL : public Instruction{
  public:
    /** Constructor de elemento condicional a partir de un nodo XML. */
    ELEMENT_CONDITIONAL(XMLElement*);
    /** Destructor del elemento condicional. */
    ~ELEMENT_CONDITIONAL();

    /** Obtienes la expresión lógica del condicional.
     *  @return puntero constante a la expresión. */
    const Expression* get_expression() const{ return expression; }
    /** Obtienes la lista de instrucciones del cuerpo del condicional. 
     *  @return vector ordenado de punteros a instrucción. */
    const vector<Instruction*>& get_instructions() const{ return instructions; }
    /** Obtienes la lista de instrucciones del cuerpo del else. 
     *  @return vector ordenado de punteros a instrucción. */
    const vector<Instruction*>& get_else_instructions() const{ return elseInstructions; }
    /** Comprueba si el condicional tiene else. */
    const bool exists_else() const{ return !elseInstructions.empty(); }
  private:
    void to_stream(Stream&, Stream&) const;
    Expression *expression;                ///< Puntero a expresión lógica del if.
    vector<Instruction*> instructions;     ///< Instrucciones del cuerpo del if.
    vector<Instruction*> elseInstructions; ///< Instrucciones del cuerpo del else.
  };

} // namespace LEAT
  
#endif // ELEMENT_CONDITIONAL_H_
