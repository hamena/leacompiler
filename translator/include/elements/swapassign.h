#ifndef ELEMENT_SWAP_ASSIGN_H_
#define ELEMENT_SWAP_ASSIGN_H_

#include <vector>
#include "instruction.h"
#include "../variable.h"
#include "../tinyxml2.h"
#include "../../modules/strings.h"

using std::vector;
using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase elemento asignación intercambio. Esta clase representa el elemento asignación
   *  intercambio, por cada asignación itercambio definida en código LEA se crea un 
   *  nuevo elemento asignación intercambio. */
  class ELEMENT_SWAP_ASSIGN : public Instruction{
  public:
    /** Constructor elemento asiganción intercambio a partir de un nodo XML. */
    ELEMENT_SWAP_ASSIGN(XMLElement*);
    /** Destructor elemento asignación intercambio. */
    ~ELEMENT_SWAP_ASSIGN();
    /** Obtienes la variable izquierda participante en el intercambio.
     *  @return puntero constante a la variable izquierda. */
    const Variable* get_left_variable() const{ return variables.front(); }
    /** Obtienes la variable derecha participante en el intercambio.
     *  @return puntero constante a la variable derecha. */
    const Variable* get_right_variable() const{ return *(++variables.begin()); }
  private:
    void to_stream(Stream&, Stream&) const;
    vector<Variable*> variables; ///< Variables participantes en la asignación itercambio.
  };

} // namespace LEAT
  
#endif // ELEMENT_SWAP_ASSIGN_H_
