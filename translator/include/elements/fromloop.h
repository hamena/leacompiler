#ifndef ELEMENT_LOOP_FROM_H_
#define ELEMENT_LOOP_FROM_H_

#include <vector>
#include "instruction.h"
#include "simpleassign.h"
#include "expressions/expression.h"
#include "../tinyxml2.h"
#include "../../modules/strings.h"

using std::vector;
using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase elemento bucle desde. Esta clase representa el elemento bucle desde, por cada
   * bucle desde programado en código LEA se crea un elemento bucle desde. */
  class ELEMENT_LOOP_FROM : public Instruction{
  public:
    /** Constructor bucle desde a partir de un nodo XML. */
    ELEMENT_LOOP_FROM(XMLElement*);
    /** Destructor bucle desde. */
    ~ELEMENT_LOOP_FROM();

    /** Obtienes la lista de instrucciones del cuerpo del bucle.
     *  @return vector ordenado de punteros a instrucción. */
    const vector<Instruction*>& get_instructions() const{ return instructions; }
    /** Obtienes la primera asignación del bucle desde.
     *  @return puntero constante a elemento asignación. */
    const ELEMENT_SIMPLE_ASSIGN* get_assign() const{ return assign; }
    /** Obtienes la asignación de la clausula 'con' del bucle desde.
     *  @return puntero constante a elemento asignación.*/
    const ELEMENT_SIMPLE_ASSIGN* get_with_assign() const{ return withAssign; }
    /** Obtienes la expresión lógica que representa la condición de parada.
     *  @return puntero constante a expresión. */
    const Expression* get_expression() const{ return expression; }
    /** Obtienes la expresión correspondiente al incremento si existe.
     *  @return puntero constante a expresión. */
    const Expression* get_increment_expression() const{ return incExpression; }
    /** Comprueba si el bucle desde se trata de un bucle desde numérico. 
     *  @return Verdadero si es numerico, falso en otro caso. */
    bool is_numeric() const{ return numericFlag; }
    /** Comprueba si el bucle desde numérico es de decremento.
     *  @return Verdadero si existe decremento, falso en otro caso. */
    bool is_decrement() const{ return decrementFlag; }
  private:
    void to_stream(Stream&, Stream&) const;
    vector<Instruction*> instructions; ///< Instrucciones del cuerpo del bucle.
    ELEMENT_SIMPLE_ASSIGN *assign;     ///< Primera asignación
    ELEMENT_SIMPLE_ASSIGN *withAssign; ///< Asignación del bucle general, clausula 'con'
    Expression *expression;            ///< Expresión de la condición de parada.
    Expression *incExpression;         ///< Expresión de incremento si es que existe.
    bool numericFlag;                  ///< Bandera de bucle desde numérico.
    bool decrementFlag;                ///< Bandera de buclde desde numérico con decremento.
  };

} // namespace LEAT
  
#endif // ELEMENT_LOOP_FROM_H_
