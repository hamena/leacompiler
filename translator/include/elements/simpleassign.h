#ifndef ELEMENT_SIMPLE_ASSIGN_H_
#define ELEMENT_SIMPLE_ASSIGN_H_

#include "instruction.h"
#include "expressions/expression.h"
#include "../tinyxml2.h"
#include "../variable.h"
#include "../../modules/strings.h"

using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase elemento asignación simple. Esta clase representa el elemento asignación simple,
   * por cada asignación simple definida en código LEA se crea un nuevo elemento asignación
   * simple. */
  class ELEMENT_SIMPLE_ASSIGN : public Instruction{
  public:
    /** Constructor del elemento asignación simple a partir de un nodo XML. */
    ELEMENT_SIMPLE_ASSIGN(XMLElement*);
    /** Destructor del elemento asignación simple. */
    ~ELEMENT_SIMPLE_ASSIGN();
    /** Obtienes la variable que será asignada.
     *  @return puntero constante a variable. */
    const Variable* get_variable() const{ return variable; }
    /** Obtienes la expresión que será asignada a la variable.
     *  @return puntero constante a expresión. */
    const Expression* get_expression() const{ return expression; }
  private:
    void to_stream(Stream&, Stream&) const;
  
    Variable *variable;     ///< Variable que será asignada.
    Expression *expression; ///< Expresión que se asignará a la variable.
  
  };

} // namespace LEAT

#endif // ELEMENT_SIMPLE_ASSIGN_H_
