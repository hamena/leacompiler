#ifndef BINARY_EXPRESSION_H_
#define BINARY_EXPRESSION_H_

#include "expression.h"
#include "../../tinyxml2.h"
#include "../../../modules/strings.h"

using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase base expresión binaria. Esta clase sirve para agrupar todas las derivadas
   * como expresiones binarias.
   * @see Expression */
  class BinaryExpression : public Expression{
  public:
    /** Constructor de expresión binaria a partir de un nodo XML. */
    BinaryExpression(XMLElement*);
    /** Destructor de expresión binaria. */
    ~BinaryExpression();

    /** Obtienes la expresión de la izquierda.
     *  @return puntero constante a expresión izquierda. */
    const Expression* get_left_expression() const{ return leftExpression; }
    /** Obtienes la expresión de la derecha.
     *  @return puntero constante a expresión derecha. */
    const Expression* get_right_expression() const{ return rightExpression; }
  private:
    Expression *leftExpression;  ///< Expresión de la izquierda.
    Expression *rightExpression; ///< Expresión de la derecha.
  }; 

} // namespace LEAT
  
#endif // BINARY_EXPRESSION_H_
