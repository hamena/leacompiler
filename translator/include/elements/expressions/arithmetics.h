#ifndef ELEMENT_ARITHMETICS_EXPRESSIONS_H_
#define ELEMENT_ARITHMETICS_EXPRESSIONS_H_

#include "binaryexpression.h"
#include "../../tinyxml2.h"
#include "../../../modules/strings.h"

using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase elemento expresión binaria suma.
   * @see BinaryExpression */
  class ELEMENT_BINEXP_PLUS : public BinaryExpression{
  public:
    /** Constructor del elemento expresión binaria suma a partir de un nodo XML. */
    ELEMENT_BINEXP_PLUS(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión binarai suma. */
    ~ELEMENT_BINEXP_PLUS() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión binaria resta. 
   * @see BinaryExpression */
  class ELEMENT_BINEXP_MINUS : public BinaryExpression{
  public:
    /** Constructor elemento expresión binaria resta a partir de un nodo XML. */
    ELEMENT_BINEXP_MINUS(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión binaria resta. */
    ~ELEMENT_BINEXP_MINUS() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión binaria división real. 
   * @see BinaryExpression */
  class ELEMENT_BINEXP_SLASH : public BinaryExpression{
  public:
    /** Constructor del elemento expresión binaria división real a partir de un nodo XML. */
    ELEMENT_BINEXP_SLASH(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión binaria división real. */
    ~ELEMENT_BINEXP_SLASH() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión binaria producto. 
   * @see BinaryExpression */
  class ELEMENT_BINEXP_STAR : public BinaryExpression{
  public:
    /** Constructor del elemento expresión binaria producto a partir de un nodo XML. */
    ELEMENT_BINEXP_STAR(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión binaria producto. */
    ~ELEMENT_BINEXP_STAR() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión binaria módulo. 
   *  @see BinaryExpression */
  class ELEMENT_BINEXP_MODULE : public BinaryExpression{
  public:
    /** Constructor del elemento expresión binaria módulo a partir de un nodo XML. */
    ELEMENT_BINEXP_MODULE(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión binaria módulo. */
    ~ELEMENT_BINEXP_MODULE() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión binaria división entera. 
   * @see BinaryExpression */
  class ELEMENT_BINEXP_DIVISION : public BinaryExpression{
  public:
    /** Constructor del elemento expresión binaria división entera a partir de un nodo XML. */
    ELEMENT_BINEXP_DIVISION(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión binaria división entera. */
    ~ELEMENT_BINEXP_DIVISION() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

} // namespace LEAT
  
#endif // ELEMENT_ARITHMETICS_EXPRESSIONS_H_
