#ifndef ELEMENT_SETEXP_EMPTY_SET_H_
#define ELEMENT_SETEXP_EMPTY_SET_H_

#include "expression.h"
#include "../../tinyxml2.h"
#include "../../../modules/strings.h"
#include "../../../../compiler/include/defstrings.h"

using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase elemento expresión conjunto vacío. 
   * @see Expression */
  class ELEMENT_SETEXP_EMPTY_SET : public Expression{
  public:
    /** Constructor del elemento expresión conjunto vacío a partir de un nodo XML. */
    ELEMENT_SETEXP_EMPTY_SET(XMLElement* elem) :
      Expression(elem->IntAttribute(ATTR_ORDER),
		 elem->FirstChildElement(XMLTAG_EXPRESSION))
    {}
    /** Destructor del elemento expresión conjunto vacío. */
    ~ELEMENT_SETEXP_EMPTY_SET(){}
  private:
    void to_stream(Stream&, Stream&) const;
  };

} // namespace LEAT
  
#endif // ELEMENT_SETEXP_EMPTY_SET_H_
