#ifndef ELEMENT_IMMEDIATE_SET_H_
#define ELEMENT_IMMEDIATE_SET_H_

#include "immediate.h"
#include "../../tinyxml2.h"
#include "../../../modules/strings.h"

using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase elemento expresión conjunto inmediato.
   * @see Immediate */
  class ELEMENT_IMMEXP_SET : public Immediate{
  public:
    /** Constructor de elemento expresión conjunto inmediato a partir de un nodo XML. */
    ELEMENT_IMMEXP_SET(XMLElement* elem) : Immediate(elem) {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

} // namespace LEAT
  
#endif // ELEMENT_IMMEDIATE_SET_H_
