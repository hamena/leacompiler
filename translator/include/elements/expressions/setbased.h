#ifndef SET_BASED_H_
#define SET_BASED_H_

#include "binaryexpression.h"
#include "../../tinyxml2.h"
#include "../../../modules/strings.h"

using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase elemento expresión en. 
   * @see BinaryExpression */
  class ELEMENT_BINEXP_IN : public BinaryExpression{
  public:
    /** Constructor elemento expresión en a partir de un nodo XML. */
    ELEMENT_BINEXP_IN(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión en. */
    ~ELEMENT_BINEXP_IN() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión contenido en. 
   * @see BinaryExpression */
  class ELEMENT_BINEXP_CONTAINED_IN : public BinaryExpression{
  public:
    /** Constructor elemento expresión contenido en a partir de un nodo XML. */
    ELEMENT_BINEXP_CONTAINED_IN(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión contenido en. */
    ~ELEMENT_BINEXP_CONTAINED_IN() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión contenido estricto en. 
   * @see BinaryExpression */
  class ELEMENT_BINEXP_SCONTAINED_IN : public BinaryExpression{
  public:
    /** Constructor elemento expresión contenido estricto en a partir de un nodo XML. */
    ELEMENT_BINEXP_SCONTAINED_IN(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión contenido estricto en. */
    ~ELEMENT_BINEXP_SCONTAINED_IN() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión contiene. 
   * @see BinaryExpression */
  class ELEMENT_BINEXP_CONTAINS : public BinaryExpression{
  public:
    /** Constructor elemento expresión contiene a partir de un nodo XML. */
    ELEMENT_BINEXP_CONTAINS(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión contiene. */
    ~ELEMENT_BINEXP_CONTAINS() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión contiene estricto. 
   * @see BinaryExpression */
  class ELEMENT_BINEXP_SCONTAINS : public BinaryExpression{
  public:
    /** Constructor elemento expresión contiene estricto a partir de un nodo XML. */
    ELEMENT_BINEXP_SCONTAINS(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión contiene estricto. */
    ~ELEMENT_BINEXP_SCONTAINS() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión unión.
   * @see BinaryExpression */
  class ELEMENT_BINEXP_UNION : public BinaryExpression{
  public:
    /** Constructor elemento expresión unión a partir de un nodo XML. */
    ELEMENT_BINEXP_UNION(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión unión. */
    ~ELEMENT_BINEXP_UNION() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión intersección. 
   * @see BinaryExpression */
  class ELEMENT_BINEXP_INTERSECTION : public BinaryExpression{
  public:
    /** Constructor elemento expresión intersección a partir de un nodo XML. */
    ELEMENT_BINEXP_INTERSECTION(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión intersección. */
    ~ELEMENT_BINEXP_INTERSECTION() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

} // namespace LEAT
  
#endif // SET_BASED_H_
