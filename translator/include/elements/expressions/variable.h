#ifndef ELEMENT_FINEXP_VARIABLE_H_
#define ELEMENT_FINEXP_VARIABLE_H_

#include "expression.h"
#include "../type.h"
#include "../../variable.h"
#include "../../tinyxml2.h"
#include "../../../modules/strings.h"

using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase elemento expresión variable. 
   * @see Expression
   * @see Variable */
  class ELEMENT_FINEXP_VARIABLE : public Expression, public Variable{
  public:
    /** Constructor del elemento expresión variable a partir de un nodo XML. */
    ELEMENT_FINEXP_VARIABLE(XMLElement*);
    /** Destructor del elemento expresión variable. */
    ~ELEMENT_FINEXP_VARIABLE();

    /** Obtienes el tipo de la variable.
     * @return puntero constante a elemento tipo. */
    const ELEMENT_TYPE* get_type() const{ return Variable::get_type(); }
    /** Obtienes el tipo de la variable.
     * @return puntero a elemento tipo. */
    ELEMENT_TYPE* get_type(){ return Variable::get_type(); }
    /** Obtienes la dimensión de la variable.
     * @return dimension. */
    int get_dimension() const{ return Variable::get_dimension(); }
  private:
    void to_stream(Stream&, Stream&) const;
  };

} // namespace LEAT
  
#endif // ELEMENT_FINEXP_VARIABLE_H_
