#ifndef ELEMENT_FUNCTION_CALL_H_
#define ELEMENT_FUNCTION_CALL_H_

#include "expression.h"
#include "../instruction.h"
#include "../../basealgorithm.h"
#include "../../argument.h"
#include "../../tinyxml2.h"
#include "../../../modules/strings.h"
#include <vector>
#include <string>

using tinyxml2::XMLElement;
using std::vector;
using std::string;

namespace LEAT{

  /** Clase elemento expresión llamada a función.
   * @see ELEMENT_FINEXP_FUNCTION_CALL */
  class ELEMENT_FINEXP_FUNCTION_CALL :
    public Expression, public Instruction, public BaseAlgorithm{
  public:
    /** Constructor elemento expresión llamada a función a partir de un nodo XML. */
    ELEMENT_FINEXP_FUNCTION_CALL(XMLElement*);
    /** Destructor del elemento expresión llamada a función. */
    ~ELEMENT_FINEXP_FUNCTION_CALL();
    /** Obtienes las expresiones que conforman los argumentos de entrada de la función. 
     *  @return vector de punteros a expresión. */
    const vector<Expression*>& get_expressions() const{ return expressions; }
    /** Obtienes los argumentos de salida de la función.
     *  @return vector de punteros a argumentos. */
    const vector<Argument*>& get_output_arguments() const{ return outputArgs; }
    /** Sobrecarga de la función virtual de expresión.
     * @see Expression */
    bool is_function_call() const{ return true; }
    /** Sobrecarga del método print del elemento LEA.
     * @see LEAElement */
    void print(Stream& line, Stream& defs) const{ Expression::print(line,defs); }
  private:
    void to_stream(Stream&, Stream&) const;
    vector<Expression*> expressions; ///< Parámetros de entrada.
    vector<Argument*> outputArgs;    ///< Argumentos de salida.
  };

} // namespace LEAT

#endif // ELEMENT_FUNCTION_CALL_H_
