#ifndef ELEMENT_PARENTHESIS_H_
#define ELEMENT_PARENTHESIS_H_

#include "expression.h"
#include "../../tinyxml2.h"
#include "../../../modules/strings.h"

using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase elemento expresión paréntesis. */
  class ELEMENT_FINEXP_PARENTHESIS : public Expression{
  public:
    /** Constructor del elemento expresión paréntesis a partir de un nodo XML. */
    ELEMENT_FINEXP_PARENTHESIS(XMLElement*);
    /** Destructor del elemento expresión paréntesis. */
    ~ELEMENT_FINEXP_PARENTHESIS();
    /** Obtienes la expresión que está dentro de los paréntesis.
     *  @return puntero constante a expresión. */
    const Expression* get_expression() const{ return expression; }
  private:
    void to_stream(Stream&, Stream&) const;
    Expression *expression; ///< Expresión interior.
  };

} // namespace LEAT
  
#endif // ELEMENT_PARENTHESIS_H_
