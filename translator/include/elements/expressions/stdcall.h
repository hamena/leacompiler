#ifndef ELEMENT_STD_EXPRESSIONS_H_
#define ELEMENT_STD_EXPRESSIONS_H_

#include "functioncall.h"
#include "../../tinyxml2.h"
#include "../../../modules/strings.h"

using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase elemento llamada estándar leer. */
  class ELEMENT_STDEXP_READ : public ELEMENT_FINEXP_FUNCTION_CALL{
  public:
    /** Constructor elemento llamada estándar leer a partir de un nodo XML. */
    ELEMENT_STDEXP_READ(XMLElement *elem) : ELEMENT_FINEXP_FUNCTION_CALL(elem) {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento llamada estándar escribir. */
  class ELEMENT_STDEXP_WRITE : public ELEMENT_FINEXP_FUNCTION_CALL{
  public:
    /** Constructor elemento llamada estándar escribir a partir de un nodo XML. */
    ELEMENT_STDEXP_WRITE(XMLElement *elem) : ELEMENT_FINEXP_FUNCTION_CALL(elem) {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento llamada estándar longitud. */
  class ELEMENT_STDEXP_LENGTH : public ELEMENT_FINEXP_FUNCTION_CALL{
  public:
    /** Constructor elemento llamada estándar longitud a partir de un nodo XML. */
    ELEMENT_STDEXP_LENGTH(XMLElement *elem) : ELEMENT_FINEXP_FUNCTION_CALL(elem) {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento llamada estándar extraer. */
  class ELEMENT_STDEXP_EXTRACT : public ELEMENT_FINEXP_FUNCTION_CALL{
  public:
    /** Constructor elemento llamada estándar extraer a partir de un nodo XML. */
    ELEMENT_STDEXP_EXTRACT(XMLElement *elem) : ELEMENT_FINEXP_FUNCTION_CALL(elem) {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

} // namespace LEAT
  
#endif // ELEMENT_STD_EXPRESSIONS_H_
