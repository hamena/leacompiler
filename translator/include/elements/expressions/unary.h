#ifndef ELEMENT_UNARY_EXPRESSIONS_H_
#define ELEMENT_UNARY_EXPRESSIONS_H_

#include "expression.h"
#include "../../tinyxml2.h"
#include "../../../modules/strings.h"

using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase expresión unaria. Esta clase se utiliza para agrupar las derivadas en
   * expresiones unarias.
   * @see Expression */
  class UnaryExpression : public Expression{
  public:
    /** Constructor de expresión unaria a partir de un nodo XML. */
    UnaryExpression(XMLElement*);
    /** Destructor de expresión unaria. */
    ~UnaryExpression();
    /** Obtienes la expresión que forma la expresión unaria.
     * @return puntero constante a expresión. */
    const Expression* get_expression() const{ return expression; }
  private:
    Expression *expression; ///< Expresión que conforma la expresión unaria.
  };

  /** Clase elemento expresión unaria más. */
  class ELEMENT_UNAEXP_PLUS : public UnaryExpression{
  public:
    /** Constructor del elemento expresión más a partir de un nodo XML. */
    ELEMENT_UNAEXP_PLUS(XMLElement* elem) : UnaryExpression(elem) {}
    /** Destructor del elemento expresión más. */
    ~ELEMENT_UNAEXP_PLUS() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión unaria menos. */
  class ELEMENT_UNAEXP_MINUS : public UnaryExpression{
  public:
    /** Constructor del elemento expresión menos a partir de un nodo XML. */
    ELEMENT_UNAEXP_MINUS(XMLElement* elem) : UnaryExpression(elem) {}
    /** Destructor del elemento expresión más. */
    ~ELEMENT_UNAEXP_MINUS() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión unaria negación. */
  class ELEMENT_UNAEXP_NOT : public UnaryExpression{
  public:
    /** Constructor del elemento expresión negación a partir de un nodo XML. */
    ELEMENT_UNAEXP_NOT(XMLElement* elem) : UnaryExpression(elem) {}
    /** Destructor del elemento expresión negación. */
    ~ELEMENT_UNAEXP_NOT() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

} // namespace LEAT
  
#endif // ELEMENT_UNARY_EXPRESSIONS_H_
