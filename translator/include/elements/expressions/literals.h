#ifndef ELEMENT_LITERALS_H_
#define ELEMENT_LITERALS_H_

#include "expression.h"
#include "../../tinyxml2.h"
#include "../../../modules/strings.h"
#include "../type.h"
using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase expresión literal. Esta clase se utiliza para agrupar sus derivadas en expresiones
   * literales. 
   * @see Expression */
  class LiteralExpression : public Expression{
  public:
    /** Constructor de expresión literal. */
    LiteralExpression(int o, const string& v, XMLElement *t) :
      Expression(o,t),
      value(v)
    {};
    /** Obtienes el valor de la expresión literal.
     *  @return valor */
    const string& get_value() const{ return value; }
  private:
    string value; ///< Valor de la expresión literal.
  };

  /** Clase elemento expresión lógica literal. 
   *  @see LiteralExpression */
  class ELEMENT_LITEXP_LOGIC : public LiteralExpression{
  public:
    /** Constructor de elemento expresión lógica literal. */
    ELEMENT_LITEXP_LOGIC(int, const string&, XMLElement*);
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión entera literal. 
   *  @see LiteralExpression */
  class ELEMENT_LITEXP_INTEGER : public LiteralExpression{
  public:
    /** Constructor de elemento expresión entero literal. */
    ELEMENT_LITEXP_INTEGER(int, const string&, XMLElement*);
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión real literal. 
   *  @see LiteralExpression */
  class ELEMENT_LITEXP_FLOAT : public LiteralExpression{
  public:
    /** Constructor de elemento expresión real literal. */
    ELEMENT_LITEXP_FLOAT(int, const string&, XMLElement*);
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión caracter literal. 
   *  @see LiteralExpression */
  class ELEMENT_LITEXP_CHARACTER : public LiteralExpression{
  public:
    /** Constructor de elemento expresión caracter literal. */
    ELEMENT_LITEXP_CHARACTER(int, const string&, XMLElement*);
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión cadena literal. 
   *  @see LiteralExpression */
  class ELEMENT_LITEXP_STRING : public LiteralExpression{
  public:
    /** Constructor de elemento expresión cadena literal. */
    ELEMENT_LITEXP_STRING(int, const string&, XMLElement*);
  private:
    void to_stream(Stream&, Stream&) const;
  };

} // namespace LEAT
  
#endif // ELEMENT_LITERALS_H_
