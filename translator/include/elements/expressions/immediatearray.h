#ifndef ELEMENT_IMMEDIATE_ARRAY_H_
#define ELEMENT_IMMEDIATE_ARRAY_H_

#include "immediate.h"
#include "../../tinyxml2.h"
#include "../../../modules/strings.h"

using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase expresión array inmediato.
   * @see Immediate */
  class ELEMENT_IMMEXP_ARRAY : public Immediate{
  public:
    /** Constructor de expresión inmediata array a partir de un nodo XML. */
    ELEMENT_IMMEXP_ARRAY(XMLElement* elem) : Immediate(elem) {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

} // namespace LEAT
  
#endif // ELEMENT_IMMEDIATE_ARRAY_H_
