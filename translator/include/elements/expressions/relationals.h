#ifndef ELEMENT_RELATIONALS_H_
#define ELEMENT_RELATIONALS_H_

#include "binaryexpression.h"
#include "../../tinyxml2.h"
#include "../../../modules/strings.h"

using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase elemento expresión iguales.
   * @see BinaryExpression */
  class ELEMENT_BINEXP_EQUALS : public BinaryExpression{
  public:
    /** Constructor del elemento expresión iguales a partir de un nodo XML. */
    ELEMENT_BINEXP_EQUALS(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión iguales. */
    ~ELEMENT_BINEXP_EQUALS() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión desiguales.
   * @see BinaryExpression */
  class ELEMENT_BINEXP_NOT_EQUALS : public BinaryExpression{
  public:
    /** Constructor del elemento expresión desiguales a partir de un nodo XML. */
    ELEMENT_BINEXP_NOT_EQUALS(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión desiguales. */
    ~ELEMENT_BINEXP_NOT_EQUALS() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión conjunción.
   * @see BinaryExpression */
  class ELEMENT_BINEXP_AND : public BinaryExpression{
  public:
    /** Constructor del elemento expresión conjunción a partir de un nodo XML. */
    ELEMENT_BINEXP_AND(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión conjunción. */
    ~ELEMENT_BINEXP_AND() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión disyunción.
   * @see BinaryExpression */
  class ELEMENT_BINEXP_OR : public BinaryExpression{
  public:
    /** Constructor del elemento expresión disyunción a partir de un nodo XML. */
    ELEMENT_BINEXP_OR(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión disyunción. */
    ~ELEMENT_BINEXP_OR() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión menor.
   * @see BinaryExpression */
  class ELEMENT_BINEXP_LESSER : public BinaryExpression{
  public:
    /** Constructor del elemento expresión menor a partir de un nodo XML. */
    ELEMENT_BINEXP_LESSER(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión menor. */
    ~ELEMENT_BINEXP_LESSER() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión menor igual.
   * @see BinaryExpression */
  class ELEMENT_BINEXP_LESSER_EQUALS : public BinaryExpression{
  public:
    /** Constructor del elemento expresión menor igual a partir de un nodo XML. */
    ELEMENT_BINEXP_LESSER_EQUALS(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión menor igual. */
    ~ELEMENT_BINEXP_LESSER_EQUALS() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión mayor.
   * @see BinaryExpression */
  class ELEMENT_BINEXP_GREATER : public BinaryExpression{
  public:
    /** Constructor del elemento expresión mayor a partir de un nodo XML. */
    ELEMENT_BINEXP_GREATER(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión mayor. */
    ~ELEMENT_BINEXP_GREATER() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

  /** Clase elemento expresión mayor igual.
   * @see BinaryExpression */
  class ELEMENT_BINEXP_GREATER_EQUALS : public BinaryExpression{
  public:
    /** Constructor del elemento expresión mayor igual a partir de un nodo XML. */
    ELEMENT_BINEXP_GREATER_EQUALS(XMLElement* elem) : BinaryExpression(elem) {}
    /** Destructor del elemento expresión mayor igual. */
    ~ELEMENT_BINEXP_GREATER_EQUALS() {}
  private:
    void to_stream(Stream&, Stream&) const;
  };

} // namespace LEAT
  
#endif // ELEMENT_RELATIONALS_H_
