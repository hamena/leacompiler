#ifndef BASE_EXPRESSION_H_
#define BASE_EXPRESSION_H_

#include "../type.h"
#include "../../leaelement.h"
#include "../../tinyxml2.h"

using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase base expresión. Esta clase sirve para agrupar todas sus derivadas en expresiónes.
   * @see LEAElement */
  class Expression : public LEAElement{
  public:
    /** Constructor de expresión a partir de un parámetro entero orden y un nodo XML. */
    Expression(int o, XMLElement* t);
    /** Destructor de expresión. */
    ~Expression();
    /** Obtienes el tipo de la expresión.
     * @return puntero constante al tipo. */
    virtual const ELEMENT_TYPE* get_type() const{ return type; }
    /** Obtienes la dimensión del tipo.
     * @return dimensión. */
    virtual int get_dimension() const{ return type->get_dimension(); }
    /** Comprueba si la expresión es una llamada a función. */ 
    virtual bool is_function_call() const{ return false; }

    /** Estructura función auxiliar que sirve para poder establecer una relación de orden entre
     * expresiones */
    struct Comparator {
      bool operator()(const Expression* e1, const Expression* e2) const{
        return e1->order < e2->order;
      }
    };
  private:
    int order;          ///< Atributo orden.
    ELEMENT_TYPE *type; ///< Tipo de la expresión.
  };

} // namespace LEAT
  
#endif // BASE_EXPRESSION_H_
