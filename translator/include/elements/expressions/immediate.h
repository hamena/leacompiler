#ifndef BASE_IMMEDIATE_H_
#define BASE_IMMEDIATE_H_

#include "expression.h"
#include "../../tinyxml2.h"
#include <vector>

using tinyxml2::XMLElement;
using std::vector;

namespace LEAT{

  /** Clase expresión inmediata. Esta clase sirve para agrupar todos los derivados
   * en expresiones inmediatas. 
   * @see Expression */
  class Immediate : public Expression{
  public:
    /** Constructor de expresión inmediata a partir de un nodo XML. */
    Immediate(XMLElement*);
    /** Obtienes las expresiones que conforman la expresión inmediata.
     * @return vector de punteros a expresión. */
    const vector<Expression*>& get_expressions() const{ return expressions; }

    /** Destructor de expresión inmediata. */
    ~Immediate();
  private:
    vector<Expression*> expressions; ///< Expresiones que forman la expresión inmediata.
  };

} // namespace LEAT
  
#endif // BASE_IMMEDIATE_H_
