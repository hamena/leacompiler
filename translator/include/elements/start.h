#ifndef ELEMENT_START_H_
#define ELEMENT_START_H_

#include "algorithm.h"
#include "../tinyxml2.h"
#include "../leaelement.h"
#include "../../modules/strings.h"
#include <vector>

using tinyxml2::XMLElement;
using std::vector;

namespace LEAT{

  /** Clase elemento comienzo. Esta clase representa el comienzo del programa y contiene
   * todos los algoritmos definidos en código LEA. */
  class ELEMENT_START : public LEAElement{
  public:
    /** Constructor del elemento comienzo a partir de un nodo XML. */
    ELEMENT_START(XMLElement*);
    /** Destructor del elemento comienzo. */
    ~ELEMENT_START();

    /** Obtienes la lista de algoritmos que forman el programa LEA.
     *  @return vector de punteros a elementos algoritmo. */
    const vector<ELEMENT_ALGORITHM*> get_algorithms() const{ return algorithms; }
  private:
    void to_stream(Stream&, Stream&) const;

    vector<ELEMENT_ALGORITHM*> algorithms; ///< Algoritmos del programa.
  };

} // namespace LEAT
  
#endif // ELEMENT_START_H_
