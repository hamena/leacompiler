#ifndef ELEMENT_LOOP_FOR_ALL_H_
#define ELEMENT_LOOP_FOR_ALL_H_

#include <vector>
#include "expressions/expression.h"
#include "instruction.h"
#include "../variable.h"
#include "../tinyxml2.h"
#include "../../modules/strings.h"

using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase elemento bucle para todo. Esta clase representa el elemento de bucle para todo, por
   * cada bucle para todo programado en código LEA se crea un elemento bucle para todo.
   * @see Instruction */
  class ELEMENT_LOOP_FOR_ALL : public Instruction{
  public:
    /** Constructor del elemento bucle para todo a partir de un nodo XML. */
    ELEMENT_LOOP_FOR_ALL(XMLElement*);
    /** Destructor del elemento bucle para todo. */
    ~ELEMENT_LOOP_FOR_ALL();

    /** Obtienes el iterador sobre el conjunto.
     *  @return puntero constante a la variable que servirá de iterador. */
    const Variable* get_iterator() const{ return iterator; }
    /** Obtienes la expresión de tipo conjunto sobre la que iterará el bucle.
     *  @return puntero constante a una expresión. */
    const Expression* get_expression() const{ return expression; }
    /** Obtienes la lista de instrucciones del cuerpo del bucle.
     *  @return vector ordenado de punteros a instrucción. */
    const vector<Instruction*>& get_instructions() const{ return instructions; }
  private:
    void to_stream(Stream&, Stream&) const;
    Variable* iterator;                ///< Variable iterador.
    Expression* expression;            ///< Expresión conjunto.
    vector<Instruction*> instructions; ///< Vector ordenado de instrucciones.
  };

} // namespace LEAT
  
#endif // ELEMENT_LOOP_FOR_ALL_H_
