#ifndef ELEMENT_ALGORITHM_H_
#define ELEMENT_ALGORITHM_H_

#include <vector>
#include <string>
#include "instruction.h"
#include "../leaelement.h"
#include "../tinyxml2.h"
#include "../basealgorithm.h"
#include "../argument.h"
#include "../../modules/strings.h"

using std::vector;
using std::string;

namespace LEAT{

  /** Clase elemento algoritmo. Esta clase representa el elemento LEA de un
   * algoritmo. Por cada algoritmo definido en LEA se creará un nuevo elemento algoritmo.
   * @see LEAElement
   * @see BaseAlgorithm */
  class ELEMENT_ALGORITHM : public LEAElement, public BaseAlgorithm{
  public:
    /** Constructor del elemento algoritmo. Construye un elemento algoritmo a partir
     * de un nodo XML. */ 
    ELEMENT_ALGORITHM(XMLElement*);
    /** Destructor del elemento algoritmo. */
    ~ELEMENT_ALGORITHM();
    /** Obtienes los argumentos de entrada del algoritmo.
     *  @return vector de punteros a argumentos. */
    const vector<Argument*>& get_input_arguments() const{ return inputArguments; }
    /** Obtienes los argumentos de salida del algoritmo.
     *  @return vector de punteros a argumentos. */
    const vector<Argument*>& get_output_arguments() const{ return outputArguments; }
    /** Obtienes las instrucciones del cuerpo del algoritmo.
     *  @return vector de punteros a instrucción. */
    const vector<Instruction*>& get_instructions() const{ return instructions; }
  private:
    void to_stream(Stream&, Stream&) const;
    vector<Argument*> inputArguments;  ///< Argumentos de entrada del algoritmo.
    vector<Argument*> outputArguments; ///< Argumentos de salida del algoritmo.
    vector<Instruction*> instructions; ///< Instrucciones del cuerpo del algoritmo.
  };

} // namespace LEAT
  
#endif // ELEMENT_ALGORITHM_H_
