#ifndef ELEMENT_REPEAT_LOOP_H_
#define ELEMENT_REPEAT_LOOP_H_

#include <vector>
#include "instruction.h"
#include "expressions/expression.h"
#include "../tinyxml2.h"
#include "../../modules/strings.h"

using std::vector;
using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase elemento bucle repite. Esta clase representa la instrucción bucle repite,
   * por cada bucle repite definido en código LEA se crea un nuevo elemento bucle repite. */
  class ELEMENT_LOOP_REPEAT : public Instruction{
  public:
    /** Constructor elemento bucle repite a partir de un nodo XML. */
    ELEMENT_LOOP_REPEAT(XMLElement*);
    /** Destructor elemento bucle repite. */
    ~ELEMENT_LOOP_REPEAT();
    /** Obtienes la expresión lógica del bucle repite.
     *  @return puntero constante a expresión. */
    const Expression* get_expression() const{ return expression; }
    /** Obtienes la lista de instrucciones del cuerpo del bucle.
     *  @return vector ordenado de punteros a instrucción. */
    const vector<Instruction*> get_instructions() const{ return instructions; }
    /** Comprueba si el bucle repite es de tipo 'hasta' en lugar de 'mientras'. */
    bool is_until() const{ return untilFlag; }
  private:
    void to_stream(Stream&, Stream&) const;
    Expression *expression;            ///< Condición de parada del bucle.
    vector<Instruction*> instructions; ///< Vector ordenado de instrucciones.
    bool untilFlag;                    ///< Bandera de bucle repite tipo 'hasta'.
  };

} // namespace LEAT
  
#endif // ELEMENT_REPEAT_LOOP_H_
