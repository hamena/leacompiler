#ifndef ELEMENT_TYPE_H_
#define ELEMENT_TYPE_H_

#include <iostream>
#include <sstream>
#include <string>
#include "../tinyxml2.h"
#include "../leaelement.h"
#include "../../modules/strings.h"

using namespace tinyxml2;
using std::stringstream;
using std::ostream;
using std::string;

namespace LEAT{

  /** Clase elemento tipo. Esta clase representa el elemento tipo y contiene toda la
   * información relativa a el tipo y subtipos del mismo necesarios para representar
   * todo tipo de elementos de LEA. */
  class ELEMENT_TYPE : public LEAElement{
  public:
    /** Constructor del elemento tipo a partir de un nodo XML. */
    ELEMENT_TYPE(XMLElement*);
    /** Constructor de copia del elemento tipo. */
    ELEMENT_TYPE(const ELEMENT_TYPE&);
    /** Constructor de tipo vector. */
    ELEMENT_TYPE(int, ELEMENT_TYPE*);
    /** Constructor de tipo conjunto vacio. */
    ELEMENT_TYPE();
    /** Destructor de elemento tipo. */
    ~ELEMENT_TYPE();
    /** Obtienes el nombre del tipo. 
     *  @return cadena con el nombre. */
    const string& get_name() const{ return name; }
    /** Obtienes la dimensión del tipo.
     *  @return >0 si es array, -1 si el tipo no es válido , 0 en otro caso. */
    int get_dimension() const{ return dimension; }
    /** Comprueba si el tipo es lógico. */
    bool is_logic() const;
    /** Comprueba si el tipo es entero. */
    bool is_integer() const;
    /** Comprueba si el tipo es real. */
    bool is_float() const;
    /** Comprueba si el tipo es caracter. */
    bool is_character() const;
    /** Comprueba si el tipo es cadena. */
    bool is_string() const;
    /** Comprueba si el tipo es conjunto. */
    bool is_set() const;
    /** Comprueba si el tipo es array. */
    bool is_array() const;
    /** Comprueba si el tipo es conjunto vacío. */
    bool is_empty_set() const{ return emptySetFlag; }
    /** Obtienes el subtipo del tipo.
     *  @return puntero constante a elemento tipo. */
    const ELEMENT_TYPE* get_type() const{ return type; }
    /** Obtienes el valor de inicialización del tipo.
     *  @return cadena con el valor de inicialización. */
    const string& get_init_value() const{ return initValue; }
    /** Guarda la dimensión del tipo. */
    void set_dimension(int);
  private:
    void to_stream(Stream&, Stream&) const;
    bool emptySetFlag;  ///< Bandera de conjunto vacío.
    string name;        ///< Nombre del tipo.
    int dimension;      ///< Dimensión del tipo.
    ELEMENT_TYPE *type; ///< Subtipo del tipo.
    string initValue;   ///< Valor de inicialización del tipo.
  };

} // namespace LEAT  
  
#endif // ELEMENT_TYPE_H_
