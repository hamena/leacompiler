#ifndef ELEMENT_WHILE_LOOP_H_
#define ELEMENT_WHILE_LOOP_H_

#include <vector>
#include "instruction.h"
#include "expressions/expression.h"
#include "../tinyxml2.h"
#include "../../modules/strings.h"

using std::vector;
using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase elemento bucle mientras. Esta clase representa al elemento bucle mientras, por
   * cada bucle mientras definido en código LEA se crea un nuevo elemento bucle mientras. */
  class ELEMENT_LOOP_WHILE : public Instruction{
  public:
    /** Constructor del elemento bucle mientras a partir de un nodo XML. */
    ELEMENT_LOOP_WHILE(XMLElement*);
    /** Destructor del elemento bucle mientras. */
    ~ELEMENT_LOOP_WHILE();
    /** Obtienes la expresión que define la condición de parada del bucle.
     *  @return puntero constante a expresión. */
    const Expression* get_expression() const{ return expression; }
    /** Obtienes la lista de instrucciones pertenecientes al cuerpo del bucle.
     *  @return vector ordenado de punteros a instrucción. */
    const vector<Instruction*>& get_instructions() const{ return instructions; }
  private:
    void to_stream(Stream&, Stream&) const;
    Expression *expression;            ///< Expresión condición de parada.
    vector<Instruction*> instructions; ///< Instrucciones del cuerpo del bucle.
  };

} // namespace LEAT
  
#endif // ELEMENT_WHILE_LOOP_H_
