#ifndef ARGUMENT_H_
#define ARGUMENT_H_

#include "variable.h"
#include "tinyxml2.h"

using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase argumento de función. Esta clase derivada de Variable representa
   * los argumentos de entrada y salida de las funciones definidas en código LEA.
   * @see Variable
   */
  class Argument : public Variable{
  public:
    /** Constructor del argumento. */
    Argument(XMLElement *elem, const string& structName) : Variable(elem,structName) {}
  };

} // namespace LEAT
  
#endif //  ARGUMENT_H_
