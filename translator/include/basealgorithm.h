#ifndef BASE_ALGORITHM_H_
#define BASE_ALGORITHM_H_

#include <string>
#include <set>
#include "../modules/util/safeuse.h"

using std::string;
using std::set;

/** Espacio de nombres del traductor. */
namespace LEAT{

  /** Clase base de algoritmos. Esta clase sirve de base tanto para
   * el elemento algoritmo como para la expresión llamada a función. 
   * @see ELEMENT_ALGORITHM
   * @see ELEMENT_FINEXP_FUNCTION_CALL
   */
  class BaseAlgorithm{
  public:
    /** Constructor. Instancia el objeto con un nombre. */
    BaseAlgorithm(const string&);
    /** Destructor virtual */
    virtual ~BaseAlgorithm(){}
    /** Comprueba si el algoritmo es el algoritmo principal main. 
	@return verdadero si es main, falso en caso contrario. */
    bool is_main() const{ return algorithmName == "main"; }
    /** Obtienes el identificador del objeto instanciado de
     *  la estructura usada para la entrada del algoritmo.
     *  @return identificador del objeto entrada. */
    string get_input_name() const{ return algorithmName + "_Entrada"; }
    /** Obtienes el identificador del objeto instanciado de
     *  la estructura usada para la salida del algoritmo.
     *  @return identificador del objeto salida. */
    string get_output_name() const{ return algorithmName + "_Salida"; }
    /** Obtienes el nombre del tipo estructura para la entrada del algoritmo.
     *  @return nombre del tipo entrada. */
    string get_input_type_name() const{ return "TipoEntrada_" + algorithmName; }
    /** Obtienes el nombre del tipo estructura para la salida del algoritmo.
     *  @return nombre del tipo salida. */
    string get_output_type_name() const{ return "TipoSalida_" + algorithmName; }
    /** Obtienes el nombre del algoritmo.
     *  @return nombre del algoritmo. */
    const string& get_name() const{ return algorithmName; }
  private:
    /** Cadena para el nombre del algoritmo. */
    string algorithmName;
    /** Conjunto de identificadores estáticos que indican 
     *  qué nombres de algoritmos no se han de codificar. */
    static const set<string> nameWhitelist;
  };

} // namespace LEAT

#endif // BASE_ALGORITHM_H_
