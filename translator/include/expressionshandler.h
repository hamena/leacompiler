#ifndef EXPRESSIONS_HANDLER_H_
#define EXPRESSIONS_HANDLER_H_

#include "elements/expressions/expression.h"
#include "tinyxml2.h"

using tinyxml2::XMLElement;

namespace LEAT{

  /** Clase auxiliar para identificar las expresiones. Esta clase es un multiplexador de
   * de clases de expresiones de LEA que vienen en un nodo XML. Identifica la expresión
   * en cuestión e instancia la expresión correspondiente. */
  class ExpressionsHandler{
  public:
    /** Función principal que identifica la expresión de un nodo XML y devuelve un puntero
     * a la nueva expresión instanciada. */
    static Expression* new_expression(XMLElement*);
  private:
    /** Función auxiliar para determinar la expresión binaria. */
    static Expression* binary_switch(XMLElement*);
    /** Función auxiliar para determinar la expresión unaria. */
    static Expression* unary_switch(XMLElement*);
    /** Función auxiliar para determinar la expresión literal. */
    static Expression* literal_switch(XMLElement*);
    /** Función auxiliar para determinar la expresión llamada a función. */
    static Expression* function_switch(XMLElement*);
  };

} // namespace LEAT
  
#endif // EXPRESSIONS_HANDLER_H_
