package GUI;

import java.io.*;
import java.nio.file.*;


import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.text.*;

import java.util.Locale;

/** Clase GUI. Esta clase proporciona la interfaz gráfica
 * a la aplicación para poder añadir todos los componentes
 * necesarios de la interfaz.
*/
public class GUI extends JFrame{

    /** Campo de texto de código LEA. */
    static JTextArea codigoLEA = new JTextArea(30,10);
    /** Scroll del campo de texto LEA.*/
    static JScrollPane scrollLEA = new JScrollPane(codigoLEA);

    /** Campo de texto de código XML. */
    static JTextArea codigoXML = new JTextArea(30,10);
    /** Scroll del campo de texto XML.*/
    static JScrollPane scrollXML = new JScrollPane(codigoXML);

    /** Campo de texto de código CPP. */
    static JTextArea codigoCPP = new JTextArea(30,10);
    /** Scroll del campo de texto CPP.*/
    static JScrollPane scrollCPP = new JScrollPane(codigoCPP);

    /** Campo de texto de la salida estándar. */
    static JTextArea salidaStd = new JTextArea(10,90);
    /** Scroll del campo de texto de la salida estándar.*/
    static JScrollPane scrollSalida = new JScrollPane(salidaStd);

    /** Botón compilar LEA. */
    static JButton compilarLEA = new JButton("Compilar");
    /** Botón traducir a C++. */
    static JButton traducir = new JButton("Traducir");
    /** Botón compilar y ejecutar código C++. */
    static JButton compilarCPP = new JButton("Compilar y ejecutar C++");

    /** Nombre del fichero LEA */
    static private String ficheroLEA = "codigoLEA_GUI.lea";
    /** Nombre del fichero XML */
    static private String ficheroXML = "salida_GUI.xml";
    /** Nombre del fichero CPP */
    static private String ficheroCPP = "salida_GUI.cpp";
    /** Nombre del ejecutable */
    static private String ejecutable = "exeGUI";

    /** Constructor de la clase. */
    public GUI(String name){
	super(name);
	setResizable(true);
    }

    /** Comienzo del programa. */
    public static void main(String[] args){
	Locale.setDefault(Locale.US);

	/* Use an appropriate Look and Feel */
	try {
            //UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
        /* Turn off metal's use of bold fonts */
        UIManager.put("swing.boldMetal", Boolean.FALSE);


	GUI frame = new GUI("Compilador y traductor LEA");
	frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	frame.anadirComponentes();
	frame.anadirComportamiento();
	
	// Mostrar la ventana
        frame.pack();
        frame.setVisible(true);
    }

    /** Método para construir la ventana de la interfaz. */
    public void anadirComponentes(){

	codigoLEA.setEditable(true);
	JPanel panelLEA = new JPanel();
	panelLEA.setLayout(new BoxLayout(panelLEA,BoxLayout.PAGE_AXIS));
	panelLEA.setBorder(BorderFactory.createTitledBorder("Código LEA"));
	panelLEA.add(scrollLEA);
	panelLEA.add(compilarLEA);

	codigoXML.setEditable(false);
	JPanel panelXML = new JPanel();
	panelXML.setLayout(new BoxLayout(panelXML,BoxLayout.PAGE_AXIS));
	panelXML.setBorder(BorderFactory.createTitledBorder("Representación intermedia"));
	panelXML.add(scrollXML);
	panelXML.add(traducir);
	
	codigoCPP.setEditable(false);
	JPanel panelCPP = new JPanel();
	panelCPP.setLayout(new BoxLayout(panelCPP,BoxLayout.PAGE_AXIS));
	panelCPP.setBorder(BorderFactory.createTitledBorder("Código C++"));
	panelCPP.add(scrollCPP);
	panelCPP.add(compilarCPP);
		
	JPanel control = new JPanel();
	control.setLayout(new GridLayout(1,3));
	control.add(panelLEA);
	control.add(panelXML);
	control.add(panelCPP);
	
	JPanel salida = new JPanel();
	salidaStd.setEditable(false);
	salida.add(scrollSalida);

	JPanel global = new JPanel();
	global.setLayout(new BoxLayout(global,BoxLayout.PAGE_AXIS));
	global.add(control);
	global.add(salida);

	this.add(global);
    }

    /** Método para añadir el comportamiento a los botones de la interfaz. */
    public void anadirComportamiento(){
	compilarLEA.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    String comando = "./leac -c "+ficheroLEA+" -o "+ficheroXML;
		    String stdaux = "";
		    try{
			codigoXML.setText("");
			PrintWriter writer = new PrintWriter(ficheroLEA, "UTF-8");
			writer.println(codigoLEA.getText());
			writer.close();
			lanzarComando("rm "+ficheroXML);
			stdaux = comando+"\n";
			stdaux += lanzarComando(comando);
		    } catch(Exception x) {
			System.out.println("Capturada excepción al escribir "+ficheroLEA);
		    }

		    codigoXML.setText(leerFichero(ficheroXML));
		    salidaStd.append(stdaux);
		}
	    });

	traducir.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    String comando = "./leat -t "+ficheroXML+" -o "+ficheroCPP;
		    String stdaux = "";
		    try{
			codigoCPP.setText("");
			lanzarComando("rm "+ficheroCPP);
			stdaux = comando+"\n";
			stdaux += lanzarComando(comando);
		    } catch(Exception x) {
			System.out.println("Capturada excepción al escribir "+ficheroLEA);
		    }

		    codigoCPP.setText(leerFichero(ficheroCPP));
		    salidaStd.append(stdaux);
		}
	    });

	compilarCPP.addActionListener(new ActionListener(){
		public void actionPerformed(ActionEvent e){
		    String comandoCompilar = "g++ "+ficheroCPP+" -o "+ejecutable+" --std=c++11";
		    String comandoEjecutar = "./"+ejecutable;
		    String stdaux = "";
		    try{
			stdaux = comandoCompilar+"\n";
			stdaux += lanzarComando(comandoCompilar);
			stdaux += "\n"+comandoEjecutar+"\n";
			stdaux += lanzarComando(comandoEjecutar);
		    } catch(Exception x) {
			System.out.println("Capturada excepción al escribir "+ficheroLEA);
		    }

		    codigoCPP.setText(leerFichero(ficheroCPP));
		    salidaStd.append(stdaux);
		}
	    });
    }

    /** Función para realizar llamadas al sistema.
     * @param Comando
     * @return Salida del comando
     */
    private String lanzarComando(String comando){
	String output = "";
	try{
	    Process proc = Runtime.getRuntime().exec(comando);
	    
	    BufferedReader stdInput =
		new BufferedReader(new InputStreamReader(proc.getInputStream()));
	    
	    BufferedReader stdError =
		new BufferedReader(new InputStreamReader(proc.getErrorStream()));

	    String line = null;
	    while ((line = stdInput.readLine()) != null) {
		output += line+"\n";
	    }
	    while ((line = stdError.readLine()) != null) {
		output += line+"\n";
	    }
	} catch (Exception e){
	    System.out.println("Capturada excepción al lanzar comando. "+e.getMessage());
	}
	return output;
    }

    /** Función para leer ficheros y devolverlos como cadenas.
     * @param Nombre del fichero
     * @return Fichero volcado en una cadena.
     */
    private String leerFichero(String fich){
	String text = "";
	Path ruta = FileSystems.getDefault().getPath("", fich);
	try (InputStream in = Files.newInputStream(ruta);
	     BufferedReader reader =
	     new BufferedReader(new InputStreamReader(in))) {
	    String line = null;
	    while ((line = reader.readLine()) != null) {
		text += line+"\n";
	    }
	} catch (IOException x) {
	    System.out.println("Capturada excepción al leer: "+x.getMessage());
	}
	return text;
    }
}
