# LEA Compiler

Compilador del lenguaje LEA, proyecto propuesto para Trabajo de Fin de Grado de la Universidad de Cádiz.

# Requisitos del sistema #
Para compilar este proyecto son necesarias las herramientas GNU flex, bison y g++ compatible con el estándar C++11.

# Compilación #
A través de makefile se han establecido las reglas para la compilación. 
Si prentende compilar tanto el compilador como el traductor ejecute:
  *make all*

Si quiere compilar únicamente el compilador:
  *make leac*

Si quiere compilar únicamente el traductor:
  *make leat*

Existen además otras reglas utiles en el makefile aparte de la compilación.
Para limpiar los directorios:
  *make clean*

Para generar la documentación:
  *make doc*

Para limpiar el directorio de documentación:
  *make cleandoc*