
MODULE = c++
MODULE_CPPOBJ = start general declarations litexpressions immediates assigns control expvariable emptyset relationals setbased arithmetics unary parenthesis functioncall stdcall

# G E N E R A L  macros
CXX = g++

CXXDEBUG = -g -Wall
CXXSTD = --std=c++11
CXXFLAGS = -O0 $(CXXDEBUG) $(CXXSTD)

# C O M P I L E R  macros
OBJDIR = compiler/object/
SRCDIR = compiler/source/

CPPOBJ = main driver algorithms algorithmbase algorithm stdalgorithm type conversionmanager errormanager dependenciesmanager xmloutputmanager
FILES = $(addsuffix .cpp, $(addprefix $(SRCDIR), $(CPPOBJ)))
OBJS = $(addsuffix .o, $(addprefix $(OBJDIR), $(CPPOBJ)))
SCANNEROBJ = $(addprefix $(OBJDIR), scanner.o)
PARSEROBJ = $(addprefix $(OBJDIR), parser.o)

SOBJ = parser scanner
FLEXFLAGS = --outfile=compiler/scanner/scanner.yy.cc
BISONFLAGS = --debug --verbose -d -o compiler/parser/parser.tab.cc

EXE = binary/leac

# T R A N S L A T O R  macros
TRANSLATOR_OBJDIR = translator/object/
TRANSLATOR_SRCDIR = translator/source/

TRANSLATOR_CPPOBJ = start main tinyxml2 instructionshandler expressionshandler safeuse declared tabs basealgorithm algorithm variablestruct type variabledeclaration arraydeclaration simpleassign parallelassign swapassign conditional whileloop repeatloop fromloop forallloop expression variable literals immediate binaryexpression unary parenthesis functioncall
TRANSLATOR_FILES = $(addsuffix .cpp, $(addprefix $(TRANSLATOR_SRCDIR), $(TRANSLATOR_CPPOBJ)))
TRANSLATOR_OBJS = $(addsuffix .o, $(addprefix $(TRANSLATOR_OBJDIR), $(TRANSLATOR_CPPOBJ)))

TRANSLATOR_EXE = binary/leat

MODULE_DIR = translator/modules/
MODULE_SRCDIR = $(addprefix $(MODULE_DIR),$(MODULE)/)
MODULE_OBJDIR = $(TRANSLATOR_OBJDIR)

MODULE_FILES = $(addsuffix .cpp, $(addprefix $(MODULE_SRCDIR), $(MODULE_CPPOBJ)))
MODULE_OBJS = $(addsuffix _mod.o, $(addprefix $(MODULE_OBJDIR), $(MODULE_CPPOBJ)))

# Makefile rules

.PHONY: all parser scanner clean leac leat doc

leac: $(EXE)

leat: $(TRANSLATOR_EXE)

all: $(EXE) $(TRANSLATOR_EXE) gui

$(EXE): $(FILES)
	$(MAKE) $(SOBJ)
	$(MAKE) $(OBJS)
	$(CXX) $(OBJS) $(addprefix $(OBJDIR), $(addsuffix .o, $(SOBJ))) -o $(EXE) $(CXXFLAGS)

$(OBJDIR)%.o: $(SRCDIR)%.cpp
	$(CXX) -c $< -o $@ $(CXXFLAGS)

scanner: compiler/scanner/scanner.ll
	flex $(FLEXFLAGS) $<
	$(CXX) -c compiler/scanner/scanner.yy.cc -o $(SCANNEROBJ) $(CXXFLAGS) 

parser: compiler/parser/parser.yy
	bison $(BISONFLAGS) $<
	$(CXX) -c compiler/parser/parser.tab.cc -o $(PARSEROBJ) $(CXXFLAGS) 

$(TRANSLATOR_EXE): $(TRANSLATOR_FILES) $(MODULE_FILES)
	$(MAKE) $(TRANSLATOR_OBJS)
	$(MAKE) $(MODULE_OBJS)
	$(CXX) $(TRANSLATOR_OBJS) $(MODULE_OBJS) -o $(TRANSLATOR_EXE) $(CXXFLAGS)

$(TRANSLATOR_OBJDIR)%.o: $(TRANSLATOR_SRCDIR)%.cpp
	$(CXX) -c $< -o $@ $(CXXFLAGS)

$(MODULE_OBJDIR)%_mod.o: $(MODULE_SRCDIR)%.cpp
	$(CXX) -c $< -o $@ $(CXXFLAGS)

gui: java
	jar cmf GUI/manifest.mf binary/GUI.jar GUI/*.class

java: GUI/GUI.java
	javac GUI/GUI.java

doc:
	doxygen Doxyfile

cleandoc:
	rm -rf doc/doxygen/*

clean:
	find . -type f -name "*~" -delete
	rm -rf $(OBJDIR)*.o $(TRANSLATOR_OBJDIR)*.o 
	rm -rf $(PARSEROBJ) compiler/parser/parser.tab.cc compiler/parser/parser.output compiler/parser/*.hh 
	rm -rf $(SCANNEROBJ) compiler/scanner/scanner.yy.cc 
	rm -rf GUI/*.class

cleanexe:
	rm -rf $(EXE) $(TRANSLATOR_EXE)
	rm -rf binary/GUI.jar

