%{ /*** C/C++ Declarations ***/


#ifndef _GLIBCXX_USE_CXX11_ABI
#define _GLIBCXX_USE_CXX11_ABI 0
#endif
#include <iostream>
#include <string>
#include "../include/defstrings.h"
#include "../include/type.h"
#include "../include/algorithms.h"
#include "../include/managers/conversionmanager.h"
#include "../include/managers/xmloutputmanager.h"
#include "../include/nodes/node.h"
#include "../include/nodes/nodedecdef.h"
#include "../include/nodes/nodeassigns.h"
#include "../include/nodes/nodeexpressions.h"
#include "../include/nodes/nodeiterations.h"
#include "../include/nodes/nodeconditionals.h"

using namespace std;
  
%}

/* add debug output code to generated parser. disable this for release
 * versions. */
%debug

/* start symbol is named "start" */
%start start

/* write out a header file containing the token defines */
%defines

/* use newer C++ skeleton file */
%skeleton "lalr1.cc"

/* namespace to enclose parser in */
%name-prefix "LEAC"

/* set the parser's class identifier */
%define parser_class_name { Parser }

/* keep track of the current position within the input */
%locations
// initialize the initial location object
%initial-action { @$.begin.filename = @$.end.filename = &driver.streamname; }

/* The driver is passed by reference to the parser and to the scanner. This
 * provides a simple but effective pure interface, not relying on global
 * variables. */
%parse-param { class Driver& driver }

/* verbose error messages */
%error-verbose
			
			
/*
%union {}
%type...

%destructor { LEA_DELETE( $$ ); } ID algorithm header
%destructor { LEA_DELETE_LISTP( $$, std::string* ); }  arglist
*/

%union {
  int ival;
  float fval;
  char cval;
  std::string* sval;
  std::vector<std::string*>* vids;
  Node* node;
  NodeExp* nodeexp;
  Type* type;
}

%token END 0 "end of file"
%token LTRUE LFALSE
%token WHILE REPEAT UNTIL FROM INCREMENT DECREMENT WITH FOR_ALL IF ELSE
%token RANGE LEFT_ARROW RIGHT_ARROW DOUBLE_ARROW DIV INDENT DEDENT
%token AND OR EQUALS LESSER LESSER_EQUALS GREATER GREATER_EQUALS NOT NOT_EQUALS	
%token IN CONTAINS SCONTAINS CONTAINED_IN SCONTAINED_IN EMPTY_SET UNION INTERSECTION
%token COMMA COLON DOT PLUS MINUS STAR SLASH MOD LBRACER RBRACER LP RP LBRACKET RBRACKET
%token TBOOL TINTEGER TREAL TCHARACTER TSTRING
%token <sval> ID LSTRING
%token <cval> LCHARACTER
%token <ival> LINTEGER
%token <fval> LREAL

%type <node> entry
%type <node> function_definition header input_args output_args
%type <node> code_block instructions instruction variable_definition
%type <node> assign simple_assign parallel_assign swap_assign
%type <node> iteration while_loop repeat_loop repeat_loop_type 
%type <node> from_loop from_loop_type from_loop_numeric 
%type <node> from_loop_general forall_loop forall_in conditional ifelse
%type <nodeexp> modificable_expression expressions expression access
%type <nodeexp> disyuntive_logical in relational1 relational2 add mult unary delimiter
%type <nodeexp> expression_final readable_expression literal_constant 
%type <nodeexp> function_call readable_tuple
%type <nodeexp> tuple modificable_expressions modificable_tuple
%type <type> final_type 
%type <sval> oprelational1 oprelational2 opadd opmult opunary
%type <vids> ids
%type <ival> inc_or_dec
					      
%{
			
#include "../include/driver.h"
#include "../scanner/scanner.h"

/* this "connects" the bison parser in the driver to the flex scanner class
 * object. it defines the yylex() function call to pull the next token from the
 * current lexer object of the driver context. */
#undef yylex
#define yylex driver.lexer->lex

%}

%%
start:	    	entry {
		        driver.sem($1);			
		      }
	;

entry:		function_definition entry { $$ = new NodeEntry(@$.begin.line,$1,$2); }
	|	{ $$ = new NodeNULL(); }
	;

final_type:	TBOOL { $$ = Type::make_new_logic(); }
	|	TINTEGER { $$ = Type::make_new_integer(); }
	|	TREAL { $$ = Type::make_new_float(); }
	|	TCHARACTER { $$ = Type::make_new_character(); }
	|	TSTRING { $$ = Type::make_new_string(); }
	;

function_definition:
		header code_block { $$ = new NodeAlgorithmDefinition(@$.begin.line,$1,$2); }
	;

header:		ID LP input_args RP RIGHT_ARROW LP output_args RP
		{ $$ = new NodeHeader(@$.begin.line,*$1,$3,$7); }
	;

input_args:	ID COMMA input_args { $$ = new NodeInputArgs(@$.begin.line,*$1,$3); }
	|	ID { $$ = new NodeInputArgs(@$.begin.line,*$1,new NodeNULL()); }
	|	{ $$ = new NodeNULL(); }
	;

output_args:	ID COMMA output_args { $$ = new NodeOutputArgs(@$.begin.line,*$1,$3); }
	|	ID { $$ = new NodeOutputArgs(@$.begin.line,*$1,new NodeNULL()); }
	|	{ $$ = new NodeNULL(); }
	;

code_block:	INDENT instructions DEDENT {    $$ = new NodeCodeblock(@$.begin.line,$2); }
	;

instructions:   instruction instructions { $$ = new NodeInstructions(@$.begin.line,$1,$2); }
	|	instruction { $$ = $1; }
        ;

instruction: 	variable_definition { $$ = new NodeInstruction(@$.begin.line,$1); }
	| 	assign { $$ = new NodeInstruction(@$.begin.line,$1); }
	|   	conditional { $$ = new NodeInstruction(@$.begin.line,$1); }
	|	iteration { $$ = new NodeInstruction(@$.begin.line,$1); }
	|	function_call { $$ = new NodeInstruction(@$.begin.line,$1); }
	;

variable_definition:
		ids COLON final_type { $$ = new NodeVariableDefinition(@$.begin.line,*$1,$3); }
	|	ids COLON final_type access { $$ = new NodeArrayDefinition(@$.begin.line,*$1,$3,$4); }
	;

ids:		ids COMMA ID { $$->push_back($3); }
	|	ID { $$ = new vector<string*>(1,$1); }
	;

assign:		simple_assign { $$ = new NodeAssign(@$.begin.line,$1); }
	|	parallel_assign { $$ = new NodeAssign(@$.begin.line,$1); }
	|	swap_assign { $$ = new NodeAssign(@$.begin.line,$1); }
	;

modificable_expression:
		ID access { $$ = new NodeModificableExpression(@$.begin.line,*$1,$2); }
	|	ID { $$ = new NodeModificableExpression(@$.begin.line,*$1,nullptr); }
	;

access:		LBRACKET expressions RBRACKET { $$ = new NodeAccess(@$.begin.line,$2); }
/*	|	DOT modificable_expression */
	;


modificable_expressions:
		modificable_expression COMMA modificable_expressions
		{ $$ = new NodeModificableExpressions(@$.begin.line,$1,$3); }
	|	modificable_expression { $$ = $1; }
	;

modificable_tuple:
		LP modificable_expressions RP { $$ = new NodeModificableTuple(@$.begin.line,$2); }
	;


simple_assign:	modificable_expression LEFT_ARROW expression {$$ = new NodeSimpleAssign(@$.begin.line,$1,$3);}
	;

parallel_assign:
		modificable_tuple LEFT_ARROW readable_tuple
		{ $$ = new NodeParallelAssign(@$.begin.line,$1,$3); }
	;
	
readable_tuple:	tuple { $$ = $1; }
	|	function_call { $$ = $1; }
	;			

swap_assign:	modificable_expression DOUBLE_ARROW modificable_expression
		{ $$ = new NodeSwapAssign(@$.begin.line,$1,$3); }
	;

oprelational1:	EQUALS { $$ = new string(OPER_EQ); }
	|	NOT_EQUALS { $$ = new string(OPER_NEQ); }
	;

oprelational2:	LESSER { $$ = new string(OPER_LT); }
	|	LESSER_EQUALS { $$ = new string(OPER_LE); }
	|	GREATER { $$ = new string(OPER_GT); }
	|	GREATER_EQUALS { $$ = new string(OPER_GE); }
	|	CONTAINED_IN { $$ = new string(OPER_CI); }
	|	SCONTAINED_IN { $$ = new string(OPER_SCI); }
	|	CONTAINS { $$ = new string(OPER_C); }
	|	SCONTAINS { $$ = new string(OPER_SC); }
	;

opadd:		PLUS { $$ = new string(OPER_PLUS); }
	|	MINUS { $$ = new string(OPER_MINUS); }
	|	UNION { $$ = new string(OPER_UNION); }
	;

opmult:		STAR { $$ = new string(OPER_STAR); }
	|	SLASH { $$ = new string(OPER_SLASH); }
	|	DIV { $$ = new string(OPER_DIV); }
	|	MOD { $$ = new string(OPER_MOD); }
	|	INTERSECTION { $$ = new string(OPER_INTERSECTION); }
	;

opunary:	MINUS { $$ = new string(OPER_MINUS); }
	|	PLUS { $$ = new string(OPER_PLUS); }
	|	NOT { $$ = new string(OPER_NOT); }
	;

expression:
		expression AND disyuntive_logical { $$ = new NodeConjLog(@$.begin.line,$1,$3); }
	|	disyuntive_logical { $$ = $1; }
	;

disyuntive_logical:
		disyuntive_logical OR relational1 { $$ = new NodeDisyLog(@$.begin.line,$1,$3); }
	|	relational1 { $$ = $1; }
	;

relational1:	relational1 oprelational1 in {$$=new NodeExpression(@$.begin.line,$1,*$2,$3);}
	|	in { $$ = $1; }
	;

in:		in IN relational2 { $$ = new NodeIn(@$.begin.line,$1,$3); }
	|	relational2 { $$ = $1; }
	;

relational2:	relational2 oprelational2 add { $$ = new NodeRelational(@$.begin.line,$1,*$2,$3); }
	|	add { $$ = $1; }
	;

add:		add opadd mult { $$ = new NodeAdd(@$.begin.line,$1,*$2,$3); }
	|	mult { $$ = $1; }
	;

mult:		mult opmult unary { $$ = new NodeMult(@$.begin.line,$1,*$2,$3); }
	|	unary { $$ = $1; }
	;

unary:		opunary delimiter { $$ = new NodeUnary(@$.begin.line,*$1,$2); }
	|	delimiter { $$ = $1; }
	;

delimiter:	LP expression RP { $$ = new NodeDelimiter(@$.begin.line,$2); }
	|	LBRACER expressions RBRACER { $$ = new NodeBracerDelimiter(@$.begin.line,$2); }
	| 	LBRACKET expressions RBRACKET { $$ = new NodeImmediateArray(@$.begin.line,$2); }   
     	|	expression_final  { $$ = $1; }
	;

expression_final:
		readable_expression { $$ = $1; }
	|	literal_constant { $$ = $1; }
	|	function_call { $$ = $1; }
	;

readable_expression:
		ID access { $$ = new NodeReadableExpression(@$.begin.line,*$1,$2); }
	|	ID { $$ = new NodeReadableExpression(@$.begin.line,*$1,nullptr); }
	;

function_call:	ID tuple { $$ = new NodeFunctionCall(@$.begin.line,*$1,$2); }
	|	ID LP RP { $$ = new NodeFunctionCall(@$.begin.line,*$1, new NodeExpNULL()); }
	;

literal_constant:
		LTRUE { $$ = new NodeLiteralLog(@$.begin.line,*(new string(LITERAL_TRUE))); }
	|	LFALSE { $$ = new NodeLiteralLog(@$.begin.line,*(new string(LITERAL_FALSE))); }
	|	LINTEGER { $$ = new NodeLiteralInt(@$.begin.line,$1); }
	|	LREAL { $$ = new NodeLiteralFloat(@$.begin.line,$1); }
	|	LCHARACTER { $$ = new NodeLiteralChar(@$.begin.line,$1); }
	|	LSTRING { $$ = new NodeLiteralString(@$.begin.line,*$1); }
	|	EMPTY_SET { $$ = new NodeLiteralEmptySet(@$.begin.line); }
	;

conditional:	IF expression { Algorithms::get_current_algorithm().new_scope(); }
		  code_block { Algorithms::get_current_algorithm().close_scope(); }
		ifelse { $$ = new NodeConditional(@$.begin.line,$2,$4,$6); }
	;

ifelse:		ELSE { Algorithms::get_current_algorithm().new_scope(); }
		  code_block { Algorithms::get_current_algorithm().close_scope(); $$ = $3; }
	|	{ $$ = nullptr; }
	;

iteration:	while_loop { $$ = $1; }
	|	repeat_loop { $$ = $1; }
	|	from_loop { $$ = $1; }
	|	forall_loop { $$ = $1; }
	;

while_loop:	WHILE expression { Algorithms::get_current_algorithm().new_scope(); }
		  code_block { Algorithms::get_current_algorithm().close_scope();
		  	       $$ = new NodeWhileLoop(@$.begin.line,$2,$4); }
	;

repeat_loop:	REPEAT { Algorithms::get_current_algorithm().new_scope(); }
		  code_block { Algorithms::get_current_algorithm().close_scope(); }
		repeat_loop_type { $$ = new NodeRepeatLoop(@$.begin.line,$3,$5); }
	;

repeat_loop_type:
		WHILE expression { $$ = new NodeRepeatWhileLoop(@$.begin.line,$2); }
	|	UNTIL expression { $$ = new NodeRepeatUntilLoop(@$.begin.line,$2); }
	;

from_loop: FROM { Algorithms::get_current_algorithm().new_scope(); }
	   simple_assign from_loop_type { $$ = new NodeFromLoop(@$.begin.line,$3,$4); }
	;
	
from_loop_type:
		from_loop_numeric { $$ = $1; }
	|	from_loop_general { $$ = $1; }

from_loop_numeric:
		UNTIL expression code_block
		{ $$ = new NodeFromNumericLoop(@$.begin.line,$2,$3);
		  Algorithms::get_current_algorithm().close_scope(); }
	|	UNTIL expression inc_or_dec expression code_block
		{ $$ = new NodeFromNumericIncDecLoop(@$.begin.line,$2,$3,$4,$5);
		  Algorithms::get_current_algorithm().close_scope(); }
	;

inc_or_dec:	INCREMENT { $$ = 1; }
	|	DECREMENT { $$ = 2; }
	;

from_loop_general:
		WHILE expression WITH simple_assign code_block
		{ Algorithms::get_current_algorithm().close_scope();
		  $$ = new NodeFromGeneralLoop(@$.begin.line,$2,$4,$5); }
	;

forall_loop:	FOR_ALL { Algorithms::get_current_algorithm().new_scope(); }
		forall_in code_block { $$ = new NodeForAllLoop(@$.begin.line,$3,$4);
			  	       Algorithms::get_current_algorithm().close_scope(); }
	;

forall_in: ID IN expression { $$ = new NodeForAllIn(@$.begin.line,*$1,$3); }

tuple:		LP expressions RP { $$ = new NodeTuple(@$.begin.line,$2); }
	;

expressions:	expression COMMA expressions { $$ = new NodeExpressions(@$.begin.line,$1,$3); }
	|	expression { $$ = $1; }
	;

%% /*** Additional Code ***/

void LEAC::Parser::error(const Parser::location_type& l,
			 const std::string& m)
{
  driver.error(l, m);
}
