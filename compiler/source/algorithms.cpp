#include "../include/algorithms.h"
#include "../include/stdalgorithms/std_read.h"
#include "../include/stdalgorithms/std_write.h"
#include "../include/stdalgorithms/std_extract.h"
#include "../include/stdalgorithms/std_length.h"

namespace LEAC{

  Algorithms::BaseAlgorithmsTable Algorithms::allAlgorithms;
  Algorithms::StdAlgorithmsTable Algorithms::stdAlgorithms;
  Algorithms::AlgorithmsTable Algorithms::algorithms;
  Algorithms::AlgorithmsIterator Algorithms::currentAlgorithm(algorithms.end());
  bool Algorithms::existsMain(false);

  void Algorithms::load_std_algorithms(){
    StdAlgorithm *aux = new StdRead();
    stdAlgorithms.insert(make_pair(aux->get_name(),aux));
    allAlgorithms.insert(make_pair(aux->get_name(),aux));

    aux = new StdWrite();
    stdAlgorithms.insert(make_pair(aux->get_name(),aux));
    allAlgorithms.insert(make_pair(aux->get_name(),aux));

    aux = new StdExtract();
    stdAlgorithms.insert(make_pair(aux->get_name(),aux));
    allAlgorithms.insert(make_pair(aux->get_name(),aux));

    aux = new StdLength();
    stdAlgorithms.insert(make_pair(aux->get_name(),aux));
    allAlgorithms.insert(make_pair(aux->get_name(),aux));
  }

  void Algorithms::define_algorithm(const string& name){
#ifdef ALGORITHMS_WARNINGS
    if (algorithms.find(name) != algorithms.end())
      cout << "Uso incorrecto de DEFINE_ALGORITHM()" << endl;
#endif
#ifdef ALGORITHMS_DEBUG
    cout << "#" << __func__ << ": " << name << endl;
#endif
    Algorithm *aux = new Algorithm(name);
    existsMain = existsMain || aux->is_main();
    algorithms.insert(make_pair(name,aux));
    currentAlgorithm = algorithms.find(name);
    allAlgorithms.insert(make_pair(name,aux));
  }

  void Algorithms::select_algorithm(const string& name){
    AlgorithmsIterator tmp = algorithms.find(name);
#ifdef ALGORITHMS_WARNINGS
    if (tmp == algorithms.end())
      cout << "Uso incorrecto de SELECT_ALGORIHTM()" << endl;
#endif
    currentAlgorithm->second->reset_scopes();
    currentAlgorithm = tmp;
  }

  void Algorithms::next_algorithm(){
    currentAlgorithm->second->reset_scopes();
    if(++currentAlgorithm == algorithms.end())
      currentAlgorithm = algorithms.begin();
  }

  bool Algorithms::is_defined_main_algorithm(){
    return existsMain;
  }

  bool Algorithms::is_std(const string& name){
    return stdAlgorithms.find(name) != stdAlgorithms.end();
  }
  
  bool Algorithms::check_algorithm(const string& name){
    return allAlgorithms.find(name) != allAlgorithms.end();
  }
  
  Algorithm& Algorithms::get_current_algorithm(){
#ifdef ALGORITHMS_WARNINGS
    if (currentAlgorithm == algorithms.end())
      cout << "Uso incorrecto de GET_CURRENT_ALGORITHM()" << endl;
#endif
    return *(currentAlgorithm->second);
  }
  
  Algorithm& Algorithms::get_algorithm(const string& name){
    AlgorithmsIterator tmp = algorithms.find(name);
#ifdef ALGORITHMS_WARNINGS
    if (tmp == algorithms.end())
      cout << "Uso incorrecto de GET_ALGORITHM()" << endl;
#endif
    return *(tmp->second);
  }

  void Algorithms::solve_all_variables_dependencies(){
    for (auto& algorithm : algorithms)
      algorithm.second->solve_dependencies();

    for (auto& algorithm : stdAlgorithms)
      algorithm.second->solve_dependencies();
  }

  void Algorithms::free_algorithms(){
    for (auto& algorithm : algorithms)
      algorithm.second = nullptr;
    for (auto& algorithm : stdAlgorithms)
      algorithm.second = nullptr;
    for (auto& algorithm : allAlgorithms)
      delete algorithm.second;
  }

  // DEBUGING
  void Algorithms::print_state(){
    for (auto& algorithm : algorithms){
      algorithm.second->print_algorithm();
    }
  }
} // namespace LEAC
