#include "../include/stdalgorithms/stdalgorithm.h"
#include "../include/stdalgorithms/std_read.h"
#include "../include/stdalgorithms/std_write.h"
#include "../include/stdalgorithms/std_extract.h"
#include "../include/stdalgorithms/std_length.h"

#include <iostream>

using std::cout;
using std::endl;
using std::to_string;

namespace LEAC{

  unsigned int StdAlgorithm::N = 0;

  bool StdAlgorithm::check_variable(const string& id){
    return find(id) != variable_not_found();
  }

  const Variable& StdAlgorithm::get_variable(const string& id){
    SymbolsIterator var = find(id);
#ifdef STDALG_WARNINGS
    if (var == variable_not_found())
      cout << "Uso incorrecto de STDALGORITHM::GET_VARIABLE(string)" << endl;
#endif
    return var->second;
  }

  SymbolsIterator StdAlgorithm::get_variable_reference(const string& id){
    SymbolsIterator var = find(id);
#ifdef STDALG_WARNINGS
    if (var == variable_not_found())
      cout << "Uso incorrecto de STDALGORITHM::GET_VARIABLE_REFERENCE(string)" << endl;
#endif
    return var;
  }

  void StdAlgorithm::set_type(const string& id, const Type& type){
    SymbolsIterator var = find(id);
#ifdef STD_WARNINGS
    if (var == variable_not_found())
      cout << "Uso incorrecto de STDALGORITHM::SET_TYPE(string,Type)" << endl;
#endif
    var->second.type = type;
  }

  void StdAlgorithm::print_algorithm(){
    for (auto& it : inputArgs){
      cout << get_tabs(0) << it.first << " " << it.second << endl;
    }
    for (auto& it : outputArgs){
      cout << get_tabs(0) << it.first << " " << it.second << endl;
    }
  }

  string StdAlgorithm::generate_aux_name() const{
    return to_string(N++) + "_variable_aux";
  }

  StdAlgorithm* StdAlgorithm::make_std(const string& name){
    if (name == STDALG_READ)
      return new StdRead();
    else if (name == STDALG_WRITE)
      return new StdWrite();
    else if (name == STDALG_EXTRACT)
      return new StdExtract();
    else if (name == STDALG_LENGTH)
      return new StdLength();

#ifdef STD_WARNINGS
    cout << "Uso incorrecto de STDALGORITHMS::MAKE_STD(string)" << endl;
#endif
    return nullptr;
  }
} // namespace LEAC
