#include <iostream>
#include <cstdlib>
#include <fstream>
#include <string>
#include <unistd.h>

#include "../include/driver.h"

using std::string;
using std::cout;
using std::endl;

int main(int argc, char **argv)
{
  LEACContext leac;
  LEAC::Driver driver(leac);
  bool help = true;
  string filename;
  int opt;
  while ((opt = getopt(argc, argv, ":c:o:hpse")) != -1){
    switch (opt){
    case 'c':
      help = false;
      filename = optarg;
      break;
    case 'o':
      driver.outputName = optarg;
      break;
    case 'h':
      help = true;
      break;
    case 'p':
      driver.trace_parsing = true;
      break;
    case 's':
      driver.trace_scanning = true;
      break;
    case 'e':
      driver.coutFlag = true;
      break;
    default:
      help = true;
    }
  }

  if (help){
    cout << "# Ayuda Compilador de LEA #" << endl;
    cout << "Opciones:" << endl;
    cout << "\t-c file.lea\tIndica fichero lea a compilar." << endl;
    cout << "\t[-o file.xml]\tEspecifica el fichero de salida del programa." << endl;
    cout << "\t[-e]\t\tActiva la salida estándar." << endl;
    cout << "\t[-p]\t\tActiva el modo trace parsing." << endl;
    cout << "\t[-s]\t\tActiva el modo trace scanning." << endl;
    cout << "\t[-h]\t\tMuestra esta ayuda." << endl;
  }
  else{
    // read a file with expressions
    std::fstream infile(filename);
    if (!infile.good())
      {
	std::cerr << "No se ha podido abrir el fichero: " << filename << std::endl;
	return 0;
      }	    
    driver.parse_stream(infile, filename);
  }
  return 0;
}
