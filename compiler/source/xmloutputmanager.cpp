#include "../include/managers/xmloutputmanager.h"
#include "../include/defstrings.h"

#ifdef XMLOUTPUTMANAGER_WARNINGS
#include <iostream>
using std::cout;
#endif
using std::to_string;
using std::endl;

namespace LEAC{

  stack<XMLOutputManager::Tag> XMLOutputManager::tags;
  set<string> XMLOutputManager::orderWhitelist =
    {
      XMLTAG_ARG, XMLTAG_INSTRUCTION, XMLTAG_NEW_VAR, XMLTAG_NEW_ARRAY,
      XMLTAG_EXPRESSION, XMLTAG_VARIABLE
    };

  int XMLOutputManager::nTabs(0);

  void XMLOutputManager::open_tag(stringstream& os, const string& tag,
				  const AttributesNames& attrNames,
				  const AttributesValues& attrValues,
				  bool close){
    os << get_tabs() << "<" << tag << " ";
    if (is_order_whitelisted(tag)){
      write_order(os);
    }
    write_attributes(os,attrNames,attrValues);
    if (close){
      os << "/>" << endl;
    }
    else{
      os << ">" << endl;
      tags.push(Tag(tag,0));
      ++nTabs;
    }
  }

  void XMLOutputManager::close_tag(stringstream& os){
#ifdef XMLOUTPUTMANAGER_WARNINGS
    if (tags.empty())
      cout << "Uso ilegal de CLOSE_TAG('tags esta vacio')" << endl;
    if (nTabs <= 0)
      cout << "Uso ilegal de CLOSE_TAG('nTabs <= 0')" << endl;
#endif
  
    --nTabs;
    os << get_tabs() << "</" << tags.top().tagName << ">" << endl;
  
    tags.pop();
  }

  void XMLOutputManager::write_attributes(stringstream& os,
					  const AttributesNames& attrNames,
					  const AttributesValues& attrValues){
#ifdef XMLOUTPUTMANAGER_WARNINGS
    if (attrNames.size() != attrValues.size())
      cout << "Uso ilegal de WRITE_ATTRIBUTES('attrNames.size() != attrValues.size()')" << endl;
#endif

    if (!attrNames.empty()){
      size_t size = attrNames.size();
      for (unsigned int i=0; i<size; ++i){
	os << attrNames[i] << "='" << attrValues[i] << "' ";
      }
    }
  }

  void XMLOutputManager::write_order(stringstream& os){
#ifdef XMLOUTPUTMANAGER_WARNINGS
    if (tags.empty())
      cout << "Uso ilegal de WRITE_ORDER('la pila esta vacia')" << endl;
#endif

    os << ATTR_ORDER << "='" << tags.top().order << "' ";
    ++tags.top().order;
  }

  bool XMLOutputManager::is_order_whitelisted(const string& tag){
    return orderWhitelist.find(tag) != orderWhitelist.end();
  }

  string XMLOutputManager::get_tabs(){
    string tabs = "";
    for (int i=0; i<nTabs; ++i)
      tabs += "\t";
    return tabs;
  }

#ifdef XMLOUTPUTMANAGER_DEBUG
  void XMLOutputManager::print_state(){
    cout << "\t# Estado de XMLOutputManager #" << endl;
    cout << "Contenido de tags: " << endl;
    stack<Tag> aux = tags;
    while(!aux.empty()){
      Tag m = aux.top();
      cout << "<" << m.tagName << "," << m.order << ">" << endl;
      aux.pop();
    }
    cout << "Numero de tabuladores = " << nTabs << endl;
  }

} // namespace LEAC

#endif
