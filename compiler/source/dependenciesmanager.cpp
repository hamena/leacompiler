#include "../include/managers/dependenciesmanager.h"
#include "../include/type.h"
#include "../include/algorithms.h"
#include "../include/managers/conversionmanager.h"
#include "../include/nodes/nodeexpressions.h"
#include <string>

using std::to_string;

namespace LEAC{

  map <SymbolsIterator,DependenciesManager::Dependencies> DependenciesManager::dependenciesTable;
  vector<SymbolsIterator> DependenciesManager::bufferVariables;
  vector<NodeExp*> DependenciesManager::bufferExpressions;
  vector<int> DependenciesManager::bufferAccess;
  vector<NodeExp*> DependenciesManager::functionCalls;

  void DependenciesManager::new_dependency(const SymbolsIterator& var, NodeExp* exp, int dim){
    Type t;
    t.set_tmpexp();
    t.set_dimension(dim);
    save_dependency(var,exp,t);

#ifdef DEPENDENCIESMANAGER_DEBUG
    cout << "#" << __func__ << ": Nueva dependencia "
	 << var->first << " <-- " << exp << "[" << dim << "]" << endl;
#endif
  }

  void DependenciesManager::new_in_dependency(const SymbolsIterator& var, NodeExp* exp){
    Type t = Type::make_set(Type());
    t.set_tmpexp();
    save_dependency(var,exp,t);
  }
					
  void DependenciesManager::push_variable(const SymbolsIterator& var, int nAccess){
    bufferVariables.push_back(var);
    bufferAccess.push_back(nAccess);
    var->second.type.set_tmpexp();
  
#ifdef DEPENDENCIESMANAGER_DEBUG
    cout << "#" << __func__ << ": push_back " << var->first << "[" << nAccess << "]" << endl;
#endif
  }

  void DependenciesManager::push_expression(NodeExp* node){
    bufferExpressions.push_back(node);

#ifdef DEPENDENCIESMANAGER_DEBUG
    cout << "#" << __func__ << ": push_back " << node << endl;
#endif

    if (bufferExpressions.size() == bufferVariables.size()){
      save_buffers();
      reset_buffers();
    }
  }

  void DependenciesManager::push_algorithm(AlgorithmBase& alg){
    Type t;
    t.set_tmpexp();
    for (unsigned int i=0; i<bufferVariables.size(); ++i){
      functionCalls.push_back(new NodeFunctionCallAux(-1,alg,i));
      t.set_dimension(bufferAccess[i]);
      save_dependency(bufferVariables[i],functionCalls.back(),t);
    }
    reset_buffers();
  }

  void DependenciesManager::free_function_calls(){
    for (unsigned int i=0; i<functionCalls.size(); ++i)
      if (functionCalls[i])
	delete functionCalls[i];
  }

  void DependenciesManager::reset_dependencies(const SymbolsIterator& var){
#ifdef DEPENDENCIESMANAGER_DEBUG
    cout << "#" << __func__ << ": reseteando vector de dependencias de "
	 << var->first << " (" << &(var->first) << ")" << endl;
#endif
    Dependencies& dependencies = dependenciesTable[var];
    for (auto& it : dependencies)
      it.second.set_tmpexp();
  }

  void DependenciesManager::solve_variable_dependencies(SymbolsIterator& var){
#ifdef DEPENDENCIESMANAGER_DEBUG
    cout << "#" << __func__ << ": resolviendo dependencias de la variable "
	 << var->first << " (" << &(var->first) << ")" << endl;
#endif
    Dependencies& dependencies = dependenciesTable.find(var)->second;
    Types newTypes = get_new_types(dependencies);
    if (!newTypes.empty()){ // si se han resuelto las dependencias: reducir y guardar tipo
      Type reducedType = reduce_type(newTypes);
      if (reducedType.is_ready())
	save_type(var,reducedType);
      else{
	bool aux = true;
	for (auto& t : newTypes)
	  if (t.is_null())
	    aux = false;
	if (aux)
	  ErrorManager::inference_type_conflict(var->first,newTypes);
      }
    }
  }

  void DependenciesManager::save_dependency(const SymbolsIterator& var, NodeExp* exp,
					    const Type& t){
    if (dependenciesTable.find(var) == dependenciesTable.end())
      dependenciesTable.insert(make_pair(var,Dependencies(1,make_pair(exp,t))));
    else
      dependenciesTable[var].push_back(make_pair(exp,t));
  }

  void DependenciesManager::save_buffers(){
    Type t;
    t.set_tmpexp();
    t.set_dimension(0);
#ifdef DEPENDENCIESMANAGER_DEBUG
    cout << "#" << __func__ << ": " << endl;
#endif
  
    while (!bufferExpressions.empty() && !bufferVariables.empty()){
      SymbolsIterator var = bufferVariables.back(); 
      NodeExp* node = bufferExpressions.back();
      t.set_dimension(bufferAccess.back());
      save_dependency(var,node,t);

#ifdef DEPENDENCIESMANAGER_DEBUG
      cout << "Nueva dependencia: "
	   << var->first << " <-- " << node << "[" << bufferAccess.back() << "]" << endl;
#endif

      bufferVariables.pop_back();
      bufferExpressions.pop_back();
      bufferAccess.pop_back();
    }
  }

  void DependenciesManager::reset_buffers(){
    bufferVariables = vector<SymbolsIterator>();
    bufferExpressions = vector<NodeExp*>();
    bufferAccess = vector<int>();

#ifdef DEPENDENCIESMANAGER_DEBUG
    cout << "#" << __func__ << ": buffers reseteados" << endl;
#endif
  }

  Types DependenciesManager::get_new_types(Dependencies& dependencies){
#ifdef DEPENDENCIESMANAGER_DEBUG
    cout << "#" << __func__ << ": [";
    for (auto& it : dependencies)
      cout << it.first << ", ";
    cout << "]" << endl;
#endif
    Types newTypes;
    for (unsigned int i=0; i<dependencies.size(); ++i){
      if (dependencies[i].second.is_tmpexp()){ // es dependencia
	dependencies[i].second.set_cutway();
	// get_type siempre devuelve un tipo READY(resuelto) o UNDEF(error) o PUTOFF(ignorar)
	Type expressionType = dependencies[i].first->get_type();
	if (expressionType.is_ready()){ // si la dependencia se ha resuelto
	  int dim = dependencies[i].second.get_dimension();
	  if (dim > 0){ // existe acceso a la expresion modificable en asignacion
	    if (expressionType.is_array())
	      expressionType.set_dimension(expressionType.get_dimension()+dim);
	    else
	      expressionType = Type::make_array(expressionType,dim);
	  }
	  else if (dependencies[i].second.is_set())
	    expressionType = expressionType.get_sub_type();

	  newTypes.push_back(expressionType);
	  dependencies[i].second = expressionType;
#ifdef DEPENDENCIESMANAGER_DEBUG
	  cout << "dependencia " << dependencies[i].first
	       <<  " resuelta en " << expressionType << endl;
#endif
	}
      }
      else if (dependencies[i].second.is_ready()){
	newTypes.push_back(dependencies[i].second);
#ifdef DEPENDENCIESMANAGER_DEBUG
	cout << "dependencia " << dependencies[i].first
	     << " previamente resuelta en " << dependencies[i].second << endl;
#endif
      }
    }
  
#ifdef DEPENDENCIESMANAGER_DEBUG
    cout << "tipos finales : [";
    for (auto& it : newTypes)
      cout << it << ", ";
    cout << "]" << endl;
#endif
  
    return newTypes;
  }

  void DependenciesManager::save_type(SymbolsIterator& var, const Type& reducedType){
#ifdef DEPENDENCIESMANAGER_DEBUG
    cout << "#" << __func__ << ": " << endl;
#endif
    if (var->second.type.is_null()){ // no hay tipo previo
      var->second.type = reducedType;
    }
    else{ // hay tipo previo
      if (ConversionManager::is_legal(var->second.type,reducedType) ||
	  ConversionManager::is_legal(reducedType,var->second.type)){ // conversion legal
	var->second.type = ConversionManager::solve(var->second.type,reducedType);
      }
      else{ // conversion no legal
	if (!reducedType.is_undef())
	  ErrorManager::inference_previous_type_conflict(var->first,var->second.type,reducedType);
      }
    }
#ifdef DEPENDENCIESMANAGER_DEBUG
    cout << var->first << " <-- " << reducedType << endl;
#endif
  }

  Type DependenciesManager::reduce_type(const Types& vTypes){
    if (vTypes.size() == 1)
      return vTypes[0];
  
    Type res, aux;
    Types types = vTypes;

#ifdef DEPENDENCIESMANAGER_DEBUG
    cout << "#" << __func__ << ":" << endl;
    cout << "Tipo sin reducir: " << vTypes << endl;
#endif
  
    res = types.back();
    types.pop_back();

    while(!types.empty()){
      aux = types.back();
      types.pop_back();
      if (ConversionManager::is_legal(aux,res) || ConversionManager::is_legal(res,aux)){
	res = ConversionManager::solve(res,aux);
      }
      else{
#ifdef DEPENDENCIESMANAGER_DEBUG
	cout << "Reduccion no legal" << endl;
#endif
	return Type::make_undef();
      }
    }

#ifdef DEPENDENCIESMANAGER_DEBUG
    cout << "Tipo reducido: " << res << endl;
#endif

    return res;
  }
} // namespace LEAC
