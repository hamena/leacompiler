#include "../include/algorithmbase.h"
#include "../include/type.h"
#include "../include/managers/dependenciesmanager.h"

using std::cout;
using std::to_string;

namespace LEAC{

  unsigned int Variable::variableCounter(0);

  Variable::Variable(const string& id, const Type& t) :
    codeName(UNDEFINED_CODE_NAME), type(t)
  {
    codeName = "variable_" + id + "_" + to_string(variableCounter++);
#ifdef ALGORITHMS_DEBUG
    cout << "Nueva variable " << codeName << " creada." << endl;
#endif
  }

  ostream& operator <<(ostream& os, const Variable& var){
    os << var.type << "\tcodename: " << var.codeName;
    return os;
  }

  bool operator <(const SymbolsIterator& var1, const SymbolsIterator& var2){
    return &(var1->first) < &(var2->first);
  }

  AlgorithmBase::AlgorithmBase(const string& name) :
    algorithmName(name),
    algorithmCodeName(UNDEFINED_CODE_NAME)
  {
#ifdef ALGBASE_DEBUG
    cout << "#Constructor algoritmo " << algorithmName << endl;
#endif
  }

  void AlgorithmBase::new_input_argument(const string& id){
#ifdef ALGBASE_WARNINGS
    if (is_input_arg(id))
      cout << "Uso incorrecto de NEW_INPUT_ARGUMENT()" << endl;
#endif
    Type aux;
    aux.set_tmpexp();
    Variable var(id,aux);
    inputNames.push_back(id);
    inputArgs.insert(make_pair(id,var));
#ifdef ALGBASE_DEBUG
    cout << "#" << __func__ << ": " << "Argumento de entrada " << id  << " añadido con éxito." << endl;
#endif
  }

  void AlgorithmBase::new_output_argument(const string& id){
#ifdef ALGBASE_WARNINGS
    if (is_output_arg(id))
      cout << "Uso incorrecto de NEW_OUTPUT_ARGUMENT()" << endl;
#endif
    Type aux;
    aux.set_tmpexp();
    Variable var(id,aux);
    outputNames.push_back(id);
    outputArgs.insert(make_pair(id,var));
#ifdef ALGBASE_DEBUG
    cout << "#" << __func__ << ": " << "Argumento de salida " << id << " añadido con éxito." << endl;
#endif
  }

  void AlgorithmBase::set_input_dependencies(const vector<NodeExp*>& deps){
#ifdef ALGBASE_DEBUG
    cout << "#" << __func__ << ": " << "[";
    for (auto& it : deps)
      cout << it << ", ";
    cout << "]" << endl;
#endif
    int i = 0;
    for (auto& it : inputNames){
      SymbolsIterator var = inputArgs.find(it);
      DependenciesManager::new_dependency(var,deps[i++]);
      var->second.type.set_tmpexp();
    }
#ifdef ALGBASE_DEBUG
    cout << "Dependencias guardadas con éxito." << endl;
#endif
  }

  bool AlgorithmBase::is_input_arg(const string& id){
    return inputArgs.find(id) != inputArgs.end();
  }

  bool AlgorithmBase::is_output_arg(const string& id){
    return outputArgs.find(id) != outputArgs.end();
  }

  string AlgorithmBase::get_name() const{
    return algorithmName;
  }

  string AlgorithmBase::get_code_name(){
    if (algorithmCodeName == UNDEFINED_CODE_NAME){
      algorithmCodeName = "algoritmo_" + to_string(inputArgs.size()) + "_";
      algorithmCodeName += to_string(outputArgs.size()) + "_" + algorithmName;
    }
    if (algorithmName == MAIN_ALGORITHM_NAME)
      algorithmCodeName = algorithmName;
  
    return algorithmCodeName;
  }

  unsigned int AlgorithmBase::get_n_input() const{
    return inputArgs.size();
  }

  unsigned int AlgorithmBase::get_n_output() const{
    return outputArgs.size();
  }

  Types AlgorithmBase::get_input_types() const{
    Types tmp;
    for (auto& it : inputNames)
      tmp.push_back(inputArgs.at(it).type);
    return tmp;
  }

  Types AlgorithmBase::get_output_types() const{
    Types tmp;
    for (auto& it : outputNames)
      tmp.push_back(outputArgs.at(it).type);
    return tmp;
  }

  vector<SymbolsIterator> AlgorithmBase::get_output_references(){
    vector<SymbolsIterator> references;
    for (unsigned int i=0; i<outputNames.size(); ++i)
      references.push_back(outputArgs.find(outputNames[i]));
    return references;
  }

  SymbolsIterator AlgorithmBase::find(const string& id){
    SymbolsIterator var;
    if ((var = inputArgs.find(id)) != inputArgs.end())
      return var;
    if ((var = outputArgs.find(id)) != outputArgs.end())
      return var;
    return variable_not_found();
  }

  SymbolsIterator AlgorithmBase::variable_not_found(){
    return outputArgs.end();
  }

  string AlgorithmBase::get_tabs(int tabs){
    string str = "";
    for (int i=0; i<tabs; ++i)
      str += "\t";
    return str;
  }
} // namespace LEAC
