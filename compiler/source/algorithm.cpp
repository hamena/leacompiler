#include "../include/algorithm.h"
#include "../include/type.h"
#include "../include/managers/dependenciesmanager.h"
#include "../include/nodes/nodeexpressions.h"

namespace LEAC{

  // PUBLIC

  Algorithm::Algorithm(const string& name) :
    AlgorithmBase(name),
    initialScope(new Scope()),
    currentScope(nullptr)
  {
    currentScope = initialScope;
  }
  
  void Algorithm::new_scope(){
#ifdef ALGORITHM_WARNINGS
    if (!currentScope)
      cout << "Situacion ilegal. NEW_SCOPE()" << endl;
#endif
    currentScope->nextScopes.push_back(new Scope(*currentScope));
    currentScope = currentScope->nextScopes.back();
#ifdef ALGORITHM_DEBUG
    cout << "#" << __func__ << ": " << " Nuevo ambito creado con exito." << endl;
#endif
  }

  void Algorithm::first_scope(){
    currentScope = initialScope;
  }

  void Algorithm::next_scope(){
#ifdef ALGORITHM_WARNINGS
    if (currentScope->scopeIndex >= (int)currentScope->nextScopes.size()){
      cout << "Situación ilegal. NEXT_SCOPE()" << endl;
      cout << "index: " << currentScope->scopeIndex << " - "
	   << currentScope->nextScopes.size() << endl;
    }
#endif

    currentScope = currentScope->nextScopes[currentScope->scopeIndex++];
#ifdef ALGORITHM_WARNINGS
    if (!currentScope)
      cout << "Puntero nulo. NEXT_SCOPE()" << endl;
#endif
    currentScope->scopeIndex = 0;
  }

  void Algorithm::close_scope(){
#ifdef ALGORITHM_WARNINGS
    if (!currentScope->previousScope)
      cout << "Situacion ilegal. CLOSE_SCOPE()" << endl;
#endif
    currentScope->scopeIndex = 0;
    currentScope = currentScope->previousScope;
  }

  void Algorithm::reset_scopes(){
    reset_scopes_rec(initialScope);
    currentScope = initialScope;
  }

  void Algorithm::new_variable(const string& id, const Type& type){
#ifdef ALGORITHM_WARNINGS
    if (check_variable(id))
      cout << "Uso incorrecto de NEW_VARIABLE()" << endl;
#endif
    Variable var(id,type);
    currentScope->symbolsTable.insert(make_pair(id,var));
#ifdef ALGORITHM_DEBUG
    cout << "#" << __func__ << ": " << "Variable " << id << " añadida con éxito." << endl;
#endif
  }

  void Algorithm::set_type(const string& id, const Type& type){
#ifdef ALGORITHM_WARNINGS
    if (!check_variable(id))
      cout << "Uso incorrecto de SET_TYPE()" << endl;
#endif
    if (is_output_arg(id)){
      outputArgs[id].type = type;
    }
    else
      currentScope->symbolsTable[id].type = type;
#ifdef ALGORITHM_DEBUG
    cout << "#" << __func__ << ": " << "Sustituido tipo de " << id << " por " << type << endl;
#endif
  }

  void Algorithm::solve_dependencies(){
    bool control = true;
    bool existsTmpexp = false;
    while(control){
      control = false;
      existsTmpexp = false;
#ifdef ALGORITHM_DEBUG
      cout << "#" << __func__ << ": " << endl;
      cout << "Resolviendo argumentos de entrada." << endl;
#endif
      for (SymbolsIterator input=inputArgs.begin(); input != inputArgs.end(); ++input){
	if (input->second.type.is_tmpexp()){
	  DependenciesManager::reset_dependencies(input);
	  DependenciesManager::solve_variable_dependencies(input);
	  control = control || input->second.type.is_ready();
	  existsTmpexp = existsTmpexp || input->second.type.is_tmpexp();
	}
      }
#ifdef ALGORITHM_DEBUG
      if (!control)
	cout << "Todos los argumentos de entrada resueltos" << endl;
      cout << "Resolviendo argumentos de salida." << endl;
#endif
      for (SymbolsIterator output=outputArgs.begin(); output != outputArgs.end(); ++output){
	if (output->second.type.is_tmpexp()){
	  DependenciesManager::reset_dependencies(output);
	  DependenciesManager::solve_variable_dependencies(output);
	  control = control || output->second.type.is_ready();
	  existsTmpexp = existsTmpexp || output->second.type.is_tmpexp();
	}
      }
#ifdef ALGORITHM_DEBUG
      if (!control)
	cout << "Todos los argumentos de salida resueltos" << endl;
      cout << "Resolviendo cuerpo de la funcion" << endl;
#endif
      control = control || solve_scope_dependencies(initialScope,existsTmpexp);
    }

    if (existsTmpexp)
      ErrorManager::inference_impossible(get_name());
  }

  void Algorithm::solve_output_dependencies(){
#ifdef ALGORITHM_DEBUG
    cout << "#" << __func__ << ": " << "Resolviendo dependencias de los argumentos de salida" << endl;
#endif
    for (SymbolsIterator it=outputArgs.begin(); it != outputArgs.end(); ++it){
      if (it->second.type.is_tmpexp())
	DependenciesManager::solve_variable_dependencies(it);
    }
  }

  const Variable& Algorithm::get_variable(const string& id){
    SymbolsIterator it = find(id);
#ifdef ALGORITHM_WARNINGS
    if (it == variable_not_found())
      cout << "Uso incorrecto de GET_VARIABLE()" << endl;
#endif
    return it->second;
  }

  SymbolsIterator Algorithm::get_variable_reference(const string& id){
    SymbolsIterator ref = find(id);
#ifdef ALGORITHM_WARNINGS
    if (ref == variable_not_found())
      cout << "Uso incorrecto de GET_VARIABLE_REFERENCE()" << endl;
#endif
    return ref;
  }

  bool Algorithm::is_main() const{
    return get_name() == MAIN_ALGORITHM_NAME;
  }

  bool Algorithm::check_variable(const string& id){
    return find(id) != variable_not_found();
  }

  bool Algorithm::is_arg(const string& id){
    return is_input_arg(id) || is_output_arg(id);
  }

  void Algorithm::print_algorithm(){
    cout << "###### " << get_name() << " ######" << endl;
    print_arguments(0);
    print_scopes(initialScope,1);
  }

  // PRIVATE

  SymbolsIterator Algorithm::find(const string& id){
    SymbolsIterator it;
    if ((it = inputArgs.find(id)) != inputArgs.end())
      return it;

    if ((it = outputArgs.find(id)) != outputArgs.end())
      return it;
      
    if ((it = currentScope->symbolsTable.find(id)) != currentScope->symbolsTable.end())
      return it;

    Scope *paux = currentScope;
    while (paux->previousScope){
      paux = paux->previousScope;

      if ((it = paux->symbolsTable.find(id)) != paux->symbolsTable.end())
	return it;
    }
  
    return variable_not_found();
  }
     
  SymbolsIterator Algorithm::variable_not_found(){
    return currentScope->symbolsTable.end();
  }

  void Algorithm::reset_scopes_rec(Scope* scope){
    for (unsigned int i=0; i<scope->nextScopes.size(); ++i)
      reset_scopes_rec(scope->nextScopes[i]);
    scope->scopeIndex = 0;
  }

  void Algorithm::delete_scopes(Scope* scope){
    if (scope->nextScopes.size() > 0){
      for (size_t i=0; i<scope->nextScopes.size(); ++i){
	delete_scopes(scope->nextScopes[i]);
	delete scope->nextScopes[i];
      }
    }
  }

  bool Algorithm::solve_scope_dependencies(Scope* scope, bool& existsTmpexp){
    bool control = false;
    SymbolsTable& variables = scope->symbolsTable;
    for (SymbolsIterator var=variables.begin(); var != variables.end(); ++var){
      if (var->second.type.is_tmpexp()){
	DependenciesManager::reset_dependencies(var);
	DependenciesManager::solve_variable_dependencies(var);
	control = control || var->second.type.is_ready();
	existsTmpexp = existsTmpexp || var->second.type.is_tmpexp();
      }
    }

    size_t size = scope->nextScopes.size();
    for (unsigned int i=0; i<size; ++i)
      control = control || solve_scope_dependencies(scope->nextScopes[i],existsTmpexp);

    return control;
  }

  Algorithm::~Algorithm(){
    delete_scopes(currentScope);
    initialScope = currentScope = nullptr;
  }

  void Algorithm::print_arguments(int tabs){
    for (auto& it : inputArgs){
      cout << get_tabs(tabs) << it.first << " " << it.second << endl;
    }

    for (auto& it : outputArgs){
      cout << get_tabs(tabs) << it.first << " " << it.second << endl;
    }
  }

  void Algorithm::print_scopes(Scope* scope, int tabs){
    for (auto& symbols : scope->symbolsTable){
      cout << get_tabs(tabs) << symbols.first << " ";
      cout << symbols.second << endl;
    }

    for (auto& it : scope->nextScopes)
      print_scopes(it,tabs+1);
  }
} // namespace LEAC
