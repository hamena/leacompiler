#include "../include/managers/errormanager.h"

#include <iostream>
#include <utility>
#include <cmath>
#include "../include/algorithms.h"
#include "../include/algorithm.h"
#include "../include/type.h"

using namespace std;

namespace LEAC{

  bool ErrorManager::error(false);

  bool ErrorManager::check_error(){
    return error;
  }

  void ErrorManager::dimension_doesnt_match(int lno, const string& tostr){
    cout << get_header(tostr,lno) << "Las dimensiones no coinciden." << endl;
  }

  void ErrorManager::access_not_integer(int lno, const string& tostr, const Types& args){
    cout << get_header(tostr,lno) << "El acceso \"[" << args
	 << "]\" no está compuesto por expresiones enteras." << endl;
  }

  void ErrorManager::expression_not_type(int lno, const string& tostr, const string& lex,
					 const Type& rex){
    cout << get_header(tostr,lno) << "La expresión \"" << lex
	 << "\" no es de tipo " << rex << "." << endl;
  }

  void ErrorManager::bexpression_not_legal(int lno, const string& tostr, const Type& lex,
					   const string& op, const Type& rex){
    cout << get_header(tostr,lno,NOPRINT) << "La expresión binaria \""
	 << lex << " " << op << " " << rex << "\" no es legal." << endl;
  }

  void ErrorManager::boperator_doesnt_match(int lno, const string& tostr, const string& op){
    cout << get_header(tostr,lno) << "El operador binario '" << op
	 << "' no es capaz de operar con tuplas de expresiones." << endl;
  }

  void ErrorManager::uexpression_not_legal(int lno, const string& tostr, const string& op,
					   const Type& rex){
    cout << get_header(tostr,lno,NOPRINT) << "La expresión unaria \""
	 << op << " " << rex << "\" no es legal." << endl;
  }

  void ErrorManager::uoperator_doesnt_match(int lno, const string& tostr, const string& op){
    cout << get_header(tostr,lno) << "El operador unario '" << op
	 << "' no es capaz de operar con tuplas de expresiones." << endl;
  }

  void ErrorManager::delimiter_doesnt_match(int lno, const string& tostr){
    cout << get_header(tostr,lno)
	 << "Los delimitadores '( )' esperan una única expresión." << endl;
  }

  void ErrorManager::simple_assign_not_legal(int lno, const string& tostr, const Type& lex,
					     const Type& rex){
    cout << get_header(tostr,lno) << "La asignación \""
	 << lex << " <-- " << rex << "\" no es legal." << endl;
  }

  void ErrorManager::simple_assign_doesnt_match(int lno, const string& tostr){
    cout << get_header(tostr,lno) << "Solo es posbile asignar una única expresión." << endl;
  }

  void ErrorManager::parallel_assign_not_legal(int lno, const string& tostr, const Types& lexs,
					       const Types& rexs){
    cout << get_header(tostr,lno) << "La asignación paralela \"("
	 << lexs << ") <-- (" << rexs << ")\" no es legal." << endl;
  }

  void ErrorManager::parallel_assign_doesnt_match(int lno, const string& tostr){
    cout << get_header(tostr,lno)
	 << "El número de expresiones a asignar no coinciden con el número de variables."
	 << endl;
  }

  void ErrorManager::swap_assign_not_legal(int lno, const string& tostr, const Type& lex,
					   const Type& rex){
    cout << get_header(tostr,lno) << "La asignación cruzada \""
	 << lex << " <-> " << rex << "\" no es legal." << endl;
  }

  void ErrorManager::swap_assign_doesnt_match(int lno, const string& tostr){
    cout << get_header(tostr,lno)
	 << "El intercambio asigna mutuamente dos únicas expresiones." << endl;
  }

  void ErrorManager::parameters_not_legal(int lno, const string& tostr, const Types& args,
					  const string& name){
    cout << get_header(tostr,lno) << "La llamada '" << name << "(" << args
	 << ")' no es legal." << endl;
  }

  void ErrorManager::variable_not_found(int lno, const string& tostr, const string& id){
    cout << get_header(tostr,lno) << "La variable '" << id << "' no existe." << endl;
  }

  void ErrorManager::algorithm_not_found(int lno, const string& tostr, const string& name){
    cout << get_header(tostr,lno) << "La función '" << name << "' no existe." << endl;
  }

  void ErrorManager::algorithm_already_defined(int lno, const string& tostr, const string& name){
    cout << get_header(tostr,lno) << "Algoritmo '" << name
	 << "' definido con anterioridad." << endl;
  }

  void ErrorManager::argument_conflict(int lno, const string& tostr, const string& id){
    cout << get_header(tostr,lno) << "El argumento '" << id
	 << "' hace conflicto con otro argumento." << endl;
  }

  void ErrorManager::variable_already_exists(int lno, const string& tostr, const string& id){
    cout << get_header(tostr,lno) << "La variable '"+id+"' ya existe." << endl;
  }

  void ErrorManager::input_arg_only_read(int lno, const string& tostr, const string& id){
    cout << get_header(tostr,lno)
	 << "El argumento de entrada '"+id+"' es de sólo lectura." << endl;
  }

  void ErrorManager::output_arg_only_write(int lno, const string& tostr, const string& id){
    cout << get_header(tostr,lno) << "El argumento de salida '"+id+"' es de sólo escritura." << endl;
  }

  void ErrorManager::function_call_args_doesnt_match(int lno, const string& tostr,
						     const string& name, unsigned int nargs){
    cout << get_header(tostr,lno,NOPRINT)
	 << "La llamada a la funcion '"+name+"' debe tener exactamente "
	 << nargs << " argumentos de entrada." << endl;
  }

  void ErrorManager::immediate_array_not_legal(int lno, const string& tostr, const Types& types){
    cout << get_header(tostr,lno,NOPRINT) << "Los tipos" << types
	 << " no forman un array legal." << endl;
  }

  void ErrorManager::immediate_set_not_legal(int lno, const string& tostr, const Types& types){
    cout << get_header(tostr,lno,NOPRINT) << "Los tipos " << types
	 << " no forman un conjunto legal." << endl;
  }

  void ErrorManager::main_not_input_argument(int lno, const string& tostr, const string& id){
    cout << get_header(tostr,lno) << "El algoritmo main no admite el argumento de entrada "
	 << id << "." << endl;
  }

  void ErrorManager::main_not_output_argument(int lno, const string& tostr, const string& id){
    cout << get_header(tostr,lno) << "El algoritmo main no admite el argumento de salida "
	 << id << "." << endl;
  }

  void ErrorManager::main_not_call(int lno, const string& tostr){
    cout << get_header(tostr,lno) << "El algoritmo main no puede ser llamado." << endl;
  }
  
  void ErrorManager::main_doesnt_exists(){
    cout << get_header("",-1,NOPRINT) << "El algoritmo main no está definido." << endl;
  }

  void ErrorManager::inference_impossible(const string& alg){
    cout << get_header("",-1,alg)
	 << "Existen variables que no se les han podido inferir su tipo"
	 << ", información insuficiente." << endl;
  }

  void ErrorManager::inference_type_conflict(const string& name, const Types& types){
    cout << get_header("",-1,NOPRINT,true)
	 << "Los tipos inferidos de la variable '"+name+"' son incompatibles: ";
    for (auto& it : types)
      cout << it << ", ";
    cout << endl;
  }

  void ErrorManager::inference_previous_type_conflict(const string& name,
						      const Type& ptype, const Type& newtype){
    cout << get_header("",-1,NOPRINT,true)
	 << "La variable '"+name+"' tiene un tipo previamente declarado "
	 << ptype << " que hace conflicto con el tipo inferido " << newtype << endl;
  }

  void ErrorManager::std_algorithm_illegal(const string& tostr, const string& algName,
					   const Types& inferredInput, const Types& modelInput){
    cout << get_header(tostr,-1,NOPRINT) << "La función estándar '" << algName
	 << "' es llamada en un contexto semántico ilegal. " << endl;
    cout << "\tLos tipos de entrada deberían de ser '" << modelInput
	 << "' pero los tipos proporcionados son '" << inferredInput << "'." << endl;
  }

  string ErrorManager::get_header(string tostr, int lno, string alg, bool inf){
    if (!error)
      error = true;

    string lineno;
    if (!inf){
      if (alg == "")
	alg = Algorithms::get_current_algorithm().get_name();

      if (alg != NOPRINT)
	alg = "En algoritmo: '"+alg+"'";
      else
	alg = "";
    
      if (tostr != "")
	tostr = "'"+tostr+"'";

      if (lno > -1)
	lineno = "linea("+to_string(lno)+"), ";
    }
    else{
      alg = "";
      tostr = "";
      lineno = "";
    }
    string header = "ERROR ( ";
    if (inf)
      header += "Durante la inferencia de tipos ): ";
    else
      header += lineno + alg + tostr + " ): ";
    return header;
  }
} // namespace LEAC
