#include "../include/managers/conversionmanager.h"

#include <iostream>
#include <string>
#include <vector>
#include "../include/defstrings.h"
#include "../include/algorithm.h"
#include "../include/type.h"
#include "../include/managers/errormanager.h"

using namespace std;

namespace LEAC{

  bool ConversionManager::conversionTableType[MAX_ROWS_TYPE][MAX_COLS_TYPE] =
    //nul,log,int,flo,cha,str,eset,set,arr,any,bas,ari
    {{1  ,1  ,1  ,1  ,1  ,1  ,1   ,1  ,1  ,1  ,1  ,1  }, // nulo
     {0  ,1  ,0  ,0  ,0  ,0  ,0   ,1  ,0  ,0  ,0  ,0  }, // logico
     {0  ,0  ,1  ,1  ,0  ,0  ,0   ,1  ,0  ,0  ,0  ,0  }, // entero
     {0  ,0  ,0  ,1  ,0  ,0  ,0   ,1  ,0  ,0  ,0  ,0  }, // real
     {0  ,0  ,0  ,0  ,1  ,1  ,0   ,1  ,0  ,0  ,0  ,0  }, // caracter
     {0  ,0  ,0  ,0  ,0  ,1  ,0   ,1  ,0  ,0  ,0  ,0  }, // cadena
     {0  ,0  ,0  ,0  ,0  ,0  ,1   ,1  ,0  ,0  ,0  ,0  }, // conjunto vacio
     {0  ,0  ,0  ,0  ,0  ,0  ,0   ,1  ,0  ,0  ,0  ,0  }, // conjunto
     {0  ,0  ,0  ,0  ,0  ,0  ,0   ,0  ,1  ,0  ,0  ,0  }, // array
     {0  ,1  ,1  ,1  ,1  ,1  ,1   ,1  ,1  ,0  ,0  ,0  }, // cualquiera (generico)
     {0  ,1  ,1  ,1  ,1  ,1  ,0   ,0  ,0  ,0  ,0  ,0  }, // basico (generico)
     {0  ,0  ,1  ,1  ,0  ,0  ,0   ,0  ,0  ,0  ,0  ,0  }} // aritmetico (generico)
    ;

  bool ConversionManager::conversionTableExpr[MAX_ROWS_EXPR][MAX_COLS_EXPR] =
    //==,!=,&&,||,in,lt,le,gt,ge,c ,sc,ic,cs,u ,i ,+ ,- ,/ ,* ,% ,dv
    {{0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0}, // nulo
     {1 ,1 ,1 ,1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0}, // logico
     {1 ,1 ,0 ,0 ,0 ,1 ,1 ,1 ,1 ,0 ,0 ,0 ,0 ,0 ,0 ,1 ,1 ,1 ,1 ,1 ,1}, // entero
     {1 ,1 ,0 ,0 ,0 ,1 ,1 ,1 ,1 ,0 ,0 ,0 ,0 ,0 ,0 ,1 ,1 ,1 ,1 ,1 ,1}, // real
     {1 ,1 ,0 ,0 ,0 ,1 ,1 ,1 ,1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0}, // caracter
     {1 ,1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 ,0 ,0 ,0 ,0 ,0}, // cadena
     {1 ,1 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,1 ,1 ,0 ,0 ,0 ,0 ,0 ,0}, // conjunto vacio
     {1 ,1 ,0 ,0 ,1 ,0 ,0 ,0 ,0 ,1 ,1 ,1 ,1 ,1 ,1 ,0 ,1 ,0 ,0 ,0 ,0}, // conjunto
     {0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0}, // array
     {0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0}, // cualquiera (generico)
     {0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0}, // basico (generico)
     {0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0 ,0}} // aritmetico (generico)
    ;

  bool ConversionManager::is_legal(const Types& types1, const Types& types2){
#ifdef CONVERSIONMANAGER_DEBUG
    cout << "#" << __func__ << ": " << types1 << " --> " << types2 << endl;
#endif
    int types1_size = types1.size(), types2_size = types2.size();
    if (types1_size == types2_size){
      for (int i=0; i<types1_size; ++i){
	if (!is_legal(types1[i],types2[i]))
	  return false;
      }
      return true;
    }
    else
      return false;
  }

  bool ConversionManager::is_legal(const Type& type1, const Type& type2){
    if (type1.is_null() && type2.is_null())
      return true;
    int index1 = type1.get_index(), index2 = type2.get_index();
    bool typeCheck = conversionTableType[index1][index2];
    if (type1.is_generic() || type2.is_generic()){
      return typeCheck;
    }
  
    bool dimensionCheck = true;
    if (!type1.is_null() && !type2.is_null()){
      dimensionCheck = type1.get_dimension() == type2.get_dimension();
    }

#ifdef CONVERSIONMANAGER_DEBUG
    cout << "#" << __func__ << ": " << type1 << " --> " << type2 << endl;
#endif
    return typeCheck && dimensionCheck && is_legal(type1.get_sub_type(),type2.get_sub_type());
  }

  bool ConversionManager::is_legal_unary(const string& op, const Type& type){
#ifdef CONVERSIONMANAGER_WARNINGS
    if (type.is_null())
      cout << "Uso incorrecto de IS_LEGAL_UNARY()" << endl;
#endif
#ifdef CONVERSIONMANAGER_DEBUG
    cout << "#" << __func__ << ": " << op << " " << type << endl;
#endif

    if (op == OPER_NOT)
      return type.is_ready() && type.is_logic();
    else
      return (type.is_ready()) && (type.is_integer() || type.is_float());
  }

  bool ConversionManager::is_legal_swap(const Type& type1, const Type& type2){
    return is_legal(type1,type2) || is_legal(type2,type1);
  }

  bool ConversionManager::is_legal_expr(const Type& type1, const Type& type2,
					const string& op){
#ifdef CONVERSIONMANAGER_DEBUG
    cout << "#" << __func__ << ": " << type1 << " " << op << " " << type2 << endl;
#endif
    int index1 = type1.get_index(), index2 = type2.get_index();
    int indexOp = get_index_op(op);

    bool exprCheck = conversionTableExpr[index1][indexOp] && conversionTableExpr[index2][indexOp];
  
    return exprCheck && (is_legal(type1,type2) || is_legal(type2,type1));
  }

  bool ConversionManager::is_legal_in(const Type& type1, const Type& type2){
#ifdef CONVERSIONMANAGER_DEBUG
    cout << "#" << __func__ << ": " << type1 << " en " << type2 << endl;
#endif
    int index2 = type2.get_index();
    int indexOp = get_index_op("en");
    Type subType = type2.get_sub_type();
  
    bool exprCheck = conversionTableExpr[index2][indexOp];
    return exprCheck && is_legal(type1,subType);
  }

  Type ConversionManager::solve(const Type& type1, const Type& type2){
#ifdef CONVERSIONMANAGER_DEBUG
    cout << "#" << __func__ << ": " << type1 << " y " << type2 << endl;
#endif
    Type res;
    return solve_rec(res,type1,type2);
  }

  Type ConversionManager::solve_rec(Type& res, const Type& type1, const Type& type2){
    if (type1.get_index() > type2.get_index())
      res = type1;
    else
      res = type2;

    const Type& subType1 = type1.get_sub_type();
    const Type& subType2 = type2.get_sub_type();
    if (!subType1.is_null() && !subType2.is_null()){
      Type aux = res.get_sub_type();
      res.set_sub_type(solve_rec(aux,subType1,subType2));
    }
    return res;
  }

  int ConversionManager::get_index_op(const string& op){
    if (op == OPER_EQ)
      return 0;
    else if (op == OPER_NEQ)
      return 1;
    else if (op == OPER_CONJ)
      return 2;
    else if (op == OPER_DISY)
      return 3;
    else if (op == OPER_IN)
      return 4;
    else if (op == OPER_LT)
      return 5;
    else if (op == OPER_LE)
      return 6;
    else if (op == OPER_GT)
      return 7;
    else if (op == OPER_GE)
      return 8;
    else if (op == OPER_C)
      return 9;
    else if (op == OPER_SC)
      return 10;
    else if (op == OPER_CI)
      return 11;
    else if (op == OPER_SCI)
      return 12;
    else if (op == OPER_UNION)
      return 13;
    else if (op == OPER_INTERSECTION)
      return 14;
    else if (op == OPER_PLUS)
      return 15;
    else if (op == OPER_MINUS)
      return 16;
    else if (op == OPER_SLASH)
      return 17;
    else if (op == OPER_STAR)
      return 18;
    else if (op == OPER_MOD)
      return 19;
    else if (op == OPER_DIV)
      return 20;
    else
      return -1;
  }
} // namespace LEAC
