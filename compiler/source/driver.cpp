#include <fstream>
#include <sstream>

#include "../include/driver.h"
#include "../scanner/scanner.h"
#include "../include/algorithms.h"
#include "../include/managers/errormanager.h"
#include "../include/managers/dependenciesmanager.h"
#include "../include/managers/xmloutputmanager.h"
#include "../include/nodes/node.h"

using std::ofstream;
using std::stringstream;

namespace LEAC {

  Driver::Driver(class LEACContext& _leac)
    : trace_scanning(false),
      trace_parsing(false),
      leac(_leac)
  {
  }

  bool Driver::parse_stream(std::istream& in, const std::string& sname)
  {
    streamname = sname;

    Scanner scanner(&in);
    scanner.set_debug(trace_scanning);
    this->lexer = &scanner;

    Parser parser(*this);
    parser.set_debug_level(trace_parsing);
    return (parser.parse() == 0);
  }

  bool Driver::parse_file(const std::string &filename)
  {
    std::ifstream in(filename.c_str());
    if (!in.good()) return false;
    return parse_stream(in, filename);
  }

  bool Driver::parse_string(const std::string &input, const std::string& sname)
  {
    std::istringstream iss(input);
    return parse_stream(iss, sname);
  }

  void Driver::error(const class location& l,
		     const std::string& m)
  {
    std::cerr << l << ": " << m << std::endl;
  }

  void Driver::error(const std::string& m)
  {
    std::cerr << m << std::endl;
  }
  
  void Driver::sem(Node* entry){
    Algorithms::load_std_algorithms();
    entry->step_1();
    entry->step_2();
  
    Algorithms::solve_all_variables_dependencies();
  
    entry->step_4();
  
    if (!Algorithms::is_defined_main_algorithm())
      ErrorManager::main_doesnt_exists();
  
    if (ErrorManager::check_error())
      cout << "No ha generado XML: existen errores al compilar" << endl;
    else{
      stringstream stream;
      XMLOutputManager::open_tag(stream,XMLTAG_ALGS);
      entry->to_XML(stream);
      XMLOutputManager::close_tag(stream);
      if (coutFlag)
	cout << stream.str();
      ofstream outfile;
      outfile.open(outputName);
      outfile << stream.str();
      outfile.close();
    }
  
    Algorithms::free_algorithms();
    DependenciesManager::free_function_calls();
  }
  
} // namespace LEAC
