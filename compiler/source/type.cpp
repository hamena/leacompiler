#include "../include/type.h"

#include <iostream>
#include <sstream>
#include <string>
#include "../include/managers/xmloutputmanager.h"
#include "../include/defstrings.h"

using std::cout;
using std::endl;
using std::to_string;
using std::stringstream;

namespace LEAC{

  string Type::sNULL= TYPE_NULL;
  string Type::sLOGIC      = TYPE_LOGIC;
  string Type::sINTEGER    = TYPE_INTEGER;
  string Type::sFLOAT      = TYPE_FLOAT;
  string Type::sCHARACTER  = TYPE_CHARACTER;
  string Type::sSTRING     = TYPE_STRING;
  string Type::sEMPTY_SET  = TYPE_EMPTY_SET;
  string Type::sSET        = TYPE_SET;
  string Type::sARRAY      = TYPE_ARRAY;
  string Type::sANY        = TYPE_ANY;
  string Type::sBASIC      = TYPE_BASIC;
  string Type::sARITHMETIC = TYPE_ARITHMETIC;
  string Type::sUNDEF      = TYPE_UNDEFINED;

  Type::Type() :
    type_name(sNULL), type(iNULL), dimension(-1), sub_type(nullptr), flag_putoff(false),
    flag_cutway(false), flag_tmpexp(false), flag_ready(false)
  {}

  Type::Type(const string& name, int t, int d, const Type& subt) :
    type_name(name), type(t), dimension(d), sub_type(nullptr), flag_putoff(false),
    flag_cutway(false), flag_tmpexp(false), flag_ready(true)
  {
    if (t == iSET || t == iARRAY)
      sub_type = new Type(subt);
  
#ifdef TYPE_WARNING
    if ((t == iSET || t == iARRAY) && subt.is_null())
      cout << "Array o conjunto sin sub-tipo. TYPE()" << endl;
    if ((t != iSET && t != iARRAY) && !subt.is_null()){
      cout << "Tipo simple con sub-tipo. TYPE()" << endl;
    }
#endif //TYPE_WARNINGS
  }

  Type::Type(const Type& t) : sub_type(nullptr){
    this->type_name = t.type_name;
    this->type = t.type;
    this->dimension = t.dimension;
    this->flag_tmpexp = t.flag_tmpexp;
    this->flag_ready = t.flag_ready;
    this->flag_cutway = t.flag_cutway;
    this->flag_putoff = t.flag_putoff;
    if (t.sub_type){
      sub_type = new Type(*t.sub_type);
    }
  }

  bool Type::is_generic() const{
    return type == iANY || type == iBASIC || type == iARITHMETIC;
  }

  bool Type::is_null() const{
    return type == iNULL;
  }

  bool Type::is_logic() const{
    return type == iLOGIC;
  }

  bool Type::is_integer() const{
    return type == iINTEGER;
  }

  bool Type::is_float() const{
    return type == iFLOAT;
  }

  bool Type::is_character() const{
    return type == iCHARACTER;
  }

  bool Type::is_string() const{
    return type == iSTRING;
  }

  bool Type::is_empty_set() const{
    return type == iEMPTY_SET;
  }

  bool Type::is_set() const{
    return type == iSET;
  }

  bool Type::is_array() const{
    return type == iARRAY;
  }

  bool Type::is_undef() const{
    return type == iUNDEF;
  }

  bool Type::is_putoff() const{
    return flag_putoff;
  }

  bool Type::is_tmpexp() const{
    return flag_tmpexp;
  }

  bool Type::is_cutway() const{
    return flag_cutway;
  }

  bool Type::is_ready() const{
    return flag_ready;
  }

  string Type::get_type_name() const{
    return type_name;
  }

  int Type::get_index() const{
    return type;
  }

  int Type::get_dimension() const{
    return dimension;
  }

  Type Type::get_sub_type() const{
    if (sub_type)
      return *sub_type;
    else
      return make_null();
  }

  void Type::to_XML(stringstream& os) const{
  
    bool close = true;
    if (sub_type)
      close = false;

    XMLOutputManager::AttributesNames  names  = {ATTR_NAME,ATTR_DIM};
    XMLOutputManager::AttributesValues values = {type_name,to_string(dimension)};
    XMLOutputManager::open_tag(os,XMLTAG_TYPE,names,values,close);
  
    if (sub_type){
      sub_type->to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  }

  void Type::set_dimension(int d){
    dimension = d;
  }

  void Type::set_sub_type(const Type& subt){
#ifdef TYPE_WARNINGS
    if (type != iSET && type != iARRAY){
      cout << "Subtipo a tipo simple. SET_SUB_TYPE" << endl;
    }
    if (subt.is_null())
      cout << "Subtipo nulo. SET_SUB_TYPE" << endl;
#endif
    if (sub_type) delete sub_type;
    sub_type = new Type(subt);
  }

  void Type::set_ready(){
    flag_ready = true;
    flag_putoff = false;
    flag_tmpexp = false;
    flag_cutway = false;
  }

  void Type::set_putoff(){
    flag_putoff = true;
    flag_ready = false;
    flag_tmpexp = false;
    flag_cutway = false;
  }

  void Type::set_cutway(){
    flag_cutway = true;
    flag_ready = false;
    flag_tmpexp = false;
    flag_putoff = false;
  }

  void Type::set_tmpexp(){
    flag_tmpexp = true;
    flag_ready = false;
    flag_cutway = false;
    flag_putoff = false;
  }

  Type& Type::operator =(const Type& t){
    this->type_name = t.type_name;
    this->type = t.type;
    this->dimension = t.dimension;
    this->flag_tmpexp = t.flag_tmpexp;
    this->flag_ready = t.flag_ready;
    this->flag_cutway = t.flag_cutway;
    this->flag_putoff = t.flag_putoff;
  
    if (this->sub_type)
      delete this->sub_type;
    this->sub_type = nullptr;
    if (t.sub_type)
      this->sub_type = new Type(*t.sub_type);
  
    return *this;
  }

  Type Type::make_any(){
    return Type(sANY,iANY,0);
  }

  Type Type::make_basic(){
    return Type(sBASIC,iBASIC,0);
  }

  Type Type::make_arithmetic(){
    return Type(sARITHMETIC,iARITHMETIC,0);
  }

  Type Type::make_logic(){
    return Type(sLOGIC,iLOGIC,0);
  }

  Type Type::make_integer(){
    return Type(sINTEGER,iINTEGER,0);
  }

  Type Type::make_float(){
    return Type(sFLOAT,iFLOAT,0);  
  }

  Type Type::make_character(){
    return Type(sCHARACTER,iCHARACTER,0);
  }

  Type Type::make_string(){
    return Type(sSTRING,iSTRING,0);
  }

  Type Type::make_empty_set(){
    return Type(sEMPTY_SET,iEMPTY_SET,0);
  }

  Type Type::make_set(const Type& subt){
    return Type(sSET,iSET,0,subt);
  }

  Type Type::make_array(const Type& subt, int dim){
    return Type(sARRAY,iARRAY,dim,subt);
  }

  Type Type::make_undef(){
    return Type(sUNDEF,iUNDEF,0);
  }

  Type Type::make_null_putoff(){
    Type t;
    t.set_putoff();
    return t;
  }

  Type* Type::make_new_logic(){
    return new Type(sLOGIC,iLOGIC,0);
  }

  Type* Type::make_new_integer(){
    return new Type(sINTEGER,iINTEGER,0);
  }

  Type* Type::make_new_float(){
    return new Type(sFLOAT,iFLOAT,0);
  }

  Type* Type::make_new_character(){
    return new Type(sCHARACTER,iCHARACTER,0);
  }

  Type* Type::make_new_string(){
    return new Type(sSTRING,iSTRING,0);
  }

  Type Type::make_null(){
    return Type();
  }

  Type::~Type(){
    if (sub_type)
      delete sub_type;
    sub_type = nullptr;
  }

  ostream& operator <<(ostream& os, const Type& t){
    if (t.is_null())
      return os;
  
    os << "<" << t.get_type_name();
    if (t.get_dimension() > 0)
      os << "[" << t.get_dimension() << "]";
    if (!t.get_sub_type().is_null())
      os << t.get_sub_type();
    os << ">";
    return os;
  }

  ostream& operator <<(ostream& os, const Types& v){
    if (!v.empty()){
      os << v[0];
      for (unsigned int i=1; i<v.size(); ++i){
	os << ", " << v[i];
      }
    }
    return os;
  }
} // namespace LEAC
