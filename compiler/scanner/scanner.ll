%{ /* C++/C */


#ifndef _GLIBCXX_USE_CXX11_ABI
#define _GLIBCXX_USE_CXX11_ABI 0
#endif

#include <iostream>
#include <stack>
#include <string>
    
#include "scanner.h"
    
using namespace std;

/* import the parser's token type into a local typedef */
typedef LEAC::Parser::token token;
typedef LEAC::Parser::token_type token_type;

/* By default yylex returns int, we use token_type. Unfortunately yyterminate
 * by default returns 0, which is not of token_type. */
#define yyterminate() return token::END

/* This disables inclusion of unistd.h, which is not available under Visual C++
 * on Win32. The C++ scanner uses STL streams instead. */
#define YY_NO_UNISTD_H

/* stack to handle indentation */
stack<int> indent;
bool multiple_dedent_flag = false;

//#define SCANNER_DEBUG 1

%}

/* enable c++ scanner class generation */
%option c++

/* change the name of the scanner class. results in "ExampleFlexLexer" */
%option prefix="LEAC"
     
/* enable scanner to generate debug output. disable this for release
 * versions. */
%option debug

/* The following paragraph suffices to track locations accurately. Each time
 * yylex is invoked, the begin position is moved onto the end position. */
%{
#define YY_USER_ACTION  yylloc->columns(yyleng); 
%}

D [0-9]
L [a-zA-Z_]
H [a-zA-Z0-9_]

INLINE_COMMENT "//"[^\n]*

%x INDENTATION
%x COMMENT
%option noyywrap
%option yylineno

%%

 /* code to place at the beginning of yylex() */
%{
    // reset location
    yylloc->step();
%}

<COMMENT>{
   \n		{ yylloc->lines(1); yylloc->step(); }
  ([^*\n])+ 	;
  "*"+[^*/\n]*  ;
   "*/" { BEGIN(INITIAL); }
}

<INDENTATION>{
  [ \t]*\n {
    	     yylloc->lines(1);
	     yylloc->step();
#ifdef SCANNER_DEBUG
	     cout << "REGLA '[ \\t]*\\n' --> Filtrado: [" << yytext << "]" << endl;
#endif
    	   }

  [ \t]*"/*" {
#ifdef SCANNER_DEBUG
             cout << "REGLA '[ \\t]*\"/*\"' --> Comentario /*, yyless y begin(initial)" << endl;
#endif
	     yyless(0); BEGIN(INITIAL);
	     }
  [ \t]*{INLINE_COMMENT} { 
#ifdef SCANNER_DEBUG
	     cout << "REGLA '[ \\t]*{INLINE_COMMENT}' --> Filtrado comentarion inline [" 
	     	  << yytext << "]" << endl;
#endif
    			 }

  [^ \t/]*  {
     	     if (indent.empty())
	       indent.push(0);

#ifdef SCANNER_DEBUG
	     cout << "REGLA '[^ \\t/]*' --> [" << yytext << "]: " << yyleng << " - "
		  << indent.top() << " Resolucion->";
#endif

	     if (indent.top() > 0){
	       //yymore();
	       yyless(0);
	       indent.pop();

#ifdef SCANNER_DEBUG
	       cout << "DEDENT, begin(initial)" << endl;
#endif
	       if (indent.top() == 0)
	       	  BEGIN(INITIAL);

	       return token::DEDENT;
	     }
	     else{
	       yyless(0);

#ifdef SCANNER_DEBUG
	       cout << "begin(initial)" << endl;
#endif

	       BEGIN(INITIAL);
	     }
	   }

  [ \t]+[^ \t\n] {
    	   	   if (indent.empty())
      		     indent.push(0);

#ifdef SCANNER_DEBUG
		   cout << "REGLA '[ \\t]+[^ \\t\\n]' --> [" << yytext << "]: " << yyleng
		   << " - " << indent.top() << " Resolucion->";
#endif

		   if (indent.top() < yyleng){ // INDENT
      		     indent.push(yyleng);
		     yyless(0);

#ifdef SCANNER_DEBUG
      		     cout << "INDENT" << endl;
#endif

		     BEGIN(INITIAL);
      		     return token::INDENT;
    		   } else if (indent.top() > yyleng || multiple_dedent_flag){ // DEDENT
		     multiple_dedent_flag = false;    
      		     indent.pop();

		     if (indent.top() > yyleng){ // multiple DEDENT
		       multiple_dedent_flag = true;
		       yymore();
      		     }

		     yyless(0);	

#ifdef SCANNER_DEBUG
      		     cout << "DEDENT" << endl;
#endif

		     if (!multiple_dedent_flag)
		       BEGIN(INITIAL);	
      		     return token::DEDENT;

		   } else {
		     yyless(0);
		     BEGIN(INITIAL);
		   }
		 }
}

<INITIAL>{
  
  "/*" { BEGIN(COMMENT);}
  {INLINE_COMMENT} ;
  
  "logico" {/*cout<<"TBOOL,";*/ return token::TBOOL;}
  "entero" {/*cout<<"TINTEGER,";*/ return token::TINTEGER;}
  "real" {/*cout<<"TREAL,";*/ return token::TREAL;}
  "caracter" {return token::TCHARACTER;}
  "cadena" {/*cout<<"TSTRING,";*/ return token::TSTRING;}
  "mientras" {/*cout<<"WHILE,";*/ return token::WHILE;}
  "repite" {/*cout<<"REPEAT,";*/ return token::REPEAT;}
  "hasta" {/*cout<<"UNTIL,";*/ return token::UNTIL;}
  "desde" {/*cout<<"FROM,";*/ return token::FROM;}
  "con incremento" {/*cout<<"INCREMENT,";*/ return token::INCREMENT;}
  "con decremento" {/*cout<<"DECREMENT,";*/ return token::DECREMENT;}
  "con" {/*cout<<"WITH,";*/ return token::WITH;}
  "para todo" {/*cout<<"FOR_ALL,";*/ return token::FOR_ALL;}
  "si no" {/*cout<<"ELSE,";*/ return token::ELSE;}
  "si" {/*cout<<"IF,";*/ return token::IF;}
  "vacio" {/*cout<<"EMPTY_SET,";*/ return token::EMPTY_SET;}
  "union" {/*cout<<"UNION,";*/ return token::UNION;}
  "interseccion" {/*cout<<"INTERSECTION,";*/ return token::INTERSECTION;}
  "contenido" {/*cout<<"CONTAINED_IN,";*/ return token::CONTAINED_IN;}
  "contenido estricto" {/*cout<<"SCONTAINED_IN,";*/ return token::SCONTAINED_IN;}
  "contiene" {/*cout<<"CONTAINS,";*/ return token::CONTAINS;}
  "contiene estricto" {/*cout<<"SCONTAINS,";*/ return token::SCONTAINS;}
  "en" {/*cout<<"IN,";*/ return token::IN;}
  "verdadero" { /*cout<<"LTRUE,";*/ return token::LTRUE;}
  "falso" {/*cout<<"LFALSE,";*/ return token::LFALSE;}

  \.\. { return token::RANGE; }
  "<--" {/*cout<<"LEFT_ARROW,";*/ return token::LEFT_ARROW;}
  "-->" {/*cout<<"RIGHT_ARROW,";*/ return token::RIGHT_ARROW;}
  "<->" {/*cout<<"DOUBLE_ARROW,";*/ return token::DOUBLE_ARROW;}
  "&&" {/*cout<<"AND,";*/ return token::AND;}
  "||" {/*cout<<"OR,";*/ return token::OR;}
  "~" {/*cout<<"NOT,";*/ return token::NOT;}
  "<" {/*cout<<"LESSER,";*/ return token::LESSER;}
  ">" {/*cout<<"GREATER,";*/ return token::GREATER;}
  "<=" {/*cout<<"LESSER_EQUALS,";*/ return token::LESSER_EQUALS;}
  ">=" {/*cout<<"GREATER_EQUALS,";*/ return token::GREATER_EQUALS;}
  "==" {/*cout<<"EQUALS,";*/ return token::EQUALS;}
  "~=" {/*cout<<"NOT_EQUALS,";*/ return token::NOT_EQUALS;}
  "div" {/*cout<<"DIV,";*/ return token::DIV;}
  "," {/*cout<<"comma,";*/ return token::COMMA;}
  ":" {/*cout<<":,";*/ return token::COLON;}
  \. {/*cout<<".,";*/ return token::DOT;}
  "+" {/*cout<<"+,";*/ return token::PLUS;}
  "-" {/*cout<<"-,";*/ return token::MINUS;}
  "*" {/*cout<<"*,";*/ return token::STAR;}
  "/" {/*cout<<"/,";*/ return token::SLASH;}
  "%" {/*cout<<"%,";*/ return token::MOD;}
  "{" {/*cout<<"{,";*/ return token::LBRACER;}
  "}" {/*cout<<"},";*/ return token::RBRACER;}
  "(" {/*cout<<"(,";*/ return token::LP;}
  ")" {/*cout<<"),";*/ return token::RP;}
  "[" {/*cout<<"[,";*/ return token::LBRACKET;}
  "]" {/*cout<<"],";*/ return token::RBRACKET;}
  
  {D}+ {/*cout<<"LINTEGER,";*/ yylval->ival = atoi(yytext); return token::LINTEGER; }
  {D}+\.{D}+ {/*cout<<"LREAL,";*/ yylval->fval = atof(yytext); return token::LREAL; }
  \'[^\']\' {
  	    char aux[2];
	    strncpy(aux,yytext+1,1);
  	    yylval->cval = aux[0]; return token::LCHARACTER;
	    }
  \"[^\"]*\" {
  	     int offset = strlen(yytext)-2;
	     char aux[256];
	     strncpy(aux,yytext+1,offset);
	     aux[offset] = '\0';
  	     yylval->sval = new string(aux); return token::LSTRING;
	     }
  {L}+{H}* {/*cout<<"ID,";*/ yylval->sval = new string(yytext); return token::ID; }
  
  \n { yylloc->lines(1); yylloc->step(); BEGIN(INDENTATION); }
  [ \t\r]+ ;
  
  . {cerr << "WARNING: Filtrado caracter deconocido: '" << yytext << "'" << endl;}
}

<INITIAL,COMMENT,INDENTATION><<EOF>> {
#ifdef SCANNER_DEBUG
    cout << "EOF encontrado --> ";
#endif
    if (indent.empty())
      indent.push(0);
    
    if (indent.top() > 0){
    
      indent.pop();
      
#ifdef SCANNER_DEBUG
      cout << "DEDENT" << endl;
#endif

      return token::DEDENT;
    }

#ifdef SCANNER_DEBUG
    cout << "yyterminate()" << endl;
#endif

    yyterminate();
  }

%%


namespace LEAC {

Scanner::Scanner(std::istream* in,
		 std::ostream* out)
    : LEACFlexLexer(in, out)
{
}

Scanner::~Scanner()
{
}

void Scanner::set_debug(bool b)
{
    yy_flex_debug = b;
}

}

/* This implementation of ExampleFlexLexer::yylex() is required to fill the
 * vtable of the class ExampleFlexLexer. We define the scanner's main yylex
 * function via YY_DECL to reside in the Scanner class instead. */

#ifdef yylex
#undef yylex
#endif

int LEACFlexLexer::yylex()
{
    std::cerr << "in ExampleFlexLexer::yylex() !" << std::endl;
    return 0;
}
