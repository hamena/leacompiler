#ifndef TYPE_H_
#define TYPE_H_

#include <string>
#include <sstream>
#include <iostream>
#include <vector>

#define TYPE_WARNINGS 1

using std::string;
using std::stringstream;
using std::vector;
using std::ostream;

namespace LEAC{

  /** Clase tipo. Abstrae el concepto de tipo de un símbolo, guardando así su estado y tipo 
   * final. */
  class Type{
  public:
    /** Constructor predeterminado. Tipo nulo. */
    Type();
    /** Constructor de copia. */
    Type(const Type&);

    /** Crea un nuevo tipo cualquiera. */
    static Type make_any();
    /** Crea un nuevo tipo básico. */
    static Type make_basic();
    /** Crea un nuevo tipo aritmético. */
    static Type make_arithmetic();
  
    /** Crea un nuevo tipo lógico. */
    static Type make_logic();
    /** Crea un nuevo tipo entero. */
    static Type make_integer();
    /** Crea un nuevo tipo real. */
    static Type make_float();
    /** Crea un nuevo tipo caracter. */
    static Type make_character();
    /** Crea un nuevo tipo cadena. */
    static Type make_string();
    /** Crea un nuevo tipo conjunto vacio. */
    static Type make_empty_set();
    /** Crea un nuevo tipo conjunto. */
    static Type make_set(const Type&);
    /** Crea un nuevo tipo array. */
    static Type make_array(const Type&, int dim=0);
    /** Crea un nuevo tipo indefinido. */
    static Type make_undef();
    /** Crea un nuevo tipo 'dejado' */
    static Type make_null_putoff();
    /** Crea un nuevo tipo lógico. */
    static Type* make_new_logic();
    /** Crea un nuevo tipo entero. */
    static Type* make_new_integer();
    /** Crea un nuevo tipo real. */
    static Type* make_new_float();
    /** Crea un nuevo tipo caracter. */
    static Type* make_new_character();
    /** Crea un nuevo tipo cadena. */
    static Type* make_new_string();

    /** Comprueba si el tipo es genérico. */
    bool is_generic() const;
    /** Comprueba si el tipo es nulo. */
    bool is_null() const;
    /** Comprueba si el tipo es lógico. */
    bool is_logic() const;
    /** Comprueba si el tipo es entero. */
    bool is_integer() const;
    /** Comprueba si el tipo es real. */
    bool is_float() const;
    /** Comprueba si el tipo es caracter. */
    bool is_character() const;
    /** Comprueba si el tipo es cadena. */
    bool is_string() const;
    /** Comprueba si el tipo es conjunto vacío. */
    bool is_empty_set() const;
    /** Comprueba si el tipo es conjunto. */
    bool is_set() const;
    /** Comprueba si el tipo es array. */
    bool is_array() const;
    /** Comprueba si el tipo es indefinido. */
    bool is_undef() const;
    /** Comprueba si el tipo es 'dejado'. */
    bool is_putoff() const;
    /** Comprueba si el tipo es de expresión temporal. */
    bool is_tmpexp() const;
    /** Comprueba si el tipo indica un camino cerrado. */
    bool is_cutway() const;
    /** Comprueba si el tipo está listo. */
    bool is_ready() const;
    /** Obtienes el nombre del tipo. */
    string get_type_name() const;
    /** Obtienes el índice para las tablas de conversión. */
    int get_index() const;
    /** Obtienes la dimensión del tipo. */
    int get_dimension() const;
    /** Obtienes el subtipo. */
    Type get_sub_type() const;

    /** Pinta en un bufer la traducción a XML del tipo. */
    void to_XML(stringstream&) const;

    /** Guarda la dimension del tipo. */
    void set_dimension(int);
    /** Guarda el tipo como subtipo. */
    void set_sub_type(const Type&);
    /** Guarda el estado listo.*/
    void set_ready();
    /** Guarda el estado 'dejado' */
    void set_putoff();
    /** Guarda el estado de camino cortado. */
    void set_cutway();
    /** Guarda el estado de expresión temporal. */
    void set_tmpexp();

    /** Sobrecarga del operador igual para tipos. */
    Type& operator =(const Type&);

    /** Destructor del tipo. */
    ~Type();
  private:
    /** Constructor privado auxiliar. */
    Type(const string&, int, int, const Type& subt = Type());
  
    string type_name; ///< Nombre del tipo.
    int type;         ///< Índice en las tablas de conversión.
    int dimension;    ///< Dimensión del tipo.
    Type* sub_type;   ///< Tipo subordinado.
    //@{
    /** Banderas de estados. */
    bool flag_putoff, flag_cutway, flag_tmpexp, flag_ready;
    //@}

    /** Enum que sirve de índice para los tipos en las tablas de conversión. */
    enum TypeInt:int{iUNDEF=-1,iNULL,iLOGIC,iINTEGER,iFLOAT,
	iCHARACTER,iSTRING,iEMPTY_SET,iSET,iARRAY,iANY,iBASIC,iARITHMETIC};
    /** Nombres estáticos de los tipos. */
    static string sNULL,sLOGIC,sINTEGER,sFLOAT,sCHARACTER,
      sSTRING,sEMPTY_SET,sSET,sARRAY,sANY,sBASIC,sARITHMETIC,sUNDEF;

    /** Crea un nuevo tipo nulo. */
    static Type make_null();

  };

  /** Definición de lista de tipos. */
  typedef vector<Type> Types;

  /** Sobrecarga del operador de inserción para tipo. */
  ostream& operator <<(ostream&, const Type&);
  /** Sobrecarga del operador de inserción para listas de tipos. */
  ostream& operator <<(ostream&, const vector<Type>&);

} // namespace LEAC

#endif // TYPE_H_
