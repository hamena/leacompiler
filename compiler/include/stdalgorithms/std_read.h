#ifndef STD_READ_H_
#define STD_READ_H_

#include "stdalgorithm.h"
#include "../defstrings.h"
#include "../type.h"
#include "../managers/dependenciesmanager.h"
#include "../managers/errormanager.h"
#include "../managers/conversionmanager.h"

namespace LEAC{

/** Clase derivada que representa el algoritmo estándar de lectura. */
class StdRead : public StdAlgorithm{
public:
  /** Constructor */
  StdRead() : StdAlgorithm(STDALG_READ)
  {
    new_input_argument(generate_aux_name());
  }

  /** Resuelve las dependencias de las variables del algoritmo. */
  void solve_dependencies(){
    SymbolsIterator var = inputArgs.begin();
    DependenciesManager::solve_variable_dependencies(var);
  }

  /** Comprueba la semántica del uso del algoritmo. */
  bool check_semantic() const{
    return ConversionManager::is_legal(Type::make_basic(),inputArgs.begin()->second.type);
  }

  /** Lanza errores. */   
  void launch_error(const string& tostr) const{
    Types inputTypes(1,Type::make_basic());
    ErrorManager::std_algorithm_illegal(tostr,STDALG_READ,get_input_types(), inputTypes);
  }
};

} // namespace LEAC
  
#endif // STD_READ_H_
