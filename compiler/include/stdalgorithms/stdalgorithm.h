#ifndef STDALGORITHM_H_
#define STDALGORITHM_H_

#include "../algorithmbase.h"
#include <string>

#define STDALG_WARNINGS 1
//#define STDALG_DEBUG 1

namespace LEAC{

  /** Clase base de algoritmos estándar.
   * Clase derivada de la clase base pero que a su vez hace de clase base abstracta 
   * para los algoritmos estándar de LEA.
   */
  class StdAlgorithm : public AlgorithmBase{
  public:
    /** Constructor */
    StdAlgorithm(const string& name): AlgorithmBase(name) {}
    /** Comprueba si el algoritmo es estándar. */
    bool is_std() const { return true; }
    /** Comprueba si existe una variable en la tabla de símbolos. */
    bool check_variable(const string&);
    /** Obtienes una variable indicada. */
    const Variable& get_variable(const string&);
    /** Obtienes la referencia de una variable indicada. */
    SymbolsIterator get_variable_reference(const string&);
    /** Otorga un tipo a una variable de la tabla de símbolos. */
    void set_type(const string&, const Type&);

    /** Resuelve las dependencias de los argumentos de salida. */
    virtual void solve_output_dependencies(){}
    /** Destructor virtual */
    virtual ~StdAlgorithm(){}

    /** Comprueba la semántica del uso del algoritmo. */
    virtual bool check_semantic() const = 0;
    /** Lanza errores. */
    virtual void launch_error(const string&) const = 0;
  
    // debug
    void print_algorithm();

    /** Creador de funciones estándar. */
    static StdAlgorithm* make_std(const string&);
  
  protected:
    /** Generador de nombres auxiliares. */
    string generate_aux_name() const;
  private:
    static unsigned int N; ///< Contador estático.
  };
} // namespace LEAC
  
#endif //STDALGORITHM_H_
