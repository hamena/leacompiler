#ifndef STD_WRITE_H_
#define STD_WRITE_H_

#include "stdalgorithm.h"
#include "../defstrings.h"
#include "../type.h"
#include "../managers/dependenciesmanager.h"
#include "../managers/errormanager.h"
#include "../managers/conversionmanager.h"

namespace LEAC{

  /** Clase derivada que representa el algoritmo estándar de escritura. */
  class StdWrite : public StdAlgorithm{
  public:
    /** Constructor */
    StdWrite() : StdAlgorithm(STDALG_WRITE)
    {
      new_input_argument(generate_aux_name());
    }

    /** Resuelve las dependencias de las variables del algoritmo. */
    void solve_dependencies(){
      SymbolsIterator var = inputArgs.begin();
      DependenciesManager::solve_variable_dependencies(var);
    }

    /** Comprueba la semántica del uso del algoritmo. */
    bool check_semantic() const{
      return ConversionManager::is_legal(Type::make_any(),inputArgs.begin()->second.type);
    }

    /** Lanza errores. */ 
    void launch_error(const string& tostr) const{
      Types inputTypes(1,Type::make_any());
      ErrorManager::std_algorithm_illegal(tostr,STDALG_WRITE,get_input_types(),inputTypes);
    }
  };

} // namespace LEAC
  
#endif // STD_WRITE_H_
