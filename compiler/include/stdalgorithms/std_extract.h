#ifndef STD_EXTRACT_H_
#define STD_EXTRACT_H_

#include "stdalgorithm.h"
#include "../defstrings.h"
#include "../type.h"
#include "../managers/dependenciesmanager.h"
#include "../managers/errormanager.h"
#include "../managers/conversionmanager.h"

namespace LEAC{

  /** Clase derivada que representa el algoritmo estándar de extración de elementos
   * de un conjunto.
   */
  class StdExtract : public StdAlgorithm{
  public:
    /** Constructor */
    StdExtract() : StdAlgorithm(STDALG_EXTRACT)
    {
      new_input_argument(generate_aux_name());
      new_output_argument(generate_aux_name());
    }

    /** Resuelve las dependencias de las variables del algoritmo. */
    void solve_dependencies(){
      solve_input_dependencies();
      solve_output_dependencies();
    }

    /** Resuelve las dependencias de los argumentos de salida. */
    void solve_output_dependencies(){
      solve_input_dependencies();
      SymbolsIterator input = inputArgs.begin(); 
      SymbolsIterator output = outputArgs.begin();
      if (!input->second.type.is_tmpexp() && output->second.type.is_tmpexp()){
	if (input->second.type.is_set())
	  output->second.type = input->second.type.get_sub_type();
      }
    }

    /** Comprueba la semántica del uso del algoritmo. */
    bool check_semantic() const{
      SymbolsTable::const_iterator input = inputArgs.cbegin();
      bool error = !input->second.type.is_set();
      error = error || !ConversionManager::is_legal(Type::make_any(),input->second.type);
      return !error;
    }

    /** Lanza errores. */ 
    void launch_error(const string& tostr) const{
      Types inputTypes(1,Type::make_set(Type::make_any()));
      Types outputTypes(1,Type::make_any());
      ErrorManager::std_algorithm_illegal(tostr,STDALG_EXTRACT,get_input_types(),inputTypes);
    }  
  private:
    /** Resuelve las dependencias de los argumentos de entrada. */
    void solve_input_dependencies(){
      SymbolsIterator var = inputArgs.begin();
      if (var->second.type.is_tmpexp())
	DependenciesManager::solve_variable_dependencies(var);
    }
  };

} // namespace LEAC
  
#endif
