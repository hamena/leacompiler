#ifndef STD_LENGTH_H_
#define STD_LENGTH_H_

#include "stdalgorithm.h"
#include "../defstrings.h"
#include "../type.h"
#include "../managers/dependenciesmanager.h"
#include "../managers/errormanager.h"

namespace LEAC{

  /** Clase derivada que representa el algoritmo estándar de longitud
   * de un array.
   */
  class StdLength : public StdAlgorithm{
  public:
    /** Constructor */
    StdLength() : StdAlgorithm(STDALG_LENGTH)
    {
      new_input_argument(generate_aux_name());
      new_output_argument(generate_aux_name());
    }

    /** Resuelve las dependencias de las variables del algoritmo. */
    void solve_dependencies(){
      SymbolsIterator var = inputArgs.begin();
      if (var->second.type.is_tmpexp()){
	DependenciesManager::solve_variable_dependencies(var);
      }
    
      solve_output_dependencies();
    }

    /** Resuelve las dependencias de los argumentos de salida. */
    void solve_output_dependencies(){
      SymbolsIterator var = outputArgs.begin();
      if (var->second.type.is_tmpexp())
	var->second.type = Type::make_integer();
    }

    /** Comprueba la semántica del uso del algoritmo. */
    bool check_semantic() const{
      return inputArgs.begin()->second.type.is_array();
    }
  
    /** Lanza errores. */ 
    void launch_error(const string& tostr) const{
      Types inputTypes(1,Type::make_array(Type::make_any()));
      ErrorManager::std_algorithm_illegal(tostr,STDALG_LENGTH,get_input_types(),inputTypes);
    }
  };

} // namespace LEAC
  
#endif // STD_LENGTH_H_
