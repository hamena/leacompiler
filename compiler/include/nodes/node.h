#ifndef NODE_H_
#define NODE_H_

//#define NODE_DEBUG 1

#ifndef _GLIBCXX_USE_CXX11_ABI
#define _GLIBCXX_USE_CXX11_ABI 0
#endif
#include <vector>
#include <sstream>

using namespace std;

typedef std::stringstream Stream;

namespace LEAC{

  /** Clase base abstracta Nodo. Clase base de todos los nodos
   * del AST.
   */
  class Node{
  public:
    /** Constructor
     * @param lno número de línea.*/
    Node(int lno) : lineno(lno), error_flag(false) {}

    /** Reconstruye el codigo LEA para los errores.
     * @return Cadena con código LEA. */
    virtual string to_string() = 0;
    /** Imprime en el búfer la salida XML correspondiente al nodo.
     * @param [out] os búfer de salida. */
    virtual void to_XML(Stream& os) = 0;
    /** Comprueba si existe estado de error.
     * @return Estado del error. */
    virtual bool error() { return error_flag; }

    /** Inicializa la tabla de símbolos. Este es el primer de los 4
     * pasos necesarios para el análisis semántico, en este caso se inicializan
     * las tablas de símbolos con todos los argumentos y variables del programa.
     */
    virtual void step_1() = 0;
    /** Establece las dependencias de tipo. Este es el segundo paso de los 4 
     * necesarios para el análisis semántico, en este caso se estableccen las
     * dependencias del tipo de cada argumento y variable.
     */
    virtual void step_2() = 0;
    /** Comprobación semántica. Este es el cuarto y último paso de los 4
     * necesarios para el análisis semántico, en este caso se realiza la
     * comprobación semántica del código. Una vez averiguado los tipos de
     * cada variable y argumento se comprueba que las reestricciones semánticas
     * se satisfagan.
     */
    virtual void step_4() = 0; // Comprobación semántica

    /** Destructor virtual de nodo. */
    virtual ~Node(){}
  protected:
    int lineno;
    bool error_flag;
  };

  /** Nodo nulo. Utilizado de forma auxiliar para parar listas recursivas. */
  class NodeNULL : public Node{
  public:
    NodeNULL() : Node(-1){}
  
    void step_1() {}
    void step_2(){}
    void step_4(){}
    string to_string(){ return ""; }
    void to_XML(Stream& os) { }
  };

  /** Nodo entrada. Representa una sucesión de nodos algoritmo. */
  class NodeEntry : public Node{
    Node *nodeFunction, *nodeEntry;
  public:
    NodeEntry(int lno, Node *f, Node *e) :
      Node(lno), nodeFunction(f), nodeEntry(e)
    {}
  
    void step_1() { 
      nodeFunction->step_1();
      nodeEntry->step_1();
    }

    void step_2(){
      nodeFunction->step_2();
      nodeEntry->step_2();
    }

    void step_4(){
      nodeFunction->step_4();
      nodeEntry->step_4();
    }

    bool error(){ return nodeFunction->error() || nodeEntry->error(); }
  
    string to_string(){ return nodeFunction->to_string() + "\n" + nodeEntry->to_string(); }
  
    void to_XML(Stream& os){
      nodeFunction->to_XML(os);
      nodeEntry->to_XML(os);
    }
  };

} // namespace LEAC
  
#endif // NODE_H_
