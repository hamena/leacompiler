#ifndef NODEASSIGNS_H_
#define NODEASSIGNS_H_

//#define NODEASSIGNS_DEBUG 1

#include <sstream>
#include <string>
#include <vector>
#include "node.h"
#include "../defstrings.h"
#include "../managers/xmloutputmanager.h"

using namespace std;

namespace LEAC{

  /************* ASIGNACION *************/
  /** Nodo asignación. Representa una instrucción asignación. */
  class NodeAssign : public Node{
    Node *assign;
  public:
    NodeAssign(int lno, Node* n) :
      Node(lno), assign(n){}

    void step_1() {
      assign->step_1();
    }

    void step_2() {
      assign->step_2();
    }

    void step_4() {
      assign->step_4();
    }
  
    bool error(){ return assign->error(); }
  
    string to_string(){ return assign->to_string(); }

    void to_XML(Stream& os){
      assign->to_XML(os);
    }
  };

  /** Nodo asignación simple. Representa la instrucción LEA asignación simple. */
  class NodeSimpleAssign : public Node{
    NodeExp *modExpression, *expression;
  public:
    NodeSimpleAssign(int lno, NodeExp* n, NodeExp* m) :
      Node(lno), modExpression(n), expression(m)
    {  }
  
    void step_1() {
      string id = modExpression->get_id();
      if (!Algorithms::get_current_algorithm().is_input_arg(id)){
	modExpression->step_1();
	expression->step_1();
      }
      else{
	error_flag = true;
	ErrorManager::input_arg_only_read(lineno,to_string(),id);
      }
    }
  
    void step_2() {
      Algorithm& algorithm = Algorithms::get_current_algorithm();
      string id = modExpression->get_id();
      SymbolsIterator refVar = algorithm.get_variable_reference(id);
      int dim = modExpression->get_n_access();
      DependenciesManager::new_dependency(refVar,expression,dim);
      refVar->second.type.set_tmpexp();
      
      modExpression->step_2();
      expression->step_2();
    }

    void step_4() {
      modExpression->step_4();
      expression->step_4();
      if (expression->get_count() == 1){
	Algorithm& algorithm = Algorithms::get_current_algorithm();
	Types t1 = modExpression->get_types();
	Types t2 = expression->get_types();
	if (ConversionManager::is_legal(t1[0],t2[0]) || ConversionManager::is_legal(t2[0],t1[0])){
	  algorithm.set_type(modExpression->get_id(),ConversionManager::solve(t1[0],t2[0]));
	}
	else{
	  error_flag = true;
	  ErrorManager::simple_assign_not_legal(lineno,to_string(),t1[0],t2[0]);
	}
      }
      else{
	error_flag = true;
	ErrorManager::simple_assign_doesnt_match(lineno,to_string());
      }
    }
     
    bool error(){ return modExpression->error() || expression->error() || error_flag; }
  
    string to_string(){ return modExpression->to_string()+" <-- "+expression->to_string(); }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames names = {ATTR_CLASS};
      XMLOutputManager::AttributesValues values = {INSTR_SASSIGN};
      XMLOutputManager::open_tag(os,XMLTAG_INSTRUCTION,names,values);
      modExpression->to_XML(os);
      expression->to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo asignación paralela. Representa la instrucción LEA asignación paralela. */
  class NodeParallelAssign : public Node{
    NodeExp *modificable_tuple, *readable_tuple;
  public:
    NodeParallelAssign(int lno, NodeExp *mt, NodeExp *rt) :
      Node(lno), modificable_tuple(mt), readable_tuple(rt)
    {  }

    void step_1() {
      modificable_tuple->step_1();
      readable_tuple->step_1();
    }

    void step_2() {
      if (modificable_tuple->get_count() != readable_tuple->get_count()){
	error_flag = true;
	ErrorManager::parallel_assign_doesnt_match(lineno,to_string());
      }
      else{
	modificable_tuple->step_2();
	readable_tuple->step_2();
	modificable_tuple->push();
	readable_tuple->push();
      }
    }

    void step_4() {
      modificable_tuple->step_4();
      readable_tuple->step_4();
      Algorithm& algorithm = Algorithms::get_current_algorithm();
      vector<string> ids = modificable_tuple->get_ids();
      Types types1 = modificable_tuple->get_types();
      Types types2 = readable_tuple->get_types();
      if (types1.size() == types2.size()){
	size_t size = types1.size();
	for (unsigned int i=0; i<size; ++i){
	  Type t1 = types1[i];
	  Type t2 = types2[i];
	  if (ConversionManager::is_legal(t1,t2) || ConversionManager::is_legal(t2,t1))
	    algorithm.set_type(ids[i],ConversionManager::solve(t1,t2));
	  else
	    error_flag = true;
	}
	if (error_flag)
	  ErrorManager::parallel_assign_not_legal(lineno,to_string(),types1,types2);
      }
      else{
	error_flag = true;
	ErrorManager::parallel_assign_doesnt_match(lineno,to_string());
      }
    }

    bool error() { return modificable_tuple->error() || readable_tuple->error() || error_flag; }
  
    string to_string(){
      return modificable_tuple->to_string()+" <-- "+readable_tuple->to_string();
    }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames names = {ATTR_CLASS};
      XMLOutputManager::AttributesValues values = {INSTR_PASSIGN};
      XMLOutputManager::open_tag(os,XMLTAG_INSTRUCTION,names,values);
      modificable_tuple->to_XML(os);
      readable_tuple->to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo asignación intercambio. Representa la instrucción LEA asignación intercambio. */
  class NodeSwapAssign : public Node{
    NodeExp *lModExp, *rModExp;
  public:
    NodeSwapAssign(int lno, NodeExp *l, NodeExp *r) :
      Node(lno), lModExp(l), rModExp(r)
    {  }

    void step_1() {
      string lid = lModExp->get_id();
      string rid = rModExp->get_id();
      Algorithm& algorithm = Algorithms::get_current_algorithm();
      if (!algorithm.check_variable(lid)){
	error_flag = true;
	ErrorManager::variable_not_found(lineno,to_string(),lid);
      }
      else if (!algorithm.check_variable(rid)){
	error_flag = true;
	ErrorManager::variable_not_found(lineno,to_string(),rid);
      }
      else {
	if (algorithm.is_arg(lid) || algorithm.is_arg(rid)){
	  error_flag = true;
	  if (algorithm.is_output_arg(lid))
	    ErrorManager::output_arg_only_write(lineno,to_string(),lid);
	  else if (algorithm.is_input_arg(lid))
	    ErrorManager::input_arg_only_read(lineno,to_string(),lid);
	  else if (algorithm.is_output_arg(rid))
	    ErrorManager::output_arg_only_write(lineno,to_string(),rid);
	  else if (algorithm.is_input_arg(rid))
	    ErrorManager::input_arg_only_read(lineno,to_string(),rid);
	}
	else {
	  lModExp->step_1();
	  rModExp->step_1();
	}
      }
    }

    void step_2() {
      if (lModExp->get_count() != rModExp->get_count()){
	error_flag = true;
	ErrorManager::swap_assign_doesnt_match(lineno,to_string());
      }
      else{
	int lAccess = lModExp->get_n_access();
	int rAccess = rModExp->get_n_access();
	Algorithm& algorithm = Algorithms::get_current_algorithm();
	SymbolsIterator refVar = algorithm.get_variable_reference(lModExp->get_id());
	DependenciesManager::new_dependency(refVar,rModExp,rAccess);
	refVar->second.type.set_tmpexp();
      
	refVar = algorithm.get_variable_reference(rModExp->get_id());
	DependenciesManager::new_dependency(refVar,lModExp,lAccess);
	refVar->second.type.set_tmpexp();
      
	lModExp->step_2();
	rModExp->step_2();
      }
    }

    void step_4() {
      lModExp->step_4();
      rModExp->step_4();
      Algorithm& algorithm = Algorithms::get_current_algorithm();
      Type t1 = lModExp->get_type();
      Type t2 = rModExp->get_type();
      if (ConversionManager::is_legal(t1,t2) || ConversionManager::is_legal(t2,t1)){
	Type solved = ConversionManager::solve(t1,t2);
	algorithm.set_type(lModExp->get_id(),solved);
	algorithm.set_type(rModExp->get_id(),solved);
      }
      else{
	error_flag = true;
	ErrorManager::swap_assign_not_legal(lineno,to_string(),t1,t2);
      }
    }

    bool error(){ return lModExp->error() || rModExp->error() || error_flag; }
    
    string to_string(){ return lModExp->to_string()+" <-> "+rModExp->to_string(); }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames names = {ATTR_CLASS};
      XMLOutputManager::AttributesValues values = {INSTR_SWAP};
      XMLOutputManager::open_tag(os,XMLTAG_INSTRUCTION,names,values);
      lModExp->to_XML(os);
      rModExp->to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

} // namespace LEAC
  
#endif
