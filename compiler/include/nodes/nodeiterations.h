#ifndef NODEITERATIONS_H_
#define NODEITERATIONS_H_

#include <sstream>
#include <string>
#include "node.h"
#include "../defstrings.h"
#include "../managers/xmloutputmanager.h"

using namespace std;

namespace LEAC{

  /** Nodo bucle mientras. Representa la instrucción de control de flujo de LEA mientras. */
  class NodeWhileLoop : public Node{
    NodeExp *expression;
    Node *codeblock;
  public:
    NodeWhileLoop(int lno, NodeExp* e, Node* c) :
      Node(lno), expression(e), codeblock(c)
    {  }

    void step_1() { 
      Algorithms::get_current_algorithm().next_scope();
      expression->step_1();
      codeblock->step_1();
      Algorithms::get_current_algorithm().close_scope();
    }

    void step_2() {
      Algorithms::get_current_algorithm().next_scope();
      expression->step_2();
      codeblock->step_2();
      Algorithms::get_current_algorithm().close_scope();
    }

    void step_4() {
      Algorithms::get_current_algorithm().next_scope();
      expression->step_4();
      Type type = expression->get_type();
      if (!type.is_logic() && !type.is_undef()){
	error_flag = true;
	ErrorManager::expression_not_type(lineno,"mientras "+expression->to_string(),
					  expression->to_string(),Type::make_logic());
      }
      codeblock->step_4();
      Algorithms::get_current_algorithm().close_scope();
    }

    bool error() { return error_flag || expression->error() || codeblock->error(); }
  
    string to_string(){
      return "mientras "+expression->to_string()+"\n"+codeblock->to_string()+"\n";
    }

    void to_XML(Stream& os){
      Algorithms::get_current_algorithm().next_scope();
      XMLOutputManager::AttributesNames names = {ATTR_CLASS};
      XMLOutputManager::AttributesValues values = {INSTR_WHILE};
      XMLOutputManager::open_tag(os,XMLTAG_INSTRUCTION,names,values);
      expression->to_XML(os);
      codeblock->to_XML(os);
      XMLOutputManager::close_tag(os);
      Algorithms::get_current_algorithm().close_scope();
    }
  };

  /** Nodo bucle repite. Representa la instrucción de control de flujo de LEA repite. */
  class NodeRepeatLoop : public Node{
    Node *codeblock;
    Node *repeatType;
  public:
    NodeRepeatLoop(int lno, Node *c, Node *r) :
      Node(lno), codeblock(c), repeatType(r)
    { }

    void step_1() { 
      Algorithms::get_current_algorithm().next_scope();
      codeblock->step_1();
      repeatType->step_1();
      Algorithms::get_current_algorithm().close_scope();
    }

    void step_2() {
      Algorithms::get_current_algorithm().next_scope();
      codeblock->step_2();
      repeatType->step_2();
      Algorithms::get_current_algorithm().close_scope();
    }

    void step_4() {
      Algorithms::get_current_algorithm().next_scope();
      codeblock->step_4();
      repeatType->step_4();
      Algorithms::get_current_algorithm().close_scope();
    }

    bool error() { return error_flag || codeblock->error() || repeatType->error(); }
  
    string to_string(){
      return "repite "+codeblock->to_string()+repeatType->to_string();
    }

    void to_XML(Stream& os){
      Algorithms::get_current_algorithm().next_scope();
      XMLOutputManager::AttributesNames names = {ATTR_CLASS};
      XMLOutputManager::AttributesValues values = {INSTR_REPEAT};
      XMLOutputManager::open_tag(os,XMLTAG_INSTRUCTION,names,values);
      codeblock->to_XML(os);
      repeatType->to_XML(os);
      XMLOutputManager::close_tag(os);
      Algorithms::get_current_algorithm().close_scope();
    }
  };

  /** Nodo bucle repite mientras. Representa la variación mientras de la instrucción repite. */
  class NodeRepeatWhileLoop : public Node{
    NodeExp *expression;
  public:
    NodeRepeatWhileLoop(int lno, NodeExp *e) : Node(lno), expression(e)
    {  }

    void step_1() { 
    
    }

    void step_2() {
      expression->step_2();
    }

    void step_4() {
      expression->step_4();
      Type type = expression->get_type();
      if (!type.is_logic() && !type.is_undef()){
	error_flag = true;
	ErrorManager::expression_not_type(lineno,"mientras "+expression->to_string(),
					  expression->to_string(),Type::make_logic());
      }
    }

    bool error(){ return error_flag || expression->error(); }
  
    string to_string(){ return "mientras "+expression->to_string()+"\n"; }

    void to_XML(Stream& os){
      XMLOutputManager::open_tag(os,XMLTAG_REPEAT_WHILE);
      expression->to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo bucle repite hasta. Representa la variación hasta de la instrucción repite. */
  class NodeRepeatUntilLoop : public Node{
    NodeExp *expression;
  public:
    NodeRepeatUntilLoop(int lno, NodeExp *e) : Node(lno), expression(e)
    {  }

    void step_1() { 
      
    }

    void step_2() {
      expression->step_2();
    }

    void step_4() {
      expression->step_4();
      Type type = expression->get_type();
      if (!type.is_logic() && !type.is_undef()){
	error_flag = true;
	ErrorManager::expression_not_type(lineno,"hasta "+expression->to_string(),
					  expression->to_string(),Type::make_logic());
      }
    }

    bool error(){ return error_flag || expression->error(); }
  
    string to_string(){ return "hasta "+expression->to_string(); }

    void to_XML(Stream& os){
      XMLOutputManager::open_tag(os,XMLTAG_REPEAT_UNTIL);
      expression->to_XML(os);
      XMLOutputManager::close_tag(os);
    }

  };

  /** Nodo bucle desde. Representa la instrucción LEA bucle desde. */
  class NodeFromLoop : public Node{
    Node *assign, *looptype;
  public:
    NodeFromLoop(int lno, Node *a, Node *l) :
      Node(lno), assign(a), looptype(l) {}

    void step_1() { 
      Algorithms::get_current_algorithm().next_scope();
      assign->step_1();
      looptype->step_1();
      Algorithms::get_current_algorithm().close_scope();
    }

    void step_2() {
      Algorithms::get_current_algorithm().next_scope();
      assign->step_2();
      looptype->step_2();
      Algorithms::get_current_algorithm().close_scope();
    }

    void step_4() {
      Algorithms::get_current_algorithm().next_scope();
      assign->step_4();
      looptype->step_4();
      Algorithms::get_current_algorithm().close_scope();
    }

    bool error(){ return error_flag || assign->error() || looptype->error(); }
  
    string to_string(){
      return "desde "+assign->to_string()+" "+looptype->to_string();
    }

    void to_XML(Stream& os){
      Algorithms::get_current_algorithm().next_scope();
      XMLOutputManager::AttributesNames names = {ATTR_CLASS};
      XMLOutputManager::AttributesValues values = {INSTR_FROM};
      XMLOutputManager::open_tag(os,XMLTAG_INSTRUCTION,names,values);
      assign->to_XML(os);
      looptype->to_XML(os);
      XMLOutputManager::close_tag(os);
      Algorithms::get_current_algorithm().close_scope();
    }
  };

  /** Nodo bucle desde general. Representa la variación general del bucle desde.*/
  class NodeFromGeneralLoop : public Node{
    NodeExp *expression;
    Node *assign, *codeblock;
  public:
    NodeFromGeneralLoop(int lno, NodeExp *e, Node *a, Node *c) :
      Node(lno), expression(e), assign(a), codeblock(c)
    {  }

    void step_1() { 
      expression->step_1();
      assign->step_1();
      codeblock->step_1();
    }

    void step_2() {
      expression->step_2();
      assign->step_2();
      codeblock->step_2();
    }

    void step_4() {
      expression->step_4();
      Type type = expression->get_type();
      if (!type.is_logic() && !type.is_undef()){
	error_flag = true;
	ErrorManager::expression_not_type(lineno,"mientras "+expression->to_string()+
					  " con "+assign->to_string(),
					  expression->to_string(),Type::make_logic());
      }
      assign->step_4();
      codeblock->step_4();
    }

    bool error(){
      return error_flag || expression->error() || assign->error() || codeblock->error();
    }
  
    string to_string()
    { return
	" mientras "+expression->to_string()+
	" con "+assign->to_string()+
	codeblock->to_string();
    }

    void to_XML(Stream& os){
      XMLOutputManager::open_tag(os,XMLTAG_FROM_GENERAL);
      expression->to_XML(os);
      assign->to_XML(os);
      XMLOutputManager::close_tag(os);
      codeblock->to_XML(os);
    }
  };

  /** Nodo bucle desde numérico. Representa la variación numérica del bucle desde.*/
  class NodeFromNumericLoop : public Node{
    NodeExp *expression;
    Node *codeblock;
  public:
    NodeFromNumericLoop(int lno, NodeExp *e, Node *c) :
      Node(lno), expression(e), codeblock(c)
    {  }

    void step_1() { 
      expression->step_1();
      codeblock->step_1();
    }

    void step_2() {
      expression->step_2();
      codeblock->step_2();
    }

    void step_4() {
      expression->step_4();
      Type type = expression->get_type();
      if (!type.is_integer() && !type.is_float() && !type.is_undef()){
	error_flag = true;
	// TODO: Integer o Float...
	ErrorManager::expression_not_type(lineno,"hasta "+expression->to_string(),
					  expression->to_string(),Type::make_integer());
      }
      codeblock->step_4();
    }

    bool error(){ return error_flag || expression->error() || codeblock->error(); }
  
    string to_string(){
      return " hasta "+expression->to_string()+" "+codeblock->to_string();
    }

    void to_XML(Stream& os){
      XMLOutputManager::open_tag(os,XMLTAG_FROM_NUMERIC);
      expression->to_XML(os);
      XMLOutputManager::close_tag(os);
      codeblock->to_XML(os);
    }
  };

  /** Nodo bucle desde con incrementos. 
   * Representa la variación incrementos del bucle desde numérico. */
  class NodeFromNumericIncDecLoop : public Node{
    NodeExp *lexpression;
    int incdec;
    NodeExp *rexpression;
    Node *codeblock;
  public:
    NodeFromNumericIncDecLoop(int lno, NodeExp *le, int t, NodeExp *re, Node *c) :
      Node(lno), lexpression(le), incdec(t), rexpression(re), codeblock(c)
    {  }

    void step_1() { 
      lexpression->step_1();
      rexpression->step_1();
      codeblock->step_1();
    }

    void step_2() {
      lexpression->step_2();
      rexpression->step_2();
      codeblock->step_2();
    }

    void step_4() {
      lexpression->step_4();

      string aux = lexpression->to_string();
      if (incdec == 1) aux += " con incremento ";
      else if (incdec == 2) aux += " con decremento ";
      aux += rexpression->to_string();

      Type type = lexpression->get_type();
      if (!type.is_integer() && !type.is_float() && !type.is_undef()){
	error_flag = true;
	// TODO: Integer o Float...
	ErrorManager::expression_not_type(lineno,aux,lexpression->to_string(),
					  Type::make_integer());
      }
      rexpression->step_4();
      type = rexpression->get_type();
      if (!type.is_integer() && !type.is_float() && !type.is_undef()){
	error_flag = true;
	// TODO: Integer o Float...
	ErrorManager::expression_not_type(lineno,aux,rexpression->to_string(),
					  Type::make_integer());
      }
      codeblock->step_4();
    }

    bool error(){
      return error_flag || lexpression->error() || rexpression->error() || codeblock->error();
    }
  
    string to_string(){ string aux = lexpression->to_string();
      if (incdec == 1) aux += " con incremento ";
      else if (incdec == 2) aux += " con decremento ";
      else aux = "borrame";
      return aux+rexpression->to_string()+codeblock->to_string();
    }

    void to_XML(Stream& os){
      XMLOutputManager::open_tag(os,XMLTAG_FROM_NUMERIC);
      lexpression->to_XML(os);
      if (incdec == 1){
	XMLOutputManager::open_tag(os,XMLTAG_INCREMENT);
	rexpression->to_XML(os);
	XMLOutputManager::close_tag(os);
      }
      else if (incdec == 2){
	XMLOutputManager::open_tag(os,XMLTAG_DECREMENT);
	rexpression->to_XML(os);
	XMLOutputManager::close_tag(os);
      }
      XMLOutputManager::close_tag(os);
      codeblock->to_XML(os);
    }
  };

  /** Nodo bucle para todo. Representa la instrucción LEA bucle para todo. */
  class NodeForAllLoop : public Node {
    Node *in;
    Node *codeblock;
  public:
    NodeForAllLoop(int lno, Node *i, Node *c) :
      Node(lno), in(i), codeblock(c)
    {  }

    void step_1() { 
      Algorithms::get_current_algorithm().next_scope();
      in->step_1();
      codeblock->step_1();
      Algorithms::get_current_algorithm().close_scope();
    }

    void step_2() {
      Algorithms::get_current_algorithm().next_scope();
      in->step_2();
      codeblock->step_2();
      Algorithms::get_current_algorithm().close_scope();
    }

    void step_4() {
      Algorithms::get_current_algorithm().next_scope();
      in->step_4();
      codeblock->step_4();
      Algorithms::get_current_algorithm().close_scope();
    }

    bool error(){ return error_flag || in->error() || codeblock->error(); }
  
    string to_string(){
      return "para todo "+in->to_string()+codeblock->to_string();
    }

    void to_XML(Stream& os){
      Algorithms::get_current_algorithm().next_scope();
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS};
      XMLOutputManager::AttributesValues values = {INSTR_FOR_ALL};
      XMLOutputManager::open_tag(os,XMLTAG_INSTRUCTION,names,values);
      in->to_XML(os);
      codeblock->to_XML(os);
      XMLOutputManager::close_tag(os);
      Algorithms::get_current_algorithm().close_scope();
    }
  };

  /** Nodo bucle para todo en. Representa la expresión en que tiene un bucle para todo. */
  class NodeForAllIn : public Node{
    string ID;
    NodeExp *expression;
  public:
    NodeForAllIn(int lno, const string& id, NodeExp *e) :
      Node(lno), ID(id), expression(e)
    {  }

    void step_1() {
      Algorithm& algorithm = Algorithms::get_current_algorithm();
      if (!algorithm.check_variable(ID))
	algorithm.new_variable(ID,Type());
      expression->step_1();
    }

    void step_2() {
      SymbolsIterator refVar = Algorithms::get_current_algorithm().get_variable_reference(ID);
      DependenciesManager::new_in_dependency(refVar,expression);
      refVar->second.type.set_tmpexp();
      expression->step_2();
    }

    void step_4() {
      expression->step_4();
      Type type = expression->get_type();
      if (!type.is_set() && !type.is_undef()){
	error_flag = true;
	ErrorManager::expression_not_type(lineno,to_string(),expression->to_string(),
					  Type::make_set(Type::make_null_putoff()));
      }
    }

    bool error(){ return error_flag || expression->error(); }
  
    string to_string(){ return ID+" en "+expression->to_string(); }

    void to_XML(Stream& os){
      const Variable& var = Algorithms::get_current_algorithm().get_variable(ID);
      XMLOutputManager::AttributesNames  names  = {ATTR_ID};
      XMLOutputManager::AttributesValues values = {var.codeName};
      XMLOutputManager::open_tag(os,XMLTAG_ITERATOR,names,values);
      var.type.to_XML(os);
      XMLOutputManager::close_tag(os);
      expression->to_XML(os);
    }
  };

} // namespace LEAC
  
#endif // NODEITERATIONS_H_
