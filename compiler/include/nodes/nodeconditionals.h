#ifndef NODECONDITIONALS_H_
#define NODECONDITIONALS_H_

#include <sstream>
#include <string>
#include "node.h"
#include "../defstrings.h"
#include "../managers/xmloutputmanager.h"

using namespace std;

namespace LEAC{

  /** Nodo condicional. Representa la instrucción de control de flujo de LEA si. */
  class NodeConditional : public Node{
    NodeExp *expression;
    Node *codeblock, *ifelse;
  public:
    NodeConditional(int lno, NodeExp *e, Node *c, Node *i) :
      Node(lno), expression(e), codeblock(c), ifelse(i)
    {  }

    void step_1() {
      Algorithms::get_current_algorithm().next_scope();
      expression->step_1();
      codeblock->step_1();
      Algorithms::get_current_algorithm().close_scope();
    
      if (ifelse){
	Algorithms::get_current_algorithm().next_scope();
	ifelse->step_1();
	Algorithms::get_current_algorithm().close_scope();
      }
    }

    void step_2() {
      Algorithms::get_current_algorithm().next_scope();
      expression->step_2();
      codeblock->step_2();
      Algorithms::get_current_algorithm().close_scope();

      if (ifelse){
	Algorithms::get_current_algorithm().next_scope();
	ifelse->step_2();
	Algorithms::get_current_algorithm().close_scope();
      }
    }

    void step_4() {
      Algorithms::get_current_algorithm().next_scope();
      expression->step_4();

      Type type = expression->get_type();
      if (!type.is_logic() && !type.is_undef()){
	error_flag = true;
	ErrorManager::expression_not_type(lineno,"si "+expression->to_string(),
					  expression->to_string(), Type::make_logic());
      }
    
      codeblock->step_4();
      Algorithms::get_current_algorithm().close_scope();

      if (ifelse){
	Algorithms::get_current_algorithm().next_scope();
	expression->step_4();
	codeblock->step_4();
	Algorithms::get_current_algorithm().close_scope();
      }
    }

    bool error() {
      bool aux = false;
      if (ifelse)
	aux = ifelse->error();
      return error_flag || expression->error() || codeblock->error() || aux;
    }

    string to_string(){
      string aux = "si "+expression->to_string()+codeblock->to_string();
      return aux + (ifelse!=nullptr ? "si no"+ifelse->to_string() : "");
    }

    void to_XML(Stream& os){
      Algorithms::get_current_algorithm().next_scope();
      XMLOutputManager::AttributesNames names = {ATTR_CLASS};
      XMLOutputManager::AttributesValues values = {INSTR_IF};
      XMLOutputManager::open_tag(os,XMLTAG_INSTRUCTION,names,values);
      expression->to_XML(os);
      codeblock->to_XML(os);
      Algorithms::get_current_algorithm().close_scope();
      if (ifelse){
	Algorithms::get_current_algorithm().next_scope();
	XMLOutputManager::open_tag(os,XMLTAG_ELSE);
	ifelse->to_XML(os);
	XMLOutputManager::close_tag(os);
	Algorithms::get_current_algorithm().close_scope();
      }
      XMLOutputManager::close_tag(os);
    }
  };
} // namespace LEAC

#endif // NODECONDITIONALS_H_
