#ifndef NODEEXPRESSIONS_H_
#define NODEEXPRESSIONS_H_

//#define NODEEXPRESSIONS_DEBUG 1

#include <sstream>
#include "node.h"
#include "../defstrings.h"
#include "../algorithms.h"
#include "../type.h"
#include "../managers/conversionmanager.h"
#include "../managers/dependenciesmanager.h"
#include "../managers/errormanager.h"
#include "../managers/xmloutputmanager.h"

using namespace std;

namespace LEAC{

  /************* NODO EXPRESION BASE *************/
  /** Nodo base expresión. Este nodo sirve de clase base para todos las expresiones de LEA. */
  class NodeExp : public Node{
  public:
    /** Constructor */
    NodeExp(int lno) : Node(lno)
    { }

    /** Función auxiliar push. Función auxiliar para establecer las dependencias de 
     * listas de expresiones como, llamadas a funciones o asignaciones paralelas. */
    virtual void push() {}
    /** Obtienes las referencias a las subexpresiones.
     *  @return vector de punteros a expresiones. */
    virtual vector<NodeExp*> get_references() { return vector<NodeExp*>(1,this); }
    /** Obtienes el identificador de la expresión. 
     *  @return identificador. */
    virtual string get_id() { return ""; }
    /** Obtienes la lista de identificadores de una tupla de expresiones.
     *  @return vector de identificadores. */
    virtual vector<string> get_ids() { return vector<string>(); }
    /** Obtienes el tamaño de una expresión tupla.
     *  @return número de expresiones de una tupla, por defecto 1. */
    virtual unsigned int get_count(){ return 1; }
    /** Obtienes el número de accesos.
     *  @return número de accesos a una expresión variable, por defecto 0. */
    virtual unsigned int get_n_access(){ return 0; }
    /** Obtienes el tipo de la expresión.
     *  @return tipo de la expresión. */
    virtual Type get_type() { return Type::make_undef(); }
    /** Obtienes los tipos de la tupla de expresiones. 
     *  @return tipos de la tupla de expresiones. */
    virtual Types get_types() { return Types(1,get_type()); }
  };

  /** Nodo expresión nula. Nodo auxiliar usado para parar la recursividad en las tuplas de
   * expresiones. */
  class NodeExpNULL : public NodeExp{
  public:
    NodeExpNULL() : NodeExp(-1)
    { }
  
    void step_1() {}
    void step_2() {}
    void step_4() {}
    vector<NodeExp*> get_references() { return vector<NodeExp*>(); }
    unsigned int get_count(){ return 0; }
    string to_string(){ return ""; }
    void to_XML(Stream& os){ }
  };

  /************* EXPRESIONES MODIFICABLES *************/
  /** Nodo expresión modificable. Representa las variables del programa. */
  class NodeModificableExpression : public NodeExp{
    string ID;
    NodeExp *access;
    SymbolsIterator refVar;
  public:
    NodeModificableExpression(int lno, const string& id, NodeExp* n) :
      NodeExp(lno), ID(id), access(n)
    {  }

    void step_1() {
      Algorithm& algorithm = Algorithms::get_current_algorithm();
      if (!algorithm.is_input_arg(ID)){
	if (!algorithm.check_variable(ID)){
	  Type t;
	  algorithm.new_variable(ID,t);
	  if (access)
	    access->step_1();
	}
      }
      else{
	error_flag = true;
	ErrorManager::input_arg_only_read(lineno,to_string(),ID);
      }
    }

    void step_2() {
      refVar = Algorithms::get_current_algorithm().get_variable_reference(ID);
      if (access)
	access->step_2();
    }

    void step_4() {
      if (access)
	access->step_4();
    }
  
    void push() {
      int nAccess = 0;
      if (access)
	nAccess = access->get_n_access();
      DependenciesManager::push_variable(refVar,nAccess);
    }

    string get_id() { return ID; }

    vector<string> get_ids() { return vector<string>(1,ID); }

    unsigned int get_n_access(){
      if (access)
	return access->get_n_access();
      else
	return 0;
    }

    Type get_type(){
      Type var = refVar->second.type;
      if(var.is_tmpexp())
	DependenciesManager::solve_variable_dependencies(refVar);
      if (refVar->second.type.is_tmpexp()){
	return Type::make_null_putoff();
      }
      else if (refVar->second.type.is_ready()){
	var = refVar->second.type;
	if (access && var.is_array()){
	  //reducir dimension por cada acceso realizado
	  int nAccess = access->get_n_access();
	  int dim = var.get_dimension();
	  if ((dim - nAccess) > 0)
	    var.set_dimension(dim-nAccess);
	  else
	    var = var.get_sub_type();
	}
      }
    
      return var;
    }

    bool error(){
      if (access)
	return error_flag || access->error();
      else
	return error_flag;
    }
  
    string to_string(){
      if (access)
	return ID + access->to_string();
      else
	return ID;
    }

    void to_XML(Stream& os){
      string codename = refVar->second.codeName;
      XMLOutputManager::AttributesNames  names  = {ATTR_ID};
      XMLOutputManager::AttributesValues values = {codename};
      XMLOutputManager::open_tag(os,XMLTAG_VARIABLE,names,values);
      if (access)
	access->to_XML(os);
      refVar->second.type.to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo acceso. Representa el acceso con corchetes a las variables, contiene una tupla de
   * expresiones. */
  class NodeAccess : public NodeExp{
    NodeExp *expressions;
  public:
    NodeAccess(int lno, NodeExp *e) : NodeExp(lno), expressions(e)
    {  }

    void step_1() {
      expressions->step_1();   
    }

    void step_2() {
      expressions->step_2();
    }

    void step_4() {
      expressions->step_4();
      Types types = expressions->get_types();
      for (unsigned int i=0; i<types.size(); ++i)
	if (!types[i].is_integer())
	  error_flag = true;
      if (error_flag)
	ErrorManager::access_not_integer(lineno,to_string(),types);
    }

    unsigned int get_n_access(){ return expressions->get_count(); }
  
    bool error(){ return error_flag || expressions->error(); }
  
    string to_string(){ return "[" + expressions->to_string() +"]"; }

    void to_XML(Stream& os){
      XMLOutputManager::open_tag(os,XMLTAG_ACCESS);
      expressions->to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo tupla modificable. Representa una tupla de variables. */
  class NodeModificableTuple : public NodeExp{
    NodeExp *expressions;
  public:
    NodeModificableTuple(int lno, NodeExp *es) :
      NodeExp(lno), expressions(es)
    {  }

    void step_1() {
      expressions->step_1();
    }

    void step_2() {
      expressions->step_2();
    }

    void step_4() {
      expressions->step_4();
    }

    void push() { expressions->push(); }

    vector<string> get_ids() { return expressions->get_ids(); }

    unsigned int get_count() { return expressions->get_count(); }

    Types get_types(){ return expressions->get_types(); }

    bool error(){ return expressions->error(); }
  
    string to_string(){ return "("+expressions->to_string()+")"; }

    void to_XML(Stream& os){
      XMLOutputManager::open_tag(os,XMLTAG_TUPLE_VARS);
      expressions->to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo expresiones modificables. Representa una lista de variables. */
  class NodeModificableExpressions : public NodeExp{
    NodeExp *expression, *expressions;
  public:
    NodeModificableExpressions(int lno, NodeExp *e, NodeExp *es) :
      NodeExp(lno), expression(e), expressions(es)
    {  }

    void step_1() {
      expression->step_1();
      expressions->step_1();
    }

    void step_2() {
      expression->step_2();
      expressions->step_2();
    }

    void step_4() {
      expression->step_4();
      expressions->step_4();
    }

    void push() {
      expression->push();
      expressions->push();
    }

    vector<string> get_ids(){
      vector<string> ids(1,expression->get_id());
      vector<string> aux = expressions->get_ids();
      ids.insert(ids.end(),aux.begin(),aux.end());
      return ids;
    }

    unsigned int get_count(){ return expression->get_count() + expressions->get_count(); }

    Types get_types(){
      Types types(1,expression->get_type());
      Types aux = expressions->get_types();
      types.insert(types.end(),aux.begin(),aux.end());
      return types;
    }
  
    bool error(){ return expression->error() || expressions->error(); }
  
    string to_string(){ return expression->to_string()+","+expressions->to_string(); }

    void to_XML(Stream& os){
      expression->to_XML(os);
      expressions->to_XML(os);
    }
  };

  /************* EXPRESIONES *************/
  /** Nodo expresiones. Representa una lista de expresiones. */
  class NodeExpressions : public NodeExp{
    NodeExp *expression, *expressions;
  public:
    NodeExpressions(int lno, NodeExp *n, NodeExp *m) :
      NodeExp(lno), expression(n), expressions(m)
    {  }

    void step_1() { 
      expression->step_1();
      expressions->step_1();
    }

    void step_2() {
      expression->step_2();
      expressions->step_2();
    }

    void step_4() {
      expression->step_4();
      expressions->step_4();
    }

    void push() {
      DependenciesManager::push_expression(expression);
      expressions->push();
    }

    vector<NodeExp*> get_references() {
      vector<NodeExp*> aux, exps = expressions->get_references();
      aux.push_back(expression);
      aux.insert(aux.end(),exps.begin(),exps.end());
      return aux;
    }

    unsigned int get_count(){ return expression->get_count() + expressions->get_count(); }

    Types get_types(){
      Types types(1,expression->get_type());
      Types aux = expressions->get_types();
      types.insert(types.end(),aux.begin(),aux.end());
      return types;
    }
  
    bool error(){ return expression->error() || expressions->error(); }
  
    string to_string(){ return expression->to_string() + "," + expressions->to_string(); }

    void to_XML(Stream& os){
      expression->to_XML(os);
      expressions->to_XML(os);
    }
  };

  /** Nodo relacional1. Representa las expresiones binarias == y ~= */
  class NodeExpression : public NodeExp{
    NodeExp *expression, *conj_log;
    string op;
  public:
    NodeExpression(int lno, NodeExp *n, const string& o, NodeExp *m) :
      NodeExp(lno), expression(n), conj_log(m), op(o)
    {  }

    void step_1() {
      expression->step_1();
      conj_log->step_1();
    }

    void step_2() {
      if (expression->get_count() == 1 && conj_log->get_count() == 1){
	expression->step_2();
	conj_log->step_2();
      }
      else{
	error_flag = true;
	ErrorManager::boperator_doesnt_match(lineno,to_string(),op);
      }
    }

    void step_4() {
      expression->step_4();
      conj_log->step_4();
    }

    void push() {
      DependenciesManager::push_expression(this);
    }

    Type get_type(){
      Type exp1 = expression->get_type();
      Type exp2 = conj_log->get_type();

      if (exp1.is_undef() || exp2.is_undef())
	return Type::make_undef();

      if (exp1.is_putoff() || exp2.is_putoff()){
	exp1.set_putoff();
	return exp1;
      }

      if (ConversionManager::is_legal_expr(exp1,exp2,op))
	{
	  return Type::make_logic();
	}
      else
	{
	  error_flag = true;
	  ErrorManager::bexpression_not_legal(lineno,to_string(),exp1,op,exp2);
	  return Type::make_undef();
	}
    }

    bool error(){ return error_flag || expression->error() || conj_log->error(); }
  
    string to_string(){ return expression->to_string()+" "+op+" "+conj_log->to_string(); }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS  , ATTR_OPER};
      XMLOutputManager::AttributesValues values = {CLASS_BINARY, op};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      expression->to_XML(os);
      conj_log->to_XML(os);
      get_type().to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo conjunción lógica. Representa la operación lógica binaria and. */
  class NodeConjLog : public NodeExp{
    NodeExp *conj_log, *disy_log;
    string op;
  public:
    NodeConjLog(int lno, NodeExp *n, NodeExp *m) :
      NodeExp(lno), conj_log(n), disy_log(m), op(OPER_CONJ)
    {  }

    void step_1() {
      conj_log->step_1();
      disy_log->step_1();
    }

    void step_2() {
      if (conj_log->get_count() == 1 && disy_log->get_count() == 1){
	conj_log->step_2();
	disy_log->step_2();
      }
      else{
	error_flag = true;
	ErrorManager::boperator_doesnt_match(lineno,to_string(),op);
      }
    }

    void step_4() {
      conj_log->step_4();
      disy_log->step_4();
    }

    void push() {
      DependenciesManager::push_expression(this);
    } 

    Type get_type(){
      Type exp1 = conj_log->get_type();
      Type exp2 = disy_log->get_type();

      if (exp1.is_undef() || exp2.is_undef())
	return Type::make_undef();
    
      if (exp1.is_putoff() || exp2.is_putoff()){
	exp1.set_putoff();
	return exp1;
      }
      
      if (ConversionManager::is_legal_expr(exp1,exp2,op))
	{
	  return Type::make_logic();
	}
      else
	{
	  error_flag = true;
	  ErrorManager::bexpression_not_legal(lineno,to_string(),exp1,op,exp2);
	  return Type::make_undef();
	}
    }

    bool error(){ return error_flag || conj_log->error() || disy_log->error(); }
  
    string to_string(){ return conj_log->to_string()+" "+op+" "+disy_log->to_string(); }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS  , ATTR_OPER};
      XMLOutputManager::AttributesValues values = {CLASS_BINARY, op};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      conj_log->to_XML(os);
      disy_log->to_XML(os);
      get_type().to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo disyunción lógica. Representa la operación lógica binaria or. */
  class NodeDisyLog : public NodeExp{
    NodeExp *disy_log, *in;
    string op;
  public:
    NodeDisyLog(int lno, NodeExp *n, NodeExp *m) :
      NodeExp(lno), disy_log(n), in(m), op(OPER_CONJ)
    {  }

    void step_1() {
      disy_log->step_1();
      in->step_1();
    }

    void step_2() {
      if (disy_log->get_count() == 1 && in->get_count() == 1){
	disy_log->step_2();
	in->step_2();
      }
      else{
	error_flag = true;
	ErrorManager::boperator_doesnt_match(lineno,to_string(),op);
      }
    }

    void step_4() {
      disy_log->step_4();
      in->step_4();
    }
  
    void push() {
      DependenciesManager::push_expression(this);
    }

    Type get_type(){
      Type exp1 = disy_log->get_type();
      Type exp2 = in->get_type();
    
      if (exp1.is_undef() || exp2.is_undef())
	return Type::make_undef();
    
      if (exp1.is_putoff() || exp2.is_putoff()){
	exp1.set_putoff();
	return exp1;
      }

      if (ConversionManager::is_legal_expr(exp1,exp2,op))
	{
	  return Type::make_logic();
	}
      else
	{
	  error_flag = true;
	  ErrorManager::bexpression_not_legal(lineno,to_string(),exp1,op,exp2);
	  return Type::make_undef();
	}
    }

    bool error(){ return error_flag || disy_log->error() || in->error(); }
  
    string to_string(){ return disy_log->to_string()+" "+op+" "+in->to_string(); }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS  , ATTR_OPER};
      XMLOutputManager::AttributesValues values = {CLASS_BINARY, op};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      disy_log->to_XML(os);
      in->to_XML(os);
      get_type().to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo en. Representa la expresión binaria de inclusión en. */
  class NodeIn : public NodeExp{
    NodeExp *in, *relational;
    string op;
  public:
    NodeIn(int lno, NodeExp *n, NodeExp *m) :
      NodeExp(lno), in(n), relational(m), op(OPER_IN)
    {  }

    void step_1() {
      in->step_1();
      relational->step_1();
    }

    void step_2() {
      if (in->get_count() == 1 && relational->get_count()){
	in->step_2();
	relational->step_2();
      }
      else{
	error_flag = true;
	ErrorManager::boperator_doesnt_match(lineno,to_string(),op);
      }
    }

    void step_4() {
      in->step_4();
      relational->step_4();
    }

    void push() {
      DependenciesManager::push_expression(this);
    }
  
    Type get_type(){
      Type exp1 = in->get_type();
      Type exp2 = relational->get_type();
    
      if (exp1.is_undef() || exp2.is_undef())
	return Type::make_undef();
    
      if (exp1.is_putoff() || exp2.is_putoff()){
	exp1.set_putoff();
	return exp1;
      }

      if (ConversionManager::is_legal_in(exp1,exp2))
	{
	  return Type::make_logic();
	}
      else
	{
	  error_flag = true;
	  ErrorManager::bexpression_not_legal(lineno,to_string(),exp1,op,exp2);
	  return Type::make_undef();
	}
    }

    bool error(){
      return error_flag || in->error() || relational->error();
    }
  
    string to_string(){ return in->to_string()+" "+op+" "+relational->to_string(); }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS  ,ATTR_OPER};
      XMLOutputManager::AttributesValues values = {CLASS_BINARY,op};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      in->to_XML(os);
      relational->to_XML(os);
      get_type().to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo relacional2. Representa las operaciones binarias <, <=, >, >=, contenido en,
   *  contenido estricto en, contiene, contiene estricto. */
  class NodeRelational : public NodeExp{
    NodeExp *relational, *add;
    string op;
  public:
    NodeRelational(int lno, NodeExp *n, const string& o, NodeExp *m) :
      NodeExp(lno), relational(n), add(m), op(o)
    {  }

    void step_1() {
      relational->step_1();
      add->step_1();
    }

    void step_2() {
      if (relational->get_count() == 1 && add->get_count() == 1){
	relational->step_2();
	add->step_2();
      }
      else{
	error_flag = true;
	ErrorManager::boperator_doesnt_match(lineno,to_string(),op);
      }
    }

    void step_4() {
      relational->step_4();
      add->step_4();
    }

    void push() {
      DependenciesManager::push_expression(this);
    }

    Type get_type(){
      Type exp1 = relational->get_type();
      Type exp2 = add->get_type();
    
      if (exp1.is_undef() || exp2.is_undef())
	return Type::make_undef();
    
      if (exp1.is_putoff() || exp2.is_putoff()){
	exp1.set_putoff();
	return exp1;
      }

      if (ConversionManager::is_legal_expr(exp1,exp2,op))
	{
	  return Type::make_logic();
	}
      else
	{
	  error_flag = true;
	  ErrorManager::bexpression_not_legal(lineno,to_string(),exp1,op,exp2);
	  return Type::make_undef();
	}
    }

    bool error(){ return error_flag || relational->error() || add->error(); }
  
    string to_string(){ return relational->to_string()+" "+op+" "+add->to_string(); }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS  , ATTR_OPER};
      XMLOutputManager::AttributesValues values = {CLASS_BINARY, op};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      relational->to_XML(os);
      add->to_XML(os);
      get_type().to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo aditivo. Representa las operaciones binarias +, -, union. */
  class NodeAdd : public NodeExp{
    NodeExp *add, *mult;
    string op;
  public:
    NodeAdd(int lno, NodeExp *n, const string& o, NodeExp *m) :
      NodeExp(lno), add(n), mult(m), op(o)
    {  }

    void step_1() {
      add->step_1();
      mult->step_1();
    }

    void step_2() {
      if (add->get_count() == 1 && mult->get_count() == 1){
	add->step_2();
	mult->step_2();
      }
      else{
	error_flag = true;
	ErrorManager::boperator_doesnt_match(lineno,to_string(),op);
      }
    }

    void step_4() {
      add->step_4();
      mult->step_4();
    }
  
    void push() {
      DependenciesManager::push_expression(this);
    }

    Type get_type(){
      Type exp1 = add->get_type();
      Type exp2 = mult->get_type();

      if (exp1.is_undef() || exp2.is_undef())
	return Type::make_undef();
    
      if (exp1.is_putoff() || exp2.is_putoff()){
	exp1.set_putoff();
	return exp1;
      }
      if (ConversionManager::is_legal_expr(exp1,exp2,op))
	{
	  return ConversionManager::solve(exp1,exp2);
	}
      else
	{
	  error_flag = true;
	  ErrorManager::bexpression_not_legal(lineno,to_string(),exp1,op,exp2);
	  return Type::make_undef();
	}
    }

    bool error(){ return error_flag || add->error() || mult->error(); }
  
    string to_string(){ return add->to_string()+" "+op+" "+mult->to_string(); }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS  , ATTR_OPER};
      XMLOutputManager::AttributesValues values = {CLASS_BINARY, op};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      add->to_XML(os);
      mult->to_XML(os);
      get_type().to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo multiplicativo. Representa las operaciones binarias *, /, div, %, intersección. */
  class NodeMult : public NodeExp{
    NodeExp *mult, *unary;
    string op;
  public:
    NodeMult(int lno, NodeExp *n, const string& o, NodeExp *m) :
      NodeExp(lno), mult(n), unary(m), op(o)
    {  }

    void step_1() {
      mult->step_1();
      unary->step_1();
    }

    void step_2() {
      if (mult->get_count() == 1 && unary->get_count()){
	mult->step_2();
	unary->step_2();
      }
      else{
	error_flag = true;
	ErrorManager::boperator_doesnt_match(lineno,to_string(),op);
      }
    }

    void step_4() {
      mult->step_4();
      unary->step_4();
    }

    void push() {
      DependenciesManager::push_expression(this);
    }

    Type get_type(){
      Type exp1 = mult->get_type();
      Type exp2 = unary->get_type();

      if (exp1.is_undef() || exp2.is_undef())
	return Type::make_undef();
    
      if (exp1.is_putoff() || exp2.is_putoff()){
	exp1.set_putoff();
	return exp1;
      }

      if (ConversionManager::is_legal_expr(exp1,exp2,op))
	{
	  return ConversionManager::solve(exp1,exp2);
	}
      else
	{
	  error_flag = true;
	  ErrorManager::bexpression_not_legal(lineno,to_string(),exp1,op,exp2);
	  return Type::make_undef();
	}
    }

    bool error(){ return error_flag || mult->error() || unary->error(); }
  
    string to_string(){ return mult->to_string()+" "+op+" "+unary->to_string(); }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS  , ATTR_OPER};
      XMLOutputManager::AttributesValues values = {CLASS_BINARY, op};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      mult->to_XML(os);
      unary->to_XML(os);
      get_type().to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo unario. Representa las operaciones unarias -, +, not. */
  class NodeUnary : public NodeExp{
    string op;
    NodeExp *delimiter;
  public:
    NodeUnary(int lno, const string& o, NodeExp *n) :
      NodeExp(lno), op(o), delimiter(n)
    {  }

    void step_1() {
      delimiter->step_1();
    }

    void step_2() {
      if (delimiter->get_count() == 1){
	delimiter->step_2();
      }
      else{
	error_flag = true;
	ErrorManager::uoperator_doesnt_match(lineno,to_string(),op);
      }
    }

    void step_4() {
      delimiter->step_4();
    }

    void push() {
      DependenciesManager::push_expression(this);
    }

    Type get_type(){
      Type exp = delimiter->get_type();
    
      if (exp.is_undef())
	return Type::make_undef();
    
      if (exp.is_putoff()){
	return exp;
      }
    
      if (ConversionManager::is_legal_unary(op,exp))
	{
	  return exp;
	}
      else
	{
	  error_flag = true;
	  ErrorManager::uexpression_not_legal(lineno,to_string(),op,exp);
	  return Type::make_undef();
	}
    }

    bool error(){ return error_flag || delimiter->error(); }
  
    string to_string(){ return op+delimiter->to_string(); }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS , ATTR_OPER};
      XMLOutputManager::AttributesValues values = {CLASS_UNARY, op};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      delimiter->to_XML(os);
      get_type().to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo delimitador. Representa el operador paréntesis. */
  class NodeDelimiter : public NodeExp{
    NodeExp *expression;
  public:
    NodeDelimiter(int lno, NodeExp *n) :
      NodeExp(lno), expression(n)
    {  }

    void step_1() {
      expression->step_1();
    }

    void step_2() {
      if (expression->get_count() == 1){
	expression->step_2();
      }
      else{
	error_flag = true;
	ErrorManager::delimiter_doesnt_match(lineno,to_string());
      }
    }

    void step_4() {
      expression->step_4();
    }
  
    void push() {
      expression->push();
    }

    Type get_type(){
      return  expression->get_type();
    }

    bool error(){ return expression->error(); }
  
    string to_string(){
      return "("+expression->to_string()+")";
    }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS};
      XMLOutputManager::AttributesValues values = {CLASS_PAREN};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      expression->to_XML(os);
      get_type().to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo delimitador de llaves. Representa las expresiones conjuntos inmediatos. */
  class NodeBracerDelimiter : public NodeExp{
    NodeExp* expressions;
  public:
    NodeBracerDelimiter(int lno, NodeExp *n) :
      NodeExp(lno), expressions(n)
    { }

    void step_1(){
      expressions->step_1();
    }

    void step_2(){
      expressions->step_2();
    }

    void step_4(){
      expressions->step_4();
    }

    void push(){
      DependenciesManager::push_expression(this);
    }

    Type get_type(){
      Types types = expressions->get_types();
      for (auto& it : types)
	if (it.is_putoff())
	  return Type::make_null_putoff();
    
      Type t = DependenciesManager::reduce_type(types);
      if (t.is_undef()){
	error_flag = true;
	ErrorManager::immediate_set_not_legal(lineno,to_string(),types);
	return t;
      }
      else
	return Type::make_set(t);
    }

    bool error(){ return error_flag || expressions->error(); }

    string to_string(){
      return "{"+expressions->to_string()+"}";
    }

    void to_XML(Stream& os){
      Type type = get_type();
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS};
      XMLOutputManager::AttributesValues values = {CLASS_SET};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      type.to_XML(os);
      expressions->to_XML(os);
      get_type().to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo array inmediato. Representa la expresión de array inmediato. */
  class NodeImmediateArray : public NodeExp{
    NodeExp *expressions;
  public:
    NodeImmediateArray(int lno, NodeExp* e) :
      NodeExp(lno), expressions(e)
    { }

    void step_1() {
      expressions->step_1();
    }

    void step_2() {
      expressions->step_2();
    }

    void step_4() {
      expressions->step_4();
    }

    void push() {
      DependenciesManager::push_expression(this);
    }

    Type get_type(){
      Types types = expressions->get_types();
      if (!expressions->error()){
	for (auto& it : types)
	  if (it.is_putoff()){
	    return Type::make_null_putoff();
	  }
	Type t = DependenciesManager::reduce_type(types);
	if (!t.is_undef()){
	  if (t.is_array()){
	    t.set_dimension(t.get_dimension() + 1);
	    return t;
	  }
	  else{
	    return Type::make_array(t,1);
	  }
	}
	else{
	  error_flag = true;
	  ErrorManager::immediate_array_not_legal(lineno,to_string(),types);
	}
      }
      return Type::make_undef();
    }
    
    Types get_types(){
      return Types(1,get_type());
    }

    bool error() { return error_flag || expressions->error(); }

    string to_string(){ return '[' + expressions->to_string() + ']'; }

    void to_XML(Stream& os){
      Type type = get_type();
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS};
      XMLOutputManager::AttributesValues values = {CLASS_ARRAY};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      type.to_XML(os);
      expressions->to_XML(os);
      get_type().to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /************* EXPRESIONES FINALES *************/
  /** Nodo expresion de lectura. Representa las variables de lectura de las expresiones. */
  class NodeReadableExpression : public NodeExp{
    string ID;
    NodeExp *access;
    SymbolsIterator refVar;
  public:
    NodeReadableExpression(int lno, const string& id, NodeExp *n) :
      NodeExp(lno), ID(id), access(n), refVar(nullptr)
    {  }

    void step_1() {
      Algorithm& algorithm = Algorithms::get_current_algorithm();
      if (algorithm.check_variable(ID)){
	refVar = algorithm.get_variable_reference(ID);
	if (access)
	  access->step_1();
      }
      else{
	error_flag = true;
	ErrorManager::variable_not_found(lineno,to_string(),ID);
      }
    }

    void step_2() {
      if (access)
	access->step_2();
    }

    void step_4() {
      if (access)
	access->step_4();
    }

    void push() {
      DependenciesManager::push_expression(this);
    }

    Type get_type(){
      Type var = refVar->second.type;
      if(var.is_tmpexp())
	DependenciesManager::solve_variable_dependencies(refVar);
      if (refVar->second.type.is_tmpexp()){
	return Type::make_null_putoff();
      }
      else if (refVar->second.type.is_ready()){
	var = refVar->second.type;
	if (access && var.is_array()){
	  //reducir dimension por cada acceso realizado
	  int nAccess = access->get_n_access();
	  int dim = var.get_dimension();
	  if ((dim - nAccess) > 0)
	    var.set_dimension(dim-nAccess);
	  else
	    var = var.get_sub_type();
	}
      }
    
      return var;
    }

    unsigned int get_n_access(){
      if (access)
	return access->get_n_access();
      else
	return 0;
    }

    bool error(){
      if (access)
	return error_flag || access->error();
      else
	return error_flag;
    }
  
    string to_string(){
      if (access)
	return ID + access->to_string();
      else
	return ID;
    }

    void to_XML(Stream& os){
      string codename = refVar->second.codeName;
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS, ATTR_ID};
      XMLOutputManager::AttributesValues values = {CLASS_VAR , codename};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      if (access)
	access->to_XML(os);
      refVar->second.type.to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo llamada a funcion auxiliar. Clase auxiliar para tratar con las dependencias de 
   * algoritmos definidos por el usuario y algoritmos estándar. */
  class NodeFunctionCallAux : public NodeExp{
    AlgorithmBase& algorithm;
    int iout;
  public:
    NodeFunctionCallAux(int lno, AlgorithmBase& alg, int i) :
      NodeExp(lno), algorithm(alg), iout(i)
    {}

    void step_1(){}
    void step_2(){}
    void step_4(){}

    Type get_type(){
      algorithm.solve_output_dependencies();
      Types output = algorithm.get_output_types();
      return output[iout];
    }

    string to_string(){
      return "SOY UN ERROR NodeFunctionCallAux: "+algorithm.get_name();
    }

    void to_XML(Stream& os){ }
  };

  /** Nodo llamada a función. Representa las expresiones llamadas a función. */
  class NodeFunctionCall : public NodeExp{
    string ID;
    NodeExp *tuple;
    Types exps;
    int exp_counter;
    vector<NodeExp*> outputTypes;
    AlgorithmBase *algorithm;
  public:
    NodeFunctionCall(int lno, const string& id, NodeExp *n) :
      NodeExp(lno), ID(id), tuple(n), exps(), exp_counter(0), outputTypes(), algorithm(nullptr)
    {  }

    void step_1() {
      if (!Algorithms::check_algorithm(ID)){
	error_flag = true;
	ErrorManager::algorithm_not_found(lineno,to_string(),ID);
      }
      else{
	if (Algorithms::is_std(ID)){
	  algorithm = StdAlgorithm::make_std(ID);
	}
	else{
	  algorithm = &Algorithms::get_algorithm(ID);
	  if (Algorithms::get_algorithm(ID).is_main()){
	    error_flag = true;
	    ErrorManager::main_not_call(lineno,to_string());
	  }
	}
	tuple->step_1();
      }
    }

    void step_2() {
      if (!error_flag){
	unsigned int n_input_args = algorithm->get_n_input();
	if (tuple->get_count() == n_input_args){
	  algorithm->set_input_dependencies(tuple->get_references());
	}
	else{
	  error_flag = true;
	  ErrorManager::function_call_args_doesnt_match(lineno,to_string(),ID,n_input_args);
	}
      }
      tuple->step_2();
    }

    void step_4() {
      tuple->step_4();
      if(algorithm->is_std()){
	algorithm->solve_dependencies();
	StdAlgorithm* std = dynamic_cast<StdAlgorithm*>(algorithm);
	if(!std->check_semantic())
	  std->launch_error(to_string());
      }
    }

    void push() {
      DependenciesManager::push_algorithm(*algorithm);
    }

    Type get_type(){
      if (algorithm->get_n_output() == 1){
	algorithm->solve_output_dependencies();
	return algorithm->get_output_types()[0];
      }
      else
	return Type::make_undef();
    }

    Types get_types(){
      algorithm->solve_output_dependencies();
      return algorithm->get_output_types();
    }

    unsigned int get_count(){ return algorithm->get_n_output(); }

    bool error(){ return error_flag || tuple->error(); }

    string to_string(){ return ID+tuple->to_string(); }

    void to_XML(Stream& os){
      string codeName;
      if (algorithm->is_std())
	codeName = algorithm->get_name();
      else
	codeName = algorithm->get_code_name();
    
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS, ATTR_ID};
      XMLOutputManager::AttributesValues values = {CLASS_CALL, codeName};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      tuple->to_XML(os);
      XMLOutputManager::open_tag(os,XMLTAG_OUTPUT_ARGS);
      XMLOutputManager::AttributesNames  argNames  = {ATTR_ID};
      for (auto& it : algorithm->get_output_references()){
	XMLOutputManager::AttributesValues argValues = {it->second.codeName};
	XMLOutputManager::open_tag(os,XMLTAG_ARG,argNames,argValues);
	it->second.type.to_XML(os);
	XMLOutputManager::close_tag(os);
      }
      XMLOutputManager::close_tag(os);
      get_type().to_XML(os);
      XMLOutputManager::close_tag(os);
    }

    ~NodeFunctionCall(){
      if (algorithm && algorithm->is_std())
	delete algorithm;
      algorithm = nullptr;
    }
  };

  /** Nodo tupla. Representa tuplas de expresiones. */
  class NodeTuple : public NodeExp{
    NodeExp *expressions;
  public:
    NodeTuple(int lno, NodeExp *n) : NodeExp(lno), expressions(n)
    {  }

    void step_1() {
      expressions->step_1();
    }

    void step_2() {
      expressions->step_2();
    }

    void step_4() {
      expressions->step_4();
    }
  
    void push() {
      expressions->push();
    }

    vector<NodeExp*> get_references(){
      return expressions->get_references();
    }

    unsigned int get_count(){ return expressions->get_count(); }

    Types get_types(){ return expressions->get_types(); }

    bool error(){ return expressions->error(); }
  
    string to_string(){ return "("+expressions->to_string()+")"; }

    void to_XML(Stream& os){
      XMLOutputManager::open_tag(os,XMLTAG_TUPLE_EXPS);
      expressions->to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /************* LITERALES *************/
  /** Nodo lógico literal. Representa las constantes literales de tipo lógico. */
  class NodeLiteralLog : public NodeExp{
    string value;
  public:
    NodeLiteralLog(int lno, const string& s) :
      NodeExp(lno), value(s)
    {  }

    void step_1() {}
    void step_2() {}
    void step_4() {}

    void push() {
      DependenciesManager::push_expression(this);
    }

    Type get_type(){
      return Type::make_logic();
    }
  
    string to_string(){ return value; }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS   , ATTR_VALUE};
      XMLOutputManager::AttributesValues values = {CLASS_LITERAL, value};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      Type::make_logic().to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo entero literal. Representa las constantes literales de tipo entero. */
  class NodeLiteralInt : public NodeExp{
    int value;
  
  public:
    NodeLiteralInt(int lno, int v) :
      NodeExp(lno), value(v)
    {  }

    void step_1() {}
    void step_2() {}
    void step_4() {}
  
    void push() {
      DependenciesManager::push_expression(this);
    }
  
    Type get_type(){
      return Type::make_integer();
    }
  
    string to_string(){ return std::to_string(value); }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS   , ATTR_VALUE};
      XMLOutputManager::AttributesValues values = {CLASS_LITERAL, std::to_string(value)};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      Type::make_integer().to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo real literal. Representa las constantes literales de tipo real. */
  class NodeLiteralFloat : public NodeExp{
    float value;
  
  public:
    NodeLiteralFloat(int lno, float v) :
      NodeExp(lno), value(v)
    {  }

    void step_1() {}
    void step_2() {}
    void step_4() {}
  
    void push() {
      DependenciesManager::push_expression(this);
    }

    Type get_type(){
      return Type::make_float();
    }
  
    string to_string(){ return std::to_string(value); }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS   , ATTR_VALUE};
      XMLOutputManager::AttributesValues values = {CLASS_LITERAL, std::to_string(value)};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      Type::make_float().to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo caracter literal. Representa las constantes literales de tipo caracter. */
  class NodeLiteralChar : public NodeExp{
    char value;
  public:
    NodeLiteralChar(int lno, char v) :
      NodeExp(lno), value(v)
    {  }

    void step_1() {}
    void step_2() {}
    void step_4() {}

    void push() {
      DependenciesManager::push_expression(this);
    }

    Type get_type(){
      return Type::make_character();
    }
  
    string to_string(){
      char v[2] = {value,'\0'};
      return v;
    }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS   , ATTR_VALUE};
      XMLOutputManager::AttributesValues values = {CLASS_LITERAL, to_string()};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      Type::make_character().to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo cadena literal. Representa las constantes literales de tipo cadena. */
  class NodeLiteralString : public NodeExp{
    string value;
  public:
    NodeLiteralString(int lno, const string& s) :
      NodeExp(lno), value(s)
    {  }

    void step_1() {}
    void step_2() {}
    void step_4() {}
  
    void push() {
      DependenciesManager::push_expression(this);
    }

    Type get_type(){
      return Type::make_string();
    }
  
    string to_string(){ return value; }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS   , ATTR_VALUE};
      XMLOutputManager::AttributesValues values = {CLASS_LITERAL, value};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values);
      Type::make_string().to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo conjunto vacío literal. Representa las constantes literales de tipo conjunto vacío. */
  class NodeLiteralEmptySet : public NodeExp{
  public:
    NodeLiteralEmptySet(int lno) : NodeExp(lno) {}
  
    void step_1() {}  
    void step_2() {}
    void step_4() {}

    void push() {
      DependenciesManager::push_expression(this);
    }

    Type get_type(){
      return Type::make_empty_set();
    }

    string to_string() { return "vacio"; }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames  names  = {ATTR_CLASS};
      XMLOutputManager::AttributesValues values = {CLASS_EMPTY_SET};
      XMLOutputManager::open_tag(os,XMLTAG_EXPRESSION,names,values,true);
    }
  };

} // namespace LEAC
  
#endif
