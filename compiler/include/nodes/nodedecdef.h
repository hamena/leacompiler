#ifndef NODEDECDEF_H_
#define NODEDECDEF_H_

//#define NODEDECDEF_DEBUG 1

#ifndef _GLIBCXX_USE_CXX11_ABI
#define _GLIBCXX_USE_CXX11_ABI 0
#endif
#include <sstream>
#include <vector>
#include "node.h"
#include "../algorithms.h"
#include "../type.h"
#include "../managers/errormanager.h"
#include "../managers/xmloutputmanager.h"
#include "../defstrings.h"
#include "nodeexpressions.h"

using namespace std;

namespace LEAC{

  /************* DEFINICION *************/
  /** Nodo algoritmo. Nodo del AST que representa la definición de un algoritmo. */
  class NodeAlgorithmDefinition : public Node{
    Node *nodeHeader, *nodeCodeblock;
  public:
    NodeAlgorithmDefinition(int lno, Node* h, Node* cb) :
      Node(lno), nodeHeader(h), nodeCodeblock(cb)
    {  }
  
    void step_1() {
      if (!nodeHeader->error()){
	nodeHeader->step_1();
	nodeCodeblock->step_1();
      }
      Algorithms::next_algorithm();
    }

    void step_2(){
      if (!nodeHeader->error()){
	nodeHeader->step_2();
	nodeCodeblock->step_2();
      }
      Algorithms::next_algorithm();
    }

    void step_4(){
      if (!nodeHeader->error()){
	nodeHeader->step_4();
	nodeCodeblock->step_4();
      }
      Algorithms::next_algorithm();
    }

    bool error() { return error_flag || nodeHeader->error() || nodeCodeblock->error(); }
  
    string to_string(){ return nodeHeader->to_string()+"\n"+nodeCodeblock->to_string()+"\n"; }

    void to_XML(Stream& os){
      XMLOutputManager::open_tag(os,XMLTAG_ALG);
      nodeHeader->to_XML(os);
      nodeCodeblock->to_XML(os);
      XMLOutputManager::close_tag(os);
      Algorithms::next_algorithm();
    }
  };
  /************* CABECERA *************/
  /** Nodo cabecera. Representa la cabecera de la definición del algoritmo. */
  class NodeHeader : public Node{
    string ID;
    Node *inputArgs, *outputArgs;
  public:
    NodeHeader(int lno, const string& id, Node* n, Node* m) :
      Node(lno), ID(id), inputArgs(n), outputArgs(m)
    {
      if (!Algorithms::check_algorithm(ID))
	Algorithms::define_algorithm(ID);
      else{
	error_flag = true;
	ErrorManager::algorithm_already_defined(lineno,to_string(),ID);
      }
    }
  
  
    void step_1() {
      if (!error_flag){
	Algorithms::select_algorithm(ID);
	Algorithms::get_current_algorithm().first_scope();
	inputArgs->step_1();
	outputArgs->step_1();
      }
    }

    void step_2(){
      if (!error_flag){
	Algorithms::select_algorithm(ID);
	Algorithms::get_current_algorithm().first_scope();
	inputArgs->step_2();
	outputArgs->step_2();
      }
    }

    void step_4(){
      if (!error_flag){
	Algorithms::select_algorithm(ID);
	Algorithms::get_current_algorithm().first_scope();
	inputArgs->step_4();
	outputArgs->step_4();
      }
    }

    bool error() { return error_flag || inputArgs->error() || outputArgs->error(); }
  
    string to_string(){
      return ID+" ("+inputArgs->to_string()+") --> ("+outputArgs->to_string()+")";
    }

    void to_XML(Stream& os){
      Algorithms::select_algorithm(ID);
      Algorithm& algorithm = Algorithms::get_current_algorithm();
      algorithm.first_scope();
      XMLOutputManager::AttributesNames  names  = {ATTR_ID};
      XMLOutputManager::AttributesValues values = {algorithm.get_code_name()};
      XMLOutputManager::open_tag(os,XMLTAG_HEADER,names,values);
      XMLOutputManager::open_tag(os,XMLTAG_INPUT_ARGS);
      inputArgs->to_XML(os);
      XMLOutputManager::close_tag(os);
      XMLOutputManager::open_tag(os,XMLTAG_OUTPUT_ARGS);
      outputArgs->to_XML(os);
      XMLOutputManager::close_tag(os);
      XMLOutputManager::close_tag(os);
    } 
  };

  /** Nodo argumentos de entrada. Representa la lista de argumentos de entrada de la cabecera. */
  class NodeInputArgs : public Node{
    string ID;
    Node *inputargs;
  public:
    NodeInputArgs(int lno, const string& id, Node* m) :
      Node(lno), ID(id), inputargs(m){}
  
    void step_1() {
      Algorithm& algorithm = Algorithms::get_current_algorithm();
      if (!algorithm.is_main()){
	if (!algorithm.is_arg(ID))
	  algorithm.new_input_argument(ID);
	else{
	  error_flag = true;
	  ErrorManager::argument_conflict(lineno,to_string(),ID);
	}
      }
      else{
	error_flag = true;
	ErrorManager::main_not_input_argument(lineno,ID,ID);
      }
      inputargs->step_1();
    }

    void step_2(){
      if (!error_flag)
	inputargs->step_2();
    }

    void step_4(){
      if (!error_flag)
	inputargs->step_4();
    }

    bool error() { return error_flag || inputargs->error(); }
  
    string to_string(){ return ID + ", " + inputargs->to_string(); }

    void to_XML(Stream& os){
      Variable var = Algorithms::get_current_algorithm().get_variable(ID);
      XMLOutputManager::AttributesNames  names  = {ATTR_ID};
      XMLOutputManager::AttributesValues values = {var.codeName};
      XMLOutputManager::open_tag(os,XMLTAG_ARG,names,values);
      var.type.to_XML(os);
      XMLOutputManager::close_tag(os);
      inputargs->to_XML(os);
    }
  };

  /** Nodo argumentos de salida. Representa la lista de argumentos de salida de la cabecera. */
  class NodeOutputArgs : public Node{
    string ID;
    Node *outputargs;
  public:
    NodeOutputArgs(int lno, const string& id, Node* m) :
      Node(lno), ID(id), outputargs(m){}
  
    void step_1() {
      Algorithm& algorithm = Algorithms::get_current_algorithm();
      if (!algorithm.is_main()){
	if (!algorithm.is_arg(ID))
	  algorithm.new_output_argument(ID);
	else{
	  error_flag = true;
	  ErrorManager::argument_conflict(lineno,to_string(),ID);
	}
      }
      else{
	error_flag = true;
	ErrorManager::main_not_output_argument(lineno,ID,ID);
      }
      outputargs->step_1();
    }

    void step_2(){
      if (!error_flag)
	outputargs->step_2();
    }

    void step_4(){
      if (!error_flag)
	outputargs->step_4();
    }

    bool error() { return error_flag || outputargs->error(); }
  
    string to_string(){ return ID + ", " + outputargs->to_string(); }
  
    void to_XML(Stream& os){
      Variable var = Algorithms::get_current_algorithm().get_variable(ID);
      XMLOutputManager::AttributesNames  names  = {ATTR_ID};
      XMLOutputManager::AttributesValues values = {var.codeName};
      XMLOutputManager::open_tag(os,XMLTAG_ARG,names,values);
      var.type.to_XML(os);
      XMLOutputManager::close_tag(os);
      outputargs->to_XML(os);
    }
  };

  /************* BLOQUE DE CODIGO *************/
  /** Nodo bloque de código. Representa un bloque de código LEA. */
  class NodeCodeblock : public Node{
    Node *instructions;
  public:
    NodeCodeblock(int lno, Node* n) :
      Node(lno), instructions(n){}
  
    void step_1() { 
      instructions->step_1();
    }

    void step_2(){
      instructions->step_2();
    }

    void step_4(){
      instructions->step_4();
    }

    bool error() { return instructions->error(); }
  
    string to_string(){ return "\n"+instructions->to_string()+"\n"; }

    void to_XML(Stream& os){
      XMLOutputManager::open_tag(os,XMLTAG_INSTRS);
      instructions->to_XML(os);
      XMLOutputManager::close_tag(os);
    }
  };

  /** Nodo instrucciones. Representa una lista de instrucciones LEA. */
  class NodeInstructions : public Node{
    Node *instruction, *instructions;
  public:
    NodeInstructions(int lno, Node* n, Node* m) :
      Node(lno), instruction(n), instructions(m){}
  
    void step_1() { 
      instruction->step_1();
      instructions->step_1();
    }

    void step_2() {
      if (!instruction->error())
	instruction->step_2();
      instructions->step_2();
    }

    void step_4(){
      if (!instruction->error())
	instruction->step_4();
      instructions->step_4();
    }

    bool error() { return instruction->error() || instructions->error(); }
  
    string to_string(){
      return instruction->to_string() + "\n" + instructions->to_string();
    }

    void to_XML(Stream& os) {
      instruction->to_XML(os);
      instructions->to_XML(os);
    }
  };

  /** Nodo instrucción. Representa una instrucción de LEA. */
  class NodeInstruction : public Node{
    Node *instruction;
  public:
    NodeInstruction(int lno, Node* n) :
      Node(lno), instruction(n) {}
  
    void step_1() { 
      instruction->step_1();
    }

    void step_2() {
      if (!instruction->error())
	instruction->step_2();
    }

    void step_4(){
      if (!instruction->error())
	instruction->step_4();
    }

    bool error(){ return instruction->error(); }
  
    string to_string(){ return instruction->to_string(); }
  
    void to_XML(Stream& os){
      instruction->to_XML(os);
    }
  };

  /************* DEFINICION DE VARIABLE *************/
  /** Nodo definición de variable. Representa la instrucción definición de variable. */
  class NodeVariableDefinition : public Node{
    vector<string*> vids;
    Type* type;
  public:
    NodeVariableDefinition(int lno, const vector<string*>& v, Type* t) :
      Node(lno), vids(v), type(t)
    {  }
  
    void step_1() {
      Algorithm& algorithm = Algorithms::get_current_algorithm();
      for (auto& it : vids){
	if (!algorithm.check_variable(*it)){
	  Type t = *type;
	  algorithm.new_variable(*it,t);
	}
	else{
	  error_flag = true;
	  ErrorManager::variable_already_exists(lineno,to_string(),*it);
	}
      }
    }

    void step_2() {
    
    }

    void step_4(){
    
    }
  
    string to_string(){
      string aux = *vids[0];
      for (unsigned i=1; i<vids.size(); ++i) aux += "," + *vids[i];
      return aux + ":" + type->get_type_name();
    }

    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames iNames = {ATTR_CLASS};
      XMLOutputManager::AttributesValues iValues = {INSTR_VAR_DEC};
      XMLOutputManager::open_tag(os,XMLTAG_INSTRUCTION,iNames,iValues);
      type->to_XML(os);
      for (unsigned int i=0; i<vids.size(); ++i){
	string codename = Algorithms::get_current_algorithm().get_variable(*(vids[i])).codeName;
	XMLOutputManager::AttributesNames  names  = {ATTR_ID};
	XMLOutputManager::AttributesValues values = {codename};
	XMLOutputManager::open_tag(os,XMLTAG_NEW_VAR,names,values,true);
      }
      XMLOutputManager::close_tag(os);
    }
  
  };

  /** Nodo definición de array. Representa la instrucción definición de array. */
  class NodeArrayDefinition : public Node{
    vector<string*> vids;
    Type* type;
    NodeExp *dimensions;
  public:
    NodeArrayDefinition(int lno, const vector<string*>& v, Type* t, NodeExp* d) :
      Node(lno), vids(v), type(t), dimensions(d)
    {  }
  
    void step_1() {
      Algorithm& algorithm = Algorithms::get_current_algorithm();
      for (auto& it : vids){
	if (!algorithm.check_variable(*it)){
	  int dim = dimensions->get_n_access();
	  Type t = Type::make_array(*type,dim);
	  algorithm.new_variable(*it,t);
	}else{
	  error_flag = true;
	  ErrorManager::variable_already_exists(lineno,to_string(),*it);
	}
      }

      dimensions->step_1();
    }

    void step_2() {
      dimensions->step_2();
    }

    void step_4(){
      dimensions->step_4();
    }

    bool error() { return error_flag || dimensions->error(); }
  
    string to_string(){
      string aux = *vids[0];
      for (unsigned i=1; i<vids.size(); ++i) aux += "," + *vids[i];
      return aux + ":" + type->get_type_name() + dimensions->to_string();
    }
  
    void to_XML(Stream& os){
      XMLOutputManager::AttributesNames iNames = {ATTR_CLASS};
      XMLOutputManager::AttributesValues iValues = {INSTR_ARRAY_DEC};
      XMLOutputManager::open_tag(os,XMLTAG_INSTRUCTION,iNames,iValues);
      type->to_XML(os);
      dimensions->to_XML(os);
      for (unsigned int i=0; i<vids.size(); ++i){
	string codename = Algorithms::get_current_algorithm().get_variable(*(vids[i])).codeName;
	XMLOutputManager::AttributesNames  names  = {ATTR_ID};
	XMLOutputManager::AttributesValues values = {codename};
	XMLOutputManager::open_tag(os,XMLTAG_NEW_ARRAY,names,values,true);
      }
      XMLOutputManager::close_tag(os);
    }
  };

} // namespace LEAC
  
#endif
