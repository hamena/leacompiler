#ifndef __leac_driver_HPP__
#define __leac_driver_HPP__ 1

#include <string>
#include <vector>
#include <list>
#include <string>

using std::string;

// TEMPORAL
class LEACContext{

};

/** Espacio de nombres del compilador. */
namespace LEAC {

  struct Node;
  
  /** Clase driver. Une el scanner y el parser proporcionando
   * vías de comuncación personalizadas.
   */  
  class Driver
  {
  public:
    /// Construye un nuevo driver en un contexto de LEA.
    Driver(class LEACContext& leac);
    /// Bandera que permite la salida debug del Scanner.
    bool trace_scanning;
    /// Bandera que permite la salida debug del Parser.
    bool trace_parsing;
    /// Nombre del flujo.
    std::string streamname;

    /** LLama al Scanner y al Parser con un flujo de datos. */
    bool parse_stream(std::istream&, const std::string& sname = "stream input");
    /** LLama al Scanner y al Parser con una cadena de caracteres. */
    bool parse_string(const std::string&, const std::string& sname = "string stream");
    /** Llama al Scanner y al Parser con un fichero. */
    bool parse_file(const std::string&);

    /** Manejo de errores con un numero de línea asociado. */
    void error(const class location& l, const std::string& m);
    /** Manejo general de errores. */
    void error(const std::string& m);

    /** Bandera de salida estándar. */
    bool coutFlag = false;
    /** Nombre del fichero de salida. */
    string outputName = "output.xml";
    
    /** Realiza los pasos del análisis del AST. */
    void sem(Node*);

    /** Puntero al scanner actual. */
    class Scanner* lexer;
    /** Referencia al contexto LEA. */
    class LEACContext& leac;
  };
} // namespace LEAC

#endif
