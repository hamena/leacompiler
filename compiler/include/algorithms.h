#ifndef ALGORITHMS_H_
#define ALGORITHMS_H_

#include <iostream>
#include <map>
#include <string>

#include "algorithmbase.h"
#include "algorithm.h"
#include "stdalgorithms/stdalgorithm.h"

//#define ALGORITHMS_DEBUG 1
#define ALGORITHMS_WARNIGS 1

using std::map;
using std::string;
using std::cout;
using std::endl;

namespace LEAC{

  /** Clase algoritmos.  
   * Esta clase es la clase contenedora de los algoritmos que se definen en código LEA.
   */
  class Algorithms{
  public:
    /** Carga los algoritmos estándar definidos para LEA. */
    static void load_std_algorithms();
    /** Crea y guarda un nuevo algoritmo. */
    static void define_algorithm(const string&);
    /** Selecciona un algoritmo indicado. */
    static void select_algorithm(const string&);
    /** Mueve el puntero de algoritmo actual al siguiente algoritmo. */
    static void next_algorithm();
    /** Comprueba si está definido el algoritmo principal main. */
    static bool is_defined_main_algorithm();
    /** Comprueba si el algoritmo es estándar. */
    static bool is_std(const string&);
    /** Comprueba si un algoritmo existe. */
    static bool check_algorithm(const string&);
    /** Obtienes el algoritmo actual. */
    static Algorithm& get_current_algorithm();
    /** Obtienes un algoritmo indicado. */
    static Algorithm& get_algorithm(const string&);

    /** Resuelve todas las dependencias de las variables de cada algoritmo. */
    static void solve_all_variables_dependencies();

    /** Libera memoria de algoritmos. */
    static void free_algorithms();

    // debug
    static void print_state();
  private:
    /** Definido tipo tabla de algoritmos base. */
    typedef map<string,AlgorithmBase*> BaseAlgorithmsTable;
    /** Definido tipo tabla de algoritmos estándar. */
    typedef map<string,StdAlgorithm*> StdAlgorithmsTable;
    /** Definido tipo tabla de algoritmos. */
    typedef map<string,Algorithm*> AlgorithmsTable;
    /** Definido tipo iterador de algoritmos base. */
    typedef map<string,AlgorithmBase*>::iterator AlgorithmsBaseIterator;
    /** Definido tipo iterador de algoritmos estándar. */
    typedef map<string,StdAlgorithm*>::iterator StdAlgorithmsIterator;
    /** Definido tipo iterador de algoritmos. */
    typedef map<string,Algorithm*>::iterator AlgorithmsIterator;
    static BaseAlgorithmsTable allAlgorithms;   ///< Tabla con todos los algoritmos.
    static StdAlgorithmsTable stdAlgorithms;    ///< Tabla con los algoritmos estándar.
    static AlgorithmsTable algorithms;          ///< Tabla con los algoritmos.
    static AlgorithmsIterator currentAlgorithm; ///< Puntero a algoritmo actual.
    static bool existsMain;                     ///< Bandera de existencia del algoritmo main.
  };

} // namespace LEAC
  
#endif // ALGORITHMS_H_
