#ifndef ALGORITHMBASE_H_
#define ALGORITHMBASE_H_

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <utility>

#include "type.h"

#define ALGBASE_WARNINGS 1
//#define ALGBASE_DEBUG 1

using std::vector;
using std::string;
using std::map;
using std::make_pair;
using std::ostream;
using std::endl;

#define MAIN_ALGORITHM_NAME "main"
#define UNDEFINED_CODE_NAME "no_codename"

namespace LEAC{

  /** Estructura contenedora auxiliar. 
   * Esta estructura representa una variable codificada.
   */
  struct Variable{
    string codeName; ///< Nombre codificado
    Type type;       ///< Tipo de la variable
    Variable() : codeName(UNDEFINED_CODE_NAME), type() {}
    Variable(const string&, const Type&);
  private:
    static unsigned int variableCounter; ///< Variable auxiliar para codificar nombres.
  };
  /** Operador de inserción. Sobrecarga del operador
   * de inserición de flujo para Variables.
   */
  ostream& operator <<(ostream&, const Variable&);

  /** Definición de tipo de tabla de símbolos.*/
  typedef map<string,Variable> SymbolsTable;
  /** Definición de tipo de iterador sobre tabla de símbolos.*/
  typedef SymbolsTable::iterator SymbolsIterator;

  /** Operador menor estricto. 
   * Sobrecarga del operador binario menor estricto para iteradores de tablas de simbolos.
   */
  bool operator <(const SymbolsIterator&, const SymbolsIterator&);

  class NodeExp; // Declaracion adelantada

  /** Clase base de algoritmos.
   * Esta clase es la clase base abstracta de los algoritmos definidos en LEA y 
   * los algoritmos estándar propios del lenguaje.
   */
  class AlgorithmBase{
  public:
    /** Constructor */
    AlgorithmBase(const string&);
  
    /** Añade un argumento de entrada al algoritmo. */
    void new_input_argument(const string&);
    /** Añade un argumento de salida al algoritmo. */
    void new_output_argument(const string&);

    /** Establece las dependencias de los argumentos de entrada. */
    void set_input_dependencies(const vector<NodeExp*>&);

    /** Comprueba si la variable es un argumento de entrada. */
    bool is_input_arg(const string&);
    /** Comprueba si la variable es un argumento de salida. */
    bool is_output_arg(const string&);
    /** Obtienes el nombre del algoritmo. */
    string get_name() const;
    /** Obtienes el nombre codificado del algoritmo. */
    string get_code_name();
    /** Obtienes el número de argumentos de entrada del algoritmo. */
    unsigned int get_n_input() const;
    /** Obtienes el número de argumentos de salida del algoritmo. */
    unsigned int get_n_output() const;

    /** Obtienes los tipos de los argumentos de entrada. */
    Types get_input_types() const;
    /** Obtienes los tipos de los argumentos de salida. */
    Types get_output_types() const;
    /** Obtienes referencias a los argumentos de salida. */
    vector<SymbolsIterator> get_output_references();

    /** Comprueba si el algoritmo es estándar. */
    virtual bool is_std() const = 0;
    /** Comprueba que la variable exista en el algoritmo. */
    virtual bool check_variable(const string&) = 0;
    /** Obtienes una variable indicada del algoritmo. */
    virtual const Variable& get_variable(const string&) = 0;
    /** Obtienes una referencia a una variable indicada del algoritmo. */
    virtual SymbolsIterator get_variable_reference(const string&) = 0;
    /** Guarda el tipo de una variable. */
    virtual void set_type(const string&, const Type&) = 0;
    /** Resuelve las dependecias de las variables en la tabla de símbolos. */
    virtual void solve_dependencies() = 0;
    /** Resuelve las dependenias de los argumentos de salida. */
    virtual void solve_output_dependencies() = 0;

    /** Destructor virtual */
    virtual ~AlgorithmBase(){}
  
    //debug
    virtual void print_algorithm() = 0;
    string get_tabs(int);
  protected:
    /** Función auxiliar para buscar variables en las tablas de símbolos. */
    SymbolsIterator find(const string&);
    /** Función auxiliar que normaliza el iterador a variable no encontrada. */
    SymbolsIterator variable_not_found();

    SymbolsTable inputArgs;   ///< Tabla de símbolos para los argumentos de entrada.
    SymbolsTable outputArgs;  ///< Tabla de símbolos para los argumentos de salida.
  private:
    string algorithmName;     ///< Nombre del algoritmo.
    string algorithmCodeName; ///< Nombre del algoritmo codificado.
    vector<string> inputNames;   ///< Nombres de los argumentos de entrada ordenados.
    vector<string> outputNames;  ///< Nombres de los argumentos de salida ordenados.
  };

} // namespace LEAC
  
#endif // ALGORITHMBASE_H_
  
