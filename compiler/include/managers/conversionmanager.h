#ifndef CONVERSIONMANAGER_H_
#define CONVERSIONMANAGER_H_

#include <sstream>
#include <vector>
#include <string>

#include "../type.h"

#define CONVERSIONMANAGER_WARNINGS 1
//#define CONVERSIONMANAGER_DEBUG 1

#define MAX_ROWS_TYPE 12
#define MAX_COLS_TYPE 12
#define MAX_ROWS_EXPR 12
#define MAX_COLS_EXPR 21

using namespace std;

namespace LEAC{

  /** Clase gestora de las conversiones entre tipos. */
  class ConversionManager{
  public:
    /** Comprueba si la conversion de cada tipo de cada lista es legal. */
    static bool is_legal(const Types&, const Types&);
    /** Comprueba si la conversion de un tipo a otro es legal. */
    static bool is_legal(const Type&, const Type&);
    /** Comprueba si el tipo es legal con la operación unaria indicada. */
    static bool is_legal_unary(const string&, const Type&);
    /** Comprueba si los tipos son legales en una asignación de intercambio. */
    static bool is_legal_swap(const Type&, const Type&);
    /** Comprueba si los tipos son legales en una expresión binaria indicada. */
    static bool is_legal_expr(const Type&, const Type&, const string&);
    /** Comprueba si los tipos son legales con el operador en. */
    static bool is_legal_in(const Type&, const Type&);
    /** Resuelve al tipo mas general. */
    static Type solve(const Type&, const Type&);
  private:
    /** Resolución recursiva auxiliar. */
    static Type solve_rec(Type&, const Type&, const Type&);
    /** Función auxiliar para obtener el índice en la tabla del operador. */
    static int get_index_op(const string&);
    /** Tabla de conversión entre tipos. */
    static bool conversionTableType[MAX_ROWS_TYPE][MAX_COLS_TYPE];
    /** Tabla de conversión de expresiones con tipos. */
    static bool conversionTableExpr[MAX_ROWS_EXPR][MAX_COLS_EXPR];
  };

} // namespace LEAC
  
#endif // CONVERSIONMANAGER_H_
