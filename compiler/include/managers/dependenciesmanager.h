#ifndef DEPENDENCIES_MANAGER_H_
#define DEPENDENCIES_MANAGER_H_

#include <string>
#include <map>
#include <vector>
#include <utility>
#include "../type.h"
#include "../algorithm.h"
#include "../stdalgorithms/stdalgorithm.h"
//#include "../nodes/nodeexpressions.h"

//#define DEPENDENCIESMANAGER_DEBUG 1

using std::string;
using std::map;
using std::vector;
using std::pair;
using std::make_pair;

namespace LEAC{

  class NodeExp;

  /** Clase gestora encargada de manejar las dependencias entre las variables del código. */
  class DependenciesManager{
  public:
    /** Definición de dependencia. */
    typedef pair<NodeExp*, Type> Dependency;
    /** Definición de lista de dependencias. */
    typedef vector<Dependency> Dependencies;

    /** Establece una nueva dependencia entre símbolo y expresión. */
    static void new_dependency(const SymbolsIterator&, NodeExp*, int dim=0);
    /** Establece una nueva dependencia entre símbolo y expresión. */ 
    static void new_in_dependency(const SymbolsIterator&, NodeExp*);
    /** Resetea las dependencias de una variable. */
    static void reset_dependencies(const SymbolsIterator&);
    /** Resuelve las dependencias de una variable. */
    static void solve_variable_dependencies(SymbolsIterator&);
    /** Reduce una lista de tipos al tipo mas general. */
    static Type reduce_type(const Types&);

    /** Función auxiliar para tuplas de variables. */
    static void push_variable(const SymbolsIterator&, int nAccess=0);
    /** Función auxiliar para tuplas de expresiones. */
    static void push_expression(NodeExp*);
    /** Función auxiliar para llamadas a algoritmos. */
    static void push_algorithm(AlgorithmBase&);

    /** Liberar memoria de llamadas a función. */
    static void free_function_calls();
  private:
    static map <SymbolsIterator,Dependencies> dependenciesTable; ///< Tabla de dependencias.
    static vector<SymbolsIterator> bufferVariables;           ///< Bufer auxiliar de variables.
    static vector<NodeExp*> bufferExpressions;                ///< Bufer auxiliar de expresiones.
    static vector<int> bufferAccess;                          ///< Bufer auxiliar de accesos.
    static vector<NodeExp*> functionCalls;                    ///< Lista de llamadas a función.

    /** Guarda dependencia en la tabla de dependencias. */
    static void save_dependency(const SymbolsIterator&, NodeExp*, const Type&);
    /** Guarda el contenido de los búfers auxiliares en la tabla de dependencias. */
    static void save_buffers();
    /** Resetea los búfers auxiliares. */
    static void reset_buffers();

    /** Obtiene los tipos de las dependencias. */
    static Types get_new_types(Dependencies&);
    /** Guarda el tipo de una variable. */
    static void save_type(SymbolsIterator&, const Type&);
  };

} // namespace LEAC
  
#endif // DEPENDENCIES_MANAGER_H_
