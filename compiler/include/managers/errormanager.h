#ifndef ERRORMANAGER_H_
#define ERRORMANAGER_H_ 

#include <string>
#include <iostream>

#include "../type.h"

#define NOPRINT "#no_print#"

using std::string;
using std::cout;
using std::endl;

namespace LEAC{

  /** Clase gestora de errores. Esta clase se encarga de formatear la salida de los errores
   * encontrados en el código LEA.
   */
  class ErrorManager{
  public:
    /** Comprueba si ha habido algun error. */
    static bool check_error();

    //@{
    /** Formatea la salida de un error. */
    static void dimension_doesnt_match(int, const string&);
    static void access_not_integer(int, const string&, const Types&);
    static void expression_not_type(int, const string&, const string&, const Type&);
    static void bexpression_not_legal(int, const string&, const Type&,const string&,const Type&);
    static void boperator_doesnt_match(int, const string&, const string& op);
    static void uexpression_not_legal(int, const string&, const string& op, const Type&);
    static void uoperator_doesnt_match(int, const string&, const string& op);
    static void delimiter_doesnt_match(int, const string&);
    static void simple_assign_not_legal(int, const string&, const Type&, const Type&);
    static void simple_assign_doesnt_match(int, const string&);
    static void parallel_assign_not_legal(int, const string&, const Types&, const Types&);
    static void parallel_assign_doesnt_match(int, const string&);
    static void swap_assign_not_legal(int, const string&, const Type&, const Type&);
    static void swap_assign_doesnt_match(int, const string&);
    static void parameters_not_legal(int, const string&, const Types&, const string&);
    static void variable_not_found(int, const string&, const string&);
    static void algorithm_not_found(int, const string&, const string&);
  
    static void algorithm_already_defined(int, const string&, const string&); //
    static void argument_conflict(int, const string&, const string&);         
    static void variable_already_exists(int, const string&, const string&);
    static void input_arg_only_read(int, const string&, const string&);
    static void output_arg_only_write(int, const string&, const string&);
    static void function_call_args_doesnt_match(int, const string&, const string&, unsigned int);
    static void immediate_array_not_legal(int, const string&, const Types&);
    static void immediate_set_not_legal(int, const string&, const Types&);

    static void main_not_input_argument(int, const string&, const string&);
    static void main_not_output_argument(int, const string&, const string&);
    static void main_not_call(int, const string&);
    static void main_doesnt_exists();
  
    static void inference_impossible(const string&);
    static void inference_type_conflict(const string&, const Types&);
    static void inference_previous_type_conflict(const string&, const Type&, const Type&);

    // STD errors
    static void std_algorithm_illegal(const string&, const string&, const Types&, const Types&);
    //@}
  private:
    /** Obtienes la cabecera del error */
    static string get_header(string tostr="", int lno=-1, string alg="", bool inf=false);

    static bool error; ///< Bandera de errores.
  };

} // namespace LEAC
  
#endif // ERROMANAGER_H_
