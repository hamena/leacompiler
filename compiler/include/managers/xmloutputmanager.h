#ifndef XMLOUTPUTMANAGER_H_
#define XMLOUTPUTMANAGER_H_

#define XMLOUTPUTMANAGER_WARNINGS 1
#define XMLOUTPUTMANAGER_DEBUG 1

#include <string>
#include <vector>
#include <sstream>
#include <stack>
#include <set>

using std::string;
using std::vector;
using std::stringstream;
using std::stack;
using std::set;

namespace LEAC{

  /** Clase gestora encargada de formatear la salida en XML. */
  class XMLOutputManager{
  public:
    //@{
    /** Definiciones de listas de nombres. */
    typedef vector<string> AttributesNames, AttributesValues;
    //@}
    /** Escribe una nueva etiqueta XML en la salida. */  
    static void open_tag(stringstream&, const string&,
			 const AttributesNames& attrNames = AttributesNames(),
			 const AttributesValues& attrValues = AttributesValues(),
			 bool close=false);
    /** Cierra la última etiqueta XML abierta. */
    static void close_tag(stringstream&);

#ifdef XMLOUTPUTMANAGER_DEBUG
    static void print_state();
#endif
  
  private:
    /** Estructura etiqueta. */
    struct Tag{
      string tagName; ///< Nombre de la etiqueta.
      int order;      ///< Atributo de orden.
      Tag(const string& m, int o) :
	tagName(m), order(o) {}
    };
    static stack<Tag> tags;            ///< Pila de etiquetas
    static set<string> orderWhitelist; ///< Lista de nombres de etiquetas que usan orden.
    static int nTabs;                  /// Contador de tabulaciones.
    /** Obtienes las tabulaciones necesarias. */
    static string get_tabs();
    /** Mira si una etiqueta está en la lista blanca. */
    static bool is_order_whitelisted(const string&);
    /** Escribe los atributos de una etiqueta. */
    static void write_attributes(stringstream&, const AttributesNames&, const AttributesValues&);
    /** Escribe el atributo de orden en una etiqueta. */
    static void write_order(stringstream&);
  };

} // namespace LEAC
  
#endif // XMLOUTPUTMANAGER_H_
