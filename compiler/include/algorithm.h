#ifndef ALGORITHM_H_
#define ALGORITHM_H_

#include <iostream>
#include <vector>
#include <string>
#include <utility>
#include <map>

#include "algorithmbase.h"
#include "type.h"

#define ALGORITHM_WARNINGS 1
//#define ALGORITHM_DEBUG 1

using std::vector;
using std::string;
using std::pair;
using std::map;
using std::cout;
using std::endl;

namespace LEAC{

  /** Clase algoritmo. 
   * Clase derivada del algoritmo base que contienen toda la información sobre las 
   * funciones definidas sobre código LEA.
   */
  class Algorithm : public AlgorithmBase{
  private:
    struct Scope;
  public:
    /** Constructor */
    Algorithm(const string&);

    /** Crea un nuevo ámbito. */
    void new_scope();
    /** Situa el ámbito actual como el primer ámbito. */
    void first_scope();
    /** Desplaza el ámbito actual hasta el siguiente ámbito. */
    void next_scope();
    /** Cierra el ámbito actual y vuelve al anterior. */
    void close_scope();
    /** Reinicia los ámbitos. */
    void reset_scopes();

    /** Crea una nueva variable en la tabla de símbolos. */
    void new_variable(const string&, const Type&);
    /** Otorga un tipo a una variable de la tabla de símbolos. */
    void set_type(const string&, const Type&);

    /** Resuelve las dependencias de las variables del algoritmo. */
    void solve_dependencies();
    /** Resuelve las dependencias de los argumentos de salida. */
    void solve_output_dependencies();

    /** Obtienes una variable indicada. */
    const Variable& get_variable(const string&);
    /** Obtienes la referencia de una variable indicada. */
    SymbolsIterator get_variable_reference(const string&);

    /** Comprueba si el algoritmo es estándar. */
    bool is_std() const { return false; }
    /** Comprueba si el algoritmo es el algoritmo principal main */
    bool is_main() const;
    /** Comprueba si existe una variable en la tabla de símbolos. */
    bool check_variable(const string&);
    /** Comprueba si una variable es un argumento. */
    bool is_arg(const string&);

    // debug
    void print_algorithm();

    /** Destructor */
    ~Algorithm();
  private:

    Scope *initialScope; ///< Puntero al ámbito inicial.
    Scope *currentScope; ///< Puntero al ámbito actual.

    /** Estructura recursiva que representa los ámbitos interiores de un algoritmo. */
    struct Scope{
      Scope* previousScope;       ///< Puntero al ámbito anterior
      vector<Scope*> nextScopes;  ///< Lista de punteros de ámbitos subordinados.
      int scopeIndex;             ///< Índice del ámbito actual
      SymbolsTable symbolsTable;  ///< Tabla de símbolos propia del ámbito.
      Scope() : previousScope(NULL), nextScopes(), scopeIndex(0){}
      Scope(Scope& s) : previousScope(&s), nextScopes(), scopeIndex(0){}
    };

    /** Función auxiliar para buscar variables en la tabla de símbolos. */
    SymbolsIterator find(const string&);
    /** Función auxiliar para normalizar el iterador de variable no encontrada. */
    SymbolsIterator variable_not_found();
    /** Reiniciar ámbitos recursivo. */
    void reset_scopes_rec(Scope*);
    /** Borrar ámbitos. */
    void delete_scopes(Scope*);

    /** Resolver dependencias de los ámbitos. */
    bool solve_scope_dependencies(Scope*, bool&);

    void print_arguments(int);
    void print_scopes(Scope*, int);
  };

} // namespace LEAC
  
#endif // ALGORITHM_H_
